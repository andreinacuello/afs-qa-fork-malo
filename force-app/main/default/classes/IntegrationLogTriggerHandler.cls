//Class handler for Integration Log Trigger
public class IntegrationLogTriggerHandler {
    //Method contains logic for after insert event
    public static void afterInsert(Map<Id,IntegrationLog__c> mapNew){
        processIntegration(mapNew);
    }
    
    public static boolean processIntegration(Map<Id,IntegrationLog__c> mapNew){        
        RemoteSiteManager rsm = new RemoteSiteManager(false);
        //Validate if integrations is enabled
        if(rsm.integrationCustomSettings != null && !rsm.integrationCustomSettings.Enabled_Integration__c){
            return false;
        }
        
        Map<Id,IntegrationLog__c> mapLogInterest = new Map<Id,IntegrationLog__c>();//{Id Log , Id Contact}
        Map<Id,Contact> mapContacts = new Map<Id,Contact>();
        Map<Id,Application__c> mapApplications = new Map<Id,Application__c>();        
        
        //Filter
        for(IntegrationLog__c log : mapNew.values()){
            if(log.Status__c == 'Pending' && log.System__c == 'Global Link' && log.Trigger__c == 'Interest' && log.Contact__c != null && log.Application__c != null){
				mapLogInterest.put(log.Id,log);
                mapContacts.put(log.Contact__c,new Contact(id=log.Contact__c));
                mapApplications.put(log.Application__c, new Application__c(id=log.Application__c));
            }
        }
        
        //Get Contacts
        if(!mapContacts.isEmpty()){
            List<String> lstFields = new List<String>(Schema.getGlobalDescribe().get('Contact').getDescribe().fields.getMap().keySet());
            Set<Id> contactsId = mapContacts.keyset();
            String query = 'SELECT ' + String.join(lstFields,',') + ', Account.Global_Link_Family_ID__c FROM Contact WHERE Id in :contactsId';
            List<Contact> lstContacts = Database.Query(query);
            mapContacts = new Map<Id,Contact>(lstContacts);
        }
        
        //Get Applications
        if(!mapApplications.isEmpty()){
            List<String> lstFields = new List<String>(Schema.getGlobalDescribe().get('Application__c').getDescribe().fields.getMap().keySet());
            Set<Id> appsId = mapApplications.keyset();
            String query = 'SELECT ' + String.join(lstFields,',') + ', Sending_Partner__r.IOC_Code__c FROM Application__c WHERE Id in :appsId';
            List<Application__c> lstApps = Database.Query(query);
            mapApplications = new Map<Id,Application__c>(lstApps);
        }
        
        //Process event Interests
        if(!mapLogInterest.isEmpty()){
        	rsm.postEventInterest(mapLogInterest,mapContacts,mapApplications);    
        }

		return true;        
    }
}