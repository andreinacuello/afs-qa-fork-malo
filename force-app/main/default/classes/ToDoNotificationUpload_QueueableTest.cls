@IsTest
private class ToDoNotificationUpload_QueueableTest {
    private static testmethod void executeTest(){
        Afs_Triggers_Switch__c afsTriggerSwitch = Util_Test.create_AFS_Triggers_Switch('Afs_Trigger', true, true);
        List<To_Do_Item__c> toDoList = new List<To_Do_Item__c>();
        List<Opportunity> opps = new List<Opportunity>();
        List<Application__c> apps = new List<Application__c>();
        
        List<User> users = [Select IOC_Code__c From User Where Id = :UserInfo.getUserId() LIMIT 1];
        Account acc = new Account(name= 'acc', IOC_Code__c = users[0].IOC_Code__c);
        insert acc;
        Contact con = new Contact(lastName = 'Test', AccountId = acc.Id);
        insert con;
        Hosting_Program__c  hp = new Hosting_Program__c(name = 'Test Hosting Program', Host_Partner__c = acc.Id, 
                                                        Duration__c = 'YP', Program_Content__c = 'as');
        Insert hp;
        GL_SF_Integrations_Batch.isExecutingBatchIntegrations = true;
        
        Program_Offer__c po = new Program_Offer__c();
        po.name = 'Test Program Offer';
        po.Hosting_Program__c = hp.Id;
        po.Sending_Partner__c = acc.Id;
        po.Applications_Received_To_local__c = System.today() + 30;
        po.Name = 'Test';
		insert po;
        
        opps.add(new Opportunity(name = 'Test opp', StageName = 'Prospecting', 
                                          CloseDate = Date.today(), Pendings_ToDo_submit_documents__c = null));
        opps.add(new Opportunity(name = 'Test opp', StageName = 'Prospecting', 
                                           CloseDate = Date.today(), Pendings_ToDo_submit_documents__c = null));
        insert opps;
        
        apps.add(Util_Test.createApplication(con, po));
        apps[0].Opportunity__c = opps[0].id;
        apps.add(Util_Test.createApplication(con, po));
        apps[1].Opportunity__c = opps[1].id;
        apps.add(Util_Test.createApplication(con, po));
        apps[2].Opportunity__c = opps[1].id;
        insert apps;
        
        toDoList.add(new To_Do_Item__c(Type__c = 'Upload', Help__c = 'Test', Name = 'toDo1',
                                       Due_Date__c = Date.today().addDays(1),
                                       Application__c = apps[0].Id, 
                                       RecordTypeId = Schema.SObjectType.To_Do_Item__c.getRecordTypeInfosByName().get('Custom').getRecordTypeId()));
        toDoList.add(new To_Do_Item__c(Help__c = 'Test', 
                                       Name = 'toDo2',
                                       Due_Date__c = Date.today().addDays(1),
                                       Application__c = apps[1].Id, 
                                       SCHOLARSHIP_DOCUMENTATION__c = True,
                                       RecordTypeId = Schema.SObjectType.To_Do_Item__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId()));
        toDoList.add(new To_Do_Item__c(Help__c = 'Test', 
                                       Name = 'toDo3',
                                       Due_Date__c = Date.today().addDays(1),
                                       Application__c = apps[2].Id, 
                                       START_YOUR_LANGUAGE_TRAINING__c = True,
                                       RecordTypeId = Schema.SObjectType.To_Do_Item__c.getRecordTypeInfosByName().get('Standard').getRecordTypeId()));
        insert toDoList;
        
        Test.startTest();
        System.enqueueJob(new ToDoNotificationUpload_Queueable());
        Test.stopTest();
        
        List<Opportunity> oppsDb = [SELECT Pendings_ToDo_submit_documents__c FROM Opportunity WHERE Id = :opps[0].Id OR Id = :opps[1].Id];
        System.assertEquals(oppsDb[0].Pendings_ToDo_submit_documents__c, toDoList[0].Name);
        System.assertEquals(oppsDb[1].Pendings_ToDo_submit_documents__c, toDoList[1].Name + '\n' + toDoList[2].Name);
    }
}