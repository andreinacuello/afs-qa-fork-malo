global without sharing class AfsComp_TravelInfomationCtrl {

     @AuraEnabled
    global static map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> getPicklistValues(String objObject, string fld) {
        list<String> fieldAPINames = fld.split(',');
        map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> mapFieldPicklistVal = new map<String,list<Afs_UtilityApex.PicklistWrapperUtil>>();
        for(String fieldAPIName : fieldAPINames){
            list<Afs_UtilityApex.PicklistWrapperUtil> picklsitValues = new list<Afs_UtilityApex.PicklistWrapperUtil>();
            picklsitValues.add(new Afs_UtilityApex.PicklistWrapperUtil(System.label.AfsLbl_General_None,false,System.label.AfsLbl_General_None));  
            picklsitValues.addAll(Afs_UtilityApex.getPicklistWrapper(objObject, fieldAPIName.trim()));
            mapFieldPicklistVal.put(fieldAPIName.trim(),picklsitValues);
        }
        for(String str : mapFieldPicklistVal.keySet()){
            system.debug(str);
            system.debug(mapFieldPicklistVal.get(str));
        }
        
        return mapFieldPicklistVal; 
    }
    
     @AuraEnabled
    global static Program_Offer__c getProgramOfferDetails(string programId) {
        system.debug('@@@@@@' + programId);
    	Program_Offer__c  programOffer  =   [select id,Visa_Requirements__c,Vaccination_Requirements__c from Program_Offer__c where Id =:  programId];
        return programOffer;
    }
    @AuraEnabled
    global static void saveTravelInfomation(contact contactInfo,String taskID) {
    	update contactInfo;
        if( taskID != null){
          To_Do_Item__c  todoItemRec  =   [select id,Status__c from To_Do_Item__c where Id =:  taskID];
            if(todoItemRec != null){
                todoItemRec.Status__c = 'In Review';
                 update todoItemRec;
            }
        }
    }
    
    
    /**
     * deleteAttachment    : Method to delete attachments uploaded
     * @param  AttchmentId : ContentDocumentId from Client Side
     * @return       
     */
    @AuraEnabled
    global static String deleteAttachment(string AttchmentId){
        	list<ContentDocument>  attchamentList  = new list<ContentDocument>();  
    	try{
          	attchamentList =  [SELECT Id FROM ContentDocument where Id =: AttchmentId];
        	delete attchamentList; 
           
        }catch(Exception e){
            Afs_UtilityApex.logger(e.getMessage());
            throw new AuraHandledException(e.getMessage().split(',')[1]);
        }	
     	return AttchmentId;
    }
}