public class LeadTriggerHandler {
    public static void beforeInsert(List<Lead> leadList){
        updateCompanyField(leadList);
    }
    
    public static void updateCompanyField(List<Lead> leadList){
        Set<Id> ownerIds = new Set<Id>();
        try{
            for(Lead l : leadList){
                if(l.OwnerId != null && l.OwnerId.getSObjectType() == User.SObjectType) ownerIds.add(l.OwnerId);
            }
            Map<Id,User> mapUser = new Map<Id,User>();
            if(ownerIds.size() > 0) mapUser = new Map<Id,User>([select id,IOC_Code__c from User where id IN: ownerIds]);
            
            for(Lead le : leadList){
                le.Company = le.LastName;
                if(ownerIds.size() > 0) le.IOC_Code__c = mapUser.get(le.OwnerId) != null ? mapUser.get(le.OwnerId).IOC_Code__c : '';
            }  
        }catch (Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
            System.debug('Line: ' + e.getLineNumber());
            System.debug('Cause: ' + e.getCause());
            System.debug('TypeName: ' + e.getTypeName());
        }
        
    }
    
}