global without sharing class Afs_PreSelectedPaymentController {
    
	@AuraEnabled
    global static Decimal getApplicationFee (string applicationId){
        Decimal appFee = 0;
        list<Payment_Installment__c > paymentInstalments = [SELECT id,Amount__c  FROM Payment_Installment__c 
                                                            WHERE Application__c =:applicationId AND Number__c = 0 ORDER BY CreatedDate Desc LIMIT 1];      
        if(!paymentInstalments.isEmpty()){
            appFee = paymentInstalments[0].Amount__c;
        }
        return appFee;
        
    }
    
    @AuraEnabled
    global static Program_Offer__c getApplicationProgram (string applicationId){
        Program_Offer__c pOffer = new Program_Offer__c();
        list<Program_Offer__c> pOfferList = [SELECT id,Camp_Name__c,Camp_URL__c  FROM Program_Offer__c 
                                                            WHERE id =:applicationId LIMIT 1];      
        if(!pOfferList.isEmpty()){
            pOffer = pOfferList[0];
        }
        return pOffer;
        
    }
    
    @AuraEnabled
    global static void updateApplication (string campURL,string applicationId,String recId){
        list<Application__c> appLIST = [SELECT id,Camp_link_clicked__c FROM Application__c WHERE id =:applicationId LIMIT 1];      
        if(!appLIST.isEmpty()){
            appLIST[0].Camp_link_clicked__c = campURL;
        }
        update appLIST;
		Afs_PreSelectedPaymentController.updateParent(recId);
    }
    
    @AuraEnabled
    //RecId - To do item record id
    global static void updateParent (string recId){
        List<To_Do_Item__c> rec = [SELECT Id, Status__c 
                                              FROM To_Do_Item__c 
                                          	WHERE Id  = :recId];
        if(rec.size() > 0){
            rec[0].status__c = 'In Review';
            update rec[0];
        }
    }
    
    
}