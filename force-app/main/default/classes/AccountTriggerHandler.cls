public class AccountTriggerHandler {
	//Method contains logic for beforeInsert Event
    public static void beforeInsert(List<Account> lstNew){
		generateNewUUID(lstNew);
    }
    
    public static void generateNewUUID(List<Account> lstNew){
        Id recordTypeID =  Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('HH_Account').getRecordTypeId();
        
        for(Account acc : lstNew){
            if(acc.RecordTypeId == recordTypeID && acc.Global_Link_Family_ID__c == null){
                acc.Global_Link_Family_ID__c = RemoteSiteParser.generateNewUUID();    
            }
        }
    }
}