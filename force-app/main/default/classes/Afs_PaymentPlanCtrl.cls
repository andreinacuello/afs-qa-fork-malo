global with sharing  class Afs_PaymentPlanCtrl {
    
    @AuraEnabled
    global static  List<Payment_Installment__c>  getPaymentInstallmentList (String aplicationID){
    	List<Payment_Installment__c> paymentInstallmentList =[select id,Name,Amount__c,Due_Date__c,Paid_Amount__c,Balance__c
                                                              from Payment_Installment__c where Application__c = : aplicationID order by createdDate ];	
        return paymentInstallmentList;
    }
    
    @AuraEnabled
    global static  List<Payment_Entry__c>  getPaymentEntryList(String aplicationID){
    	List<Payment_Entry__c> paymentList =[select id,Name,Amount__c,Accreditation_date__c,Payment_method__c
                                                              from Payment_Entry__c where Application__c = : aplicationID order by createdDate ];	
         return paymentList;
    }
    
    @AuraEnabled
    global static  void  updateTask(String taskID){
        if( taskID != null){
          To_Do_Item__c  todoItemRec  =   [select id,Status__c from To_Do_Item__c where Id =:  taskID];
            if(todoItemRec != null){
                todoItemRec.Status__c = 'In Review';
                 update todoItemRec;
            }
        }
    }
}