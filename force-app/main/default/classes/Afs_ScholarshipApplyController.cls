global without sharing class Afs_ScholarshipApplyController {
    /**
    * Desc : Method to get all records of scholarships 
    * 		  available on the basis of account linked to the contact
    * 		  and return them in format of wrapper
    */
    @AuraEnabled
    global static list<ScholarshipWrapper> getScholarshipWrapper(String acountId,String contactId,String appId){
        list<ScholarshipWrapper> ScholarshipWrapperList = new list<ScholarshipWrapper> ();
        map<id,Scholarship_Application__c> mapScholarshipId = new map<id,Scholarship_Application__c> ();
        Set<id> programOfferedIdSet = new Set<id>();
        Set<id> scholarshipIdSet = new Set<id>();
        // Get Picklist labels and create wrapper
        SObject objObject = Schema.getGlobalDescribe().get('Scholarship__c').newSObject() ;
    	Schema.sObjectType objType = objObject.getSObjectType();               
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();                
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap(); 
        map<String,map<String,Afs_UtilityApex.PicklistWrapperUtil>> fieldMapNew = new map<String,map<String,Afs_UtilityApex.PicklistWrapperUtil>>();
        
        map<String,Afs_UtilityApex.PicklistWrapperUtil> valueMap = new map<String,Afs_UtilityApex.PicklistWrapperUtil>();
        for(Schema.PicklistEntry picklistVal : fieldMap.get('Scholarship_Amount_Available__c').getDescribe().getPickListValues()){
            valueMap.put(String.valueOf(picklistVal.getValue()), new Afs_UtilityApex.PicklistWrapperUtil(picklistVal.getLabel(),false,picklistVal.getValue()));
        }
        fieldMapNew.put('Scholarship_Amount_Available__c',valueMap);
        
        valueMap = new map<String,Afs_UtilityApex.PicklistWrapperUtil>();
        for(Schema.PicklistEntry picklistVal : fieldMap.get('Applicant_Eligibility_Specifications__c').getDescribe().getPickListValues()){
            valueMap.put(String.valueOf(picklistVal.getValue()), new Afs_UtilityApex.PicklistWrapperUtil(picklistVal.getLabel(),false,picklistVal.getValue()));
        }
        fieldMapNew.put('Applicant_Eligibility_Specifications__c',valueMap);
        
        // get existing records 
        for(Scholarship_Application__c scholarApp : [SELECT id,Applicant__c,Application__c,Scholarship__c,Application_Date__c,Eli__c,Amount_Asked__c 
                                                     FROM Scholarship_Application__c WHERE Applicant__c =: contactId
                                                     AND Application__c =: appId])
        {
            mapScholarshipId.put(scholarApp.Scholarship__c,scholarApp);
        }
        
        list<Program_Offer_in_App__c> prList = [SELECT id,Program_Offer__c,Application__r.Status_App__c,Is_the_applicant_Eligible__c FROM Program_Offer_in_App__c 
                                                        WHERE Application__c =: appId AND Contact__c =: contactId AND Is_the_applicant_Eligible__c = null];
        Boolean isAppProcessed = prList.isEmpty();
        
        // Get All Program Interest for the current contact
        for(Program_Offer_in_App__c programInterest :  [SELECT id,Program_Offer__c,Application__r.Status_App__c,Is_the_applicant_Eligible__c FROM Program_Offer_in_App__c 
                                                        WHERE Application__c =: appId AND Contact__c =: contactId])
        {
            //system.debug(' programInterest.Application__r.Status_App__c -- > ' + programInterest.Application__r.Status_App__c);
            if(isAppProcessed){
               if(programInterest.Is_the_applicant_Eligible__c == System.label.AfsLbl_Application_Eligible){
                   programOfferedIdSet.add(programInterest.Program_Offer__c);
               } 
            }else{
                programOfferedIdSet.add(programInterest.Program_Offer__c);
            }
            
        }
        //GET all scholarship ids corresponding to the select program interest
        for(Sch_in_Program__c programScholarship :  [SELECT id,Scholarship__c,Program_Offer__c FROM Sch_in_Program__c WHERE Program_Offer__c IN: programOfferedIdSet])
        {
            scholarshipIdSet.add(programScholarship.Scholarship__c);
        }
        Datetime currentDate = System.now();
        for(Scholarship__c scholarShipObj : [SELECT id,Applicant_Eligibility_Specifications__c,Scholarship_Amount_Available__c,
                                             Scholarship_Application_Deadline__c,Name,Scholarship_Description_Rich__c,Min_GPA__c, 
                                             Rre_Application_Fee_Amount__c  FROM Scholarship__c
                                             WHERE id IN: scholarshipIdSet AND Scholarship_Application_Deadline__c >: currentDate])
        {
            list<Afs_UtilityApex.PicklistWrapperUtil> Elegibility = new list<Afs_UtilityApex.PicklistWrapperUtil>();
            list<Afs_UtilityApex.PicklistWrapperUtil> AmountAvailable = new list<Afs_UtilityApex.PicklistWrapperUtil>();
            AmountAvailable.add(new Afs_UtilityApex.PicklistWrapperUtil(System.label.AfsLbl_ScholarshipApply_DesiredAmountDefault,false,System.label.AfsLbl_ScholarshipApply_DesiredAmountDefault));
            Elegibility.add(new Afs_UtilityApex.PicklistWrapperUtil(System.label.AfsLbl_ScholarshipApply_ElegibilityDefault,false,System.label.AfsLbl_ScholarshipApply_ElegibilityDefault));
            //for existing records
            if(mapScholarshipId.containsKey(scholarShipObj.id)){
                if(scholarShipObj.Applicant_Eligibility_Specifications__c != null){
                	// Get wrapper and also..update value.. according to the backend value selected
                	for(String strEligibility : scholarShipObj.Applicant_Eligibility_Specifications__c.split(';')){
                		Elegibility.add(fieldMapNew.get('Applicant_Eligibility_Specifications__c').get(strEligibility));
                	}
                    //Elegibility.addAll(scholarShipObj.Applicant_Eligibility_Specifications__c.split(';'));
                }
                if(scholarShipObj.Scholarship_Amount_Available__c != null){
                	// Get wrapper and also..update value.. according to the backend value selected
                	for(String strEligibility : scholarShipObj.Scholarship_Amount_Available__c.split(';')){
                		AmountAvailable.add(fieldMapNew.get('Scholarship_Amount_Available__c').get(strEligibility));
                	}
                    //AmountAvailable.addAll(scholarShipObj.Scholarship_Amount_Available__c.split(';'));
                }
                if(mapScholarshipId.get(scholarShipObj.id).Amount_Asked__c != 100){
                	scholarShipObj.Scholarship_Amount_Available__c = System.label.AfsLbl_ScholarshipApply_Partial+ ' ' + mapScholarshipId.get(scholarShipObj.id).Amount_Asked__c + '%';
                }else{
                    scholarShipObj.Scholarship_Amount_Available__c = System.label.AfsLbl_ScholarshipApply_Full+ ' ' + mapScholarshipId.get(scholarShipObj.id).Amount_Asked__c + '%';
                }
                scholarShipObj.Applicant_Eligibility_Specifications__c = mapScholarshipId.get(scholarShipObj.id).Eli__c;
                ScholarshipWrapperList.add(new ScholarshipWrapper(scholarShipObj,Elegibility,AmountAvailable,true));
            }else{
                //for NON existing records
                if(scholarShipObj.Applicant_Eligibility_Specifications__c != null){
                	// Get wrapper and also..update value.. according to the backend value selected
                	for(String strEligibility : scholarShipObj.Applicant_Eligibility_Specifications__c.split(';')){
                		Elegibility.add(fieldMapNew.get('Applicant_Eligibility_Specifications__c').get(strEligibility));
                	}
                    //Elegibility.addAll(scholarShipObj.Applicant_Eligibility_Specifications__c.split(';'));
                }
                if(scholarShipObj.Scholarship_Amount_Available__c != null){
                	// Get wrapper and also..update value.. according to the backend value selected
                	for(String strEligibility : scholarShipObj.Scholarship_Amount_Available__c.split(';')){
                		AmountAvailable.add(fieldMapNew.get('Scholarship_Amount_Available__c').get(strEligibility));
                	}
                    //AmountAvailable.addAll(scholarShipObj.Scholarship_Amount_Available__c.split(';'));
                }
                ScholarshipWrapperList.add(new ScholarshipWrapper(scholarShipObj,Elegibility,AmountAvailable,false));
            }
            
        }
        return ScholarshipWrapperList;
    }
    
    /**
    * Desc : Method to get all records of scholarships 
    * 		  available on the basis of account linked to the contact
    * 		  and return them in format of wrapper
    */
    @AuraEnabled
    global static void saveScholarshipWrapper(list<Scholarship__c> ScholarshipWrapperList,String contactId, String applicationId){
        //Afs_Triggers_Switch__c afsTriggerSwitch = Afs_Triggers_Switch__c.getValues('Afs_Trigger');
        
        //afsTriggerSwitch.By_Pass_Opportunity_Trigger__c = true;
        //update afsTriggerSwitch;
        try{
            // get existing records 
            map<id,Scholarship_Application__c> mapScholarshipId = new map<id,Scholarship_Application__c> ();
            map<id,id> mapScholarshipProgram = new map<id,id> ();
            Set<id> programOfferedIdSet = new Set<id>();
            // Get All Program Interest for the current contact
            for(Program_Offer_in_App__c programInterest :  [SELECT id,Program_Offer__c FROM Program_Offer_in_App__c 
                                                            WHERE Application__c =: applicationId AND Contact__c =: contactId])
            {
                programOfferedIdSet.add(programInterest.Program_Offer__c);
            }
            
            //GET all scholarship ids corresponding to the select program interest
            for(Sch_in_Program__c programScholarship :  [SELECT id,Scholarship__c,Program_Offer__c FROM Sch_in_Program__c  
                                                         WHERE Program_Offer__c  IN: programOfferedIdSet])
            {
                mapScholarshipProgram.put(programScholarship.Scholarship__c,programScholarship.Program_Offer__c);
            }
            for(Scholarship_Application__c scholarApp : [SELECT id,Applicant__c,Application__c,Scholarship__c,Application_Date__c,Eli__c,Amount_Asked__c,Program_Offer__c 
                                                         FROM Scholarship_Application__c WHERE Applicant__c =: contactId
                                                         AND Application__c =: applicationId])
            {
                mapScholarshipId.put(scholarApp.Scholarship__c,scholarApp);
            }
            list<Scholarship_Application__c> toDelete = new list<Scholarship_Application__c>();
            list<Scholarship_Application__c> scholarApplicationList = new list<Scholarship_Application__c> ();
            set<id> selectedIdSet = new set<id> ();
            for(Scholarship__c scholarshipObj : ScholarshipWrapperList){
                selectedIdSet.add(scholarshipObj.id);
                if(mapScholarshipId.containsKey(scholarshipObj.id)){
                    //UPdate
                    
                    scholarApplicationList.add(new Scholarship_Application__c(
                        id = mapScholarshipId.get(scholarshipObj.id).id,
                        stage__c = 'Applying',
                        Applicant__c = contactId,
                        Application__c = applicationId,
                        Scholarship__c = scholarshipObj.id,
                        Program_Offer__c = mapScholarshipProgram.containsKey(scholarshipObj.id) ? mapScholarshipProgram.get(scholarshipObj.id) : null,
                        Application_Date__c = (scholarshipObj.Scholarship_Application_Deadline__c != null ? scholarshipObj.Scholarship_Application_Deadline__c.date() : null),// stores date from datetime
                        Eli__c = scholarshipObj.Applicant_Eligibility_Specifications__c,
                        Amount_Asked__c  = Integer.valueOf(scholarshipObj.Scholarship_Amount_Available__c.split(' ')[1].split('%')[0])
                        // stores on numeric value from picklist value 
                    ));
                }else{
                    //NEW 
                    scholarApplicationList.add(new Scholarship_Application__c(
                        Applicant__c = contactId,
                        stage__c = 'Applying',
                        Application__c = applicationId,
                        Scholarship__c = scholarshipObj.id,
                        Program_Offer__c = mapScholarshipProgram.containsKey(scholarshipObj.id) ? mapScholarshipProgram.get(scholarshipObj.id) : null,
                        Application_Date__c = (scholarshipObj.Scholarship_Application_Deadline__c != null ? scholarshipObj.Scholarship_Application_Deadline__c.date() : null),// stores date from datetime
                        Eli__c = scholarshipObj.Applicant_Eligibility_Specifications__c,
                        Amount_Asked__c  = Integer.valueOf(scholarshipObj.Scholarship_Amount_Available__c.split(' ')[1].split('%')[0])
                        // stores on numeric value from picklist value 
                    ));
                }
                
                
            }
            // get existing records 
            for(Scholarship_Application__c scholarApp : mapScholarshipId.values())
            {
                if(!selectedIdSet.contains(scholarApp.Scholarship__c)){
                    toDelete.add(scholarApp);
                }
            }
            
            
            
            if(!toDelete.isEmpty()){
                //DELETE DESELECTED
                delete toDelete;
            }
            
            
            // UPDATE INSERT SELECTED
            upsert scholarApplicationList;
            
            //afsTriggerSwitch.By_Pass_Opportunity_Trigger__c = false;
            //update afsTriggerSwitch;
        }catch(Exception e){
            Afs_UtilityApex.logger(e.getStackTraceString());
            //afsTriggerSwitch.By_Pass_Opportunity_Trigger__c = false;
            //update afsTriggerSwitch;
            throw new AuraHandledException(e.getMessage());
        }
        
    }
    
    global class ScholarshipWrapper {
        
        @AuraEnabled
        global list<Afs_UtilityApex.PicklistWrapperUtil> Elegibility {get;set;}
        @AuraEnabled
        global list<Afs_UtilityApex.PicklistWrapperUtil> AmountAvailable {get;set;}
        @AuraEnabled
        global Boolean isSelected {get;set;}
        @AuraEnabled
        global Boolean isCreated {get;set;}
        @AuraEnabled
        global Scholarship__c scholarShipObj {get;set;}
        
        global ScholarshipWrapper(Scholarship__c scholarShipObjNew,list<Afs_UtilityApex.PicklistWrapperUtil> ElegibilityNew,list<Afs_UtilityApex.PicklistWrapperUtil> AmountAvailableNew,Boolean isSelectedNew){
            scholarShipObj = scholarShipObjNew;
            Elegibility = ElegibilityNew;
            AmountAvailable = AmountAvailableNew;
            isSelected = isSelectedNew;
            isCreated = isSelectedNew;
        }
        
    }    
}