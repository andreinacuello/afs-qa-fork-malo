@isTest
public class WaitListed_Batch_Test {
    @testSetup
    public static void setup_method(){
        //RecordTypes
        id recordTypeIdApp = [Select Id From RecordType Where DeveloperName = 'Flagship' AND SObjectType = 'application__c'].Id;
        RecordType recordTypeIdPo = [Select Id From RecordType Where DeveloperName = 'Minors'];
        RecordType recordTypeIdHp = [Select Id From RecordType Where DeveloperName = 'X18'];
        
        Account acc = Util_Test.create_AFS_Partner();
        
        Hosting_Program__c hp = new Hosting_Program__c();
        hp.name = 'Test Hosting Program';
        hp.Host_Partner__c = acc.Id;
        hp.RecordType = recordTypeIdHp; //flagship
        hp.App_Received_From__c = Date.today() + 10;
        hp.App_Received_To__c = Date.today() + 20;
        hp.Date_of_Birth_From__c = Date.today() + 30;
        hp.Date_of_Birth_To__c = Date.today() + 40;
        hp.Return_Date__c = Date.today() + 30;
        hp.Program_Content__c = 'as';
        hp.Program_Type__c = 'University';
        hp.Hemisphere__c = 'SH';
        hp.Duration__c = 'YP';
        hp.Language_of_Placement__c = 'English';
        hp.Year__c = '2019';
        insert hp;
        
        List<Program_Offer__c> lstProgramOffer = new List<Program_Offer__c>();
        Program_Offer__c po = new Program_Offer__c();
        po.name = 'Test Program Offer';
        po.Hosting_Program__c = hp.Id;
        po.Sending_Partner__c = acc.Id;
        po.Applications_Received_To_local__c = System.today() + 30;
        po.RecordType = recordTypeIdPo; //flagship
        po.Projected_Sending_Number__c = 10;
        po.Updated_Projected_Sending_Number__c = 10;
        po.Visa_Fee_numeric__c = 30;
        po.Pre_application_Fee_numeric__c = 20;
        po.Program_Price_numeric__c = 150;
        po.Flexible_Age_Range__c = 'Yes';
        lstProgramOffer.add(po);
        
        Program_Offer__c po2 = new Program_Offer__c();
        po2.name = 'Test Program Offer 2';
        po2.Hosting_Program__c = hp.Id;
        po2.Sending_Partner__c = acc.Id;
        po2.Applications_Received_To_local__c = System.today() + 30;
        po2.RecordType =  recordTypeIdPo;//flagship
        po2.Projected_Sending_Number__c = 10;
        po2.Updated_Projected_Sending_Number__c = 10;
        po2.Program_Price_numeric__c = 150;
        po2.Visa_Fee_numeric__c = 30;
        po2.Pre_application_Fee_numeric__c = 20;        
        po2.Flexible_Age_Range__c = 'Yes';
        lstProgramOffer.add(po2);
        
        insert lstProgramOffer;       
		
        contact con = new contact();
        con.lastName = 'Test contact';
        con.firstname ='Test Test contact';
        con.RecordType = [Select Id From RecordType Where DeveloperName = 'Applicant'];
        con.accountId = acc.id;
        Insert con;
        
        AFS_Triggers_Switch__c swi = Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
        
        
        List<application__c> appList = new List<application__c>();        
        for(integer i = 0; i < 3; i++){
            application__c app = new application__c();
            app.Applicant__c = con.Id;
            app.Program_Offer__c = po.Id;
            app.RecordTypeId = recordTypeIdApp;
            appList.add(app);
        }
        
        application__c app = new application__c();
        app.Applicant__c = con.Id;
        app.Program_Offer__c = po2.Id;
        app.RecordTypeId = recordTypeIdApp;
        appList.add(app);
        insert appList;
        

    }
    
    public static testMethod void executeBatch(){
        List<Program_Offer__c> lstPO = new List<Program_Offer__c>();
        list<Application__c> lstApp = new List<Application__c>();
        list<Application__History> lstAppHis = new List<Application__History>();
        Id firstAppId;
        for(Program_Offer__c po : [SELECT Id,Updated_Projected_Sending_Number__c,Applications_Received_To_local__c, (SELECT Id, Status__c, Status_App__c, Program_Offer__c FROM Applications__r) FROM Program_Offer__c]){
            if(po.Applications__r.size() == 1){
                po.Applications_Received_To_local__c = System.today() - 1;            	
            }else{
                po.Updated_Projected_Sending_Number__c = 1;
                
                for(Application__c app : po.Applications__r){
                    app.Status__c = 'Decision';
                    firstAppId = app.Id;
                    Application__History appH = new Application__History();
                    appH.Field = 'Created';
                    appH.ParentId = app.Id;
                    lstAppHis.add(appH);
                    appH = new Application__History();
                    appH.Field = 'Program_Offer__c';
                    appH.ParentId = app.Id;
                    lstAppHis.add(appH);
                    lstApp.add(app);
                }
                //Insert lstAppHis;
                //update lstApp;
            }
            lstPO.add(po);                    
        }
        Test.startTest();
        if(lstAppHis.size() > 0 && lstApp.size() > 0){
            Insert lstAppHis;
            update lstApp; 
        }
        if(lstPO.size() > 0){
            Update lstPO;            
        }
        
        Database.executeBatch(new WaitListed_Batch(), 2);
        Test.stopTest();
        for(Application__c app : [Select Id, Status_App__c From Application__c]){
            if(app.Id != firstAppId){
                system.assertEquals('Wait-listed', app.Status_App__c);
            }else{
                system.assertEquals(null, app.Status_App__c);
            }
        }
    }
}