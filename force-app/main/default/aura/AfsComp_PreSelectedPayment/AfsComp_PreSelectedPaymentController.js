({
	doSaveRecord : function(component, event, helper) {
		helper.onSaveRecord(component, event);
	},
    
    doInit : function(component, event, helper){
        component.set("v.Spinner","true"); 
        var configObj = component.get("v.configWrapper");
        helper.getDynamicCustomLabelVal(configObj.Payment_Txt_Description__c,component);
        component.set("v.Payment_Txt_Description",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.Payment_Txt_ApplicationFee__c,component);
        component.set("v.Payment_Txt_ApplicationFee",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.Payment_Txt_PaymentButton1Text__c,component);
        component.set("v.Payment_Txt_PaymentButton1Text",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.Payment_Txt_PaymentButton2Text__c,component);
        component.set("v.Payment_Txt_PaymentButton2Text",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.Payment_Txt_PaymentButton3Text__c,component);
        component.set("v.Payment_Txt_PaymentButton3Text",component.get("v.LabelTempVal"));
		helper.onInit(component,event);        
    },
    
    changeTextLine : function(component, event, helper){
        var configObj = component.get("v.configWrapper");
    	var buttonPressed = event.currentTarget.id;
    	if(buttonPressed == "button1"){
    		component.set("v.toggleButton1",true);
            component.set("v.toggleButton2",false);
            component.set("v.toggleButton3",false);
            helper.getDynamicCustomLabelVal(configObj.Payment_Txt_PaymentButton1TextLine__c,component);
            var MultiLineAttribute = component.get("v.LabelTempVal");
            if(MultiLineAttribute != undefined && MultiLineAttribute != ""){
                component.set("v.Payment_Txt_TextLine",MultiLineAttribute.split(";"));
            }
        }else if(buttonPressed == "button2"){
            component.set("v.toggleButton1",false);
            component.set("v.toggleButton2",true);
            component.set("v.toggleButton3",false);
            helper.getDynamicCustomLabelVal(configObj.Payment_Txt_PaymentButton2TextLine__c,component);
            var MultiLineAttribute = component.get("v.LabelTempVal");
            if(MultiLineAttribute != undefined && MultiLineAttribute != ""){
                component.set("v.Payment_Txt_TextLine",MultiLineAttribute.split(";"));
            }
        }else if(buttonPressed == "button3"){
            component.set("v.toggleButton1",false);
            component.set("v.toggleButton2",false);
            component.set("v.toggleButton3",true);
            helper.getDynamicCustomLabelVal(configObj.Payment_Txt_PaymentButton3TextLine__c,component);
            var MultiLineAttribute = component.get("v.LabelTempVal");
            if(MultiLineAttribute != undefined && MultiLineAttribute != ""){
                component.set("v.Payment_Txt_TextLine",MultiLineAttribute.split(";"));
            }
        }
        
    },
    
    backToList : function (component, event, helper){
        component.set("v.parentToggle",false);
    }
    
    
})