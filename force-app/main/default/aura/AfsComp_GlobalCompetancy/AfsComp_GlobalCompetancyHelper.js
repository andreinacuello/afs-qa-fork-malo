({
	onInit : function (component,event,fieldName){
    	var action = component.get("c.getPicklistValues");
        action.setParams({
            "objObject": 'Application__c',
            "fld": fieldName
        });
        var opts=[];
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var allValues = response.getReturnValue();
				component.set("v.WhyGlobal", allValues['Why_is_Global_Competence_important_for_s__c']);
                component.set("v.HowGlobal", allValues['How_to_be_a_Global_Citizen__c']);
                
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false"); 
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    doSubmitApplication : function (component,wrapperObj){
         var cmpEvent = component.getEvent("afscompnavigation");
        cmpEvent.setParam("navigateTo","4");
        
        var action = component.get("c.doSubmittApplication");
        action.setParams({
             "appObj": wrapperObj.applicationObj,
        });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                cmpEvent.fire();
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    }
})