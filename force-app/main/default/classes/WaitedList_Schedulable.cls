global class WaitedList_Schedulable implements Schedulable{
	
    global void execute(SchedulableContext sc) {      
        WaitListed_Batch batch = new WaitListed_Batch(); 
        database.executebatch(batch,10);
    }
    
    public static void SchedulerMethod() {
        String jobId;
        List<CronJobDetail> cronList=  new List<CronJobDetail>([SELECT Id, Name, JobType   FROM CronJobDetail]);
        for(CronJobDetail cronJob:cronList){
            if('WaitListed_Batch'.equals(cronJob.Name)){
                jobId = cronJob.id;
                break;
            }    
        }
        CronTrigger ct = null;
        try{
            ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime,State FROM CronTrigger WHERE CronJobDetailId = :jobId]; 
        }
        catch(Exception ex){
        }
        WaitedList_Schedulable  updateBatch = new WaitedList_Schedulable();
        if(ct == null){
        	System.schedule('WaitListed_Batch','0 0 0 ? * * *', updateBatch);
        }
    }
}