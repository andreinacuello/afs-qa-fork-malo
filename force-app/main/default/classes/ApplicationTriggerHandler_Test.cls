@isTest
public class ApplicationTriggerHandler_Test {
    @TestSetup
    public static void setup_method(){
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        Contact con = Util_Test.createContact('Test Contact',null);
        Insert con;
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
    }    
     
    public static testMethod void calculateScore_Test(){
        Contact con = [SELECT Id From Contact Limit 1];
        Program_Offer__c po = [SELECT Id From Program_Offer__c Limit 1];
        
        List<Scoring_Settings__c> lstSS = new List<Scoring_Settings__c>();
        Scoring_Settings__c ss = Util_Test.createScoringSettingsRecord('Test SS1', 'Application__c', 'Tell_us_about_yourself__c', 'Length', '5', '10',null, 10);
        lstSS.add(ss);
        ss = Util_Test.createScoringSettingsRecord('Test SS2', 'Application__c', 'What_do_you_want_out_of_this_experience__c', 'Blank', null, null,null, -5);
        lstSS.add(ss);
        ss = Util_Test.createScoringSettingsRecord('Test SS3', 'Contact', 'Name', 'Blank', null, null,null, -5);
        lstSS.add(ss);
        ss = Util_Test.createScoringSettingsRecord('Test SS4', 'Application__c', 'Interests__r', 'Child', '1', null,null, 5);
        lstSS.add(ss);
        Insert lstSS;
         
        List<Application__c> lstApplication = new List<Application__c>();
        Application__c app = Util_Test.createApplication(con,null);
        lstApplication.add(app);       
        
        Test.startTest();
        Insert lstApplication;
        app = [SELECT Score__c FROM Application__c Limit 1];
        System.assertEquals(95,app.Score__c);
        
        Program_Offer_in_App__c poa = new Program_Offer_in_App__c(Application__c = app.Id, Program_offer__c = po.Id, Contact__c = con.Id);
        Insert poa;
        app.Tell_us_about_yourself__c = 'Test 123';
        update app;
        app = [SELECT Score__c FROM Application__c Limit 1];
        Test.stopTest();
        
        System.assertEquals(110,app.Score__c);
        
    }
    
    @isTest static void updateScholarshipPreApplicationFeeByProgramOffer() {
        
        Account newAccount1 = Util_Test.create_AFS_Partner2('Test account 1'); //It's already insert
        Account newAccount2 = Util_Test.create_AFS_Partner2('Test account 2'); //It's already insert

        Contact newContact = Util_Test.create_Conact(newAccount1,'Applicant','Test newContact'); //It's already insert
        system.debug('newContact.RecordType:' + newContact.RecordType);
        Test.startTest();
        Hosting_Program__c newHostingProgram1 = Util_Test.create_Hosting_Program(newAccount1); //It's already insert
        Hosting_Program__c newHostingProgram2 = Util_Test.create_Hosting_Program(newAccount2); //It's already insert
        
        
        
        
        Scholarship__c newScholarship1 = Util_Test.create_Scholarship2(newAccount1,50,'Test Scholarship 1');
        Scholarship__c newScholarship2 = Util_Test.create_Scholarship2(newAccount2,20,'Test Scholarship 2');
        Scholarship__c newScholarship3 = Util_Test.create_Scholarship2(newAccount2,10,'Test Scholarship 3');
        List<Scholarship__c> newScholarshipList = new List<Scholarship__c>();
        newScholarshipList.add(newScholarship1);
        newScholarshipList.add(newScholarship2);
        newScholarshipList.add(newScholarship3);
        insert newScholarshipList;
        
        Id idtestproofff =  Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        list<Program_Offer__c> listProg = new list<Program_Offer__c>();
        
        Program_Offer__c newProgramOffer1 = new Program_Offer__c();
        newProgramOffer1.name = 'Test-programoff-1';
        newProgramOffer1.Durations__c = 'SM';
        newProgramOffer1.Hosting_Program__c = newHostingProgram1.Id;
        newProgramOffer1.From__c = Date.newInstance(2020, 2, 17);
        newProgramOffer1.To__c = Date.newInstance(2020, 6, 17);
        newProgramOffer1.Sending_Partner__c = newAccount1.Id;
        newProgramOffer1.Flexible_Age_Range__c = 'Yes';
        newProgramOffer1.Applications_Received_To_local__c = System.today() + 30;
        newProgramOffer1.RecordTypeId = idtestproofff; //flagship
        listProg.add(newProgramOffer1);
        Program_Offer__c newProgramOffer2 = new Program_Offer__c();
        newProgramOffer2.name = 'Test-programoff-2';
        newProgramOffer2.Durations__c = 'SM';
        newProgramOffer2.Hosting_Program__c = newHostingProgram2.Id;
        newProgramOffer2.From__c = Date.newInstance(2020, 2, 17);
        newProgramOffer2.To__c = Date.newInstance(2020, 6, 17);
        newProgramOffer2.Sending_Partner__c = newAccount2.Id;
        newProgramOffer2.Flexible_Age_Range__c = 'Yes';
        newProgramOffer2.Applications_Received_To_local__c = System.today() + 30;
        newProgramOffer2.RecordTypeId = idtestproofff; //flagship
        listProg.add(newProgramOffer2);
		insert listProg;
        
        Id RecordTypeIdApp =  Schema.SObjectType.application__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        application__c newApplication = Util_Test.create_Applicant2(newContact,newProgramOffer2,RecordTypeIdApp);
        
        Sch_in_Program__c newSchInPro1 = Util_Test.create_ScholarshipProgram(newScholarship1,newProgramOffer1);
        Sch_in_Program__c newSchInPro2 = Util_Test.create_ScholarshipProgram(newScholarship2,newProgramOffer1);
        Sch_in_Program__c newSchInPro3 = Util_Test.create_ScholarshipProgram(newScholarship3,newProgramOffer2);
        Test.stopTest();
                system.debug('newContact.recordTypeId:' + newContact.RecordType);

        Scholarship_Application__c newSchApp1 = Util_Test.create_ScholarshipApplication2(newScholarship1,newContact,newApplication,'Applying');
        Scholarship_Application__c newSchApp2 = Util_Test.create_ScholarshipApplication2(newScholarship2,newContact,newApplication,'Processing');
        Scholarship_Application__c newSchApp3 = Util_Test.create_ScholarshipApplication2(newScholarship3,newContact,newApplication,'Won');
        List<Scholarship_Application__c> newSchAppList = new List<Scholarship_Application__c>();
        newSchAppList.add(newSchApp1);
        newSchAppList.add(newSchApp2);
        newSchAppList.add(newSchApp3);
        system.debug('newSchApp1.Applicant__r.recordTypeId:' +newSchApp1.Applicant__r.RecordType);
        system.debug('newSchApp2.Applicant__r.recordTypeId:' + newSchApp2.Applicant__r.RecordType);
        system.debug('newSchApp3.Applicant__r.recordTypeId:' +newSchApp3.Applicant__r.RecordType);
        insert newSchAppList;
        
        newApplication.Program_Offer__c = newProgramOffer1.Id;
        update newApplication;
        
        
        application__c finalApp = [SELECT Id, Scholarship_Pre_Application_Fee__c FROM application__c WHERE Id =: newApplication.Id];
        
        system.assertEquals(70, finalApp.Scholarship_Pre_Application_Fee__c);
    }   
    
    public static testMethod void generateNewUUID_Test(){
        Contact con = [SELECT Id From Contact Limit 1];
        Application__c app = Util_Test.createApplication(con,null);
        Insert app;
        app = [SELECT Global_Link_Participant_App_ID__c, GL_Service_Id__c FROM Application__c WHERE Id = :app.Id];
        System.assert(app.Global_Link_Participant_App_ID__c != NULL);
		System.assert(app.GL_Service_Id__c != NULL);        
    }
    
    public static testMethod void sendAppContactToGlobalLink_Test(){
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        Contact con = [SELECT Id From Contact Limit 1];
        Program_Offer__c po = [SELECT Id From Program_Offer__c Limit 1];
        
        Application__c app = Util_Test.createApplication(con,po);
        Insert app;
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name = 'Applicant Community Member Profile' Limit 1];
		User user1 = new User(
    	Username = System.now().millisecond() + 'test12345@test.com',
    	ContactId = con.Id,
    	ProfileId = portalProfile.Id,
    	Alias = 'test123',
    	Email = 'test12345@test.com',
    	EmailEncodingKey = 'UTF-8',
    	LastName = 'McTesty',
        CommunityNickname = 'test12345',
        TimeZoneSidKey = 'America/Los_Angeles',
        LocaleSidKey = 'en_US',
        LanguageLocaleKey = 'en_US');
        insert user1;
		app.Status__c = 'Decision';
		Update app;    
        test.startTest();
        	app.Status__c = 'Cancelled';
        	app.Cancellation_Reason__c = 'Became Non-Responsive';
			Update app;
        test.stopTest();
        system.assertEquals('Applicant Community Profile Login',[SELECT Profile.Name FROM User WHERE id =: user1.id].Profile.Name);
    }
    
    @isTest public static void getTravelDataFromGL_Test(){        
        Contact con = [SELECT Id From Contact Limit 1];
        Program_Offer__c po = [SELECT Id From Program_Offer__c Limit 1];
        
        Test.startTest();
        Application__c app = Util_Test.createApplication(con,po);   		
        Insert app;       
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{"response":[{"statusCode":"200","pageSize":"50"}],"serviceAndOAList":[{"id":"0429B6AD-2976-4A1B-934C-00001A30FE5F","BV_RECORD_LOCATOR":"Test"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
       
        app.Get_Travel_Data_From_Global_Link__c = false;
        Update app;
        Test.stopTest();
		
        app = [SELECT Get_Travel_Data_From_Global_Link__c FROM Application__c WHERE Id = :app.Id];
        System.assert(!app.Get_Travel_Data_From_Global_Link__c);
        
    }
}