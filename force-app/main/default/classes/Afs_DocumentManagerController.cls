global without sharing class Afs_DocumentManagerController {
    
    
    @AuraEnabled
    global static list<String> getDownloadForm(String recId){
        list<String> fileData = new list<String>();
        if(String.isNotEmpty(recId)){
            for(ContentVersion fileObj : [SELECT id,VersionData,ContentDocumentId,FileType,Title  FROM ContentVersion WHERE ContentDocumentId =: recId]){
                fileData.add('data:application/octet-stream;base64,' + EncodingUtil.base64Encode(fileObj.VersionData));
                fileData.add(fileObj.Title + '.' + fileObj.FileType.toLowerCase());
            }
        }
        return fileData;
        
    }
    
    @AuraEnabled
    //RecId - To do item record id
    global static List<ContentDocumentLink> getFiles (string recId){
        List<ContentDocumentLink> lstFiles = [SELECT ContentDocumentId, ContentDocument.Title FROM ContentDocumentLink 
                                              WHERE LinkedEntityId  = :recId];
        return lstFiles;
    }
    
    @AuraEnabled
    //contentDocumentIds - ContentDocument id 
    //toDoItem  - To do item record
    global static String updateDocumentLink (list<String> contentDocumentIds,To_Do_Item__c toDoItem){
        String url = '';
        Integer count = 0;
        String prefix = '';
        Set<id> contentDocumentsExist = new Set<id>();
        // DOne to update the name of the file uploaded
        list<ContentDocument> contentDocuments = [SELECT id,Title FROM ContentDocument WHERE id IN: contentDocumentIds];
        
        if(toDoItem.SUBMIT_YOUR_HEALTH_CERTIFICATE_BY__c){
            prefix = 'Health';
        }else if(toDoItem.SUBMIT_YOUR_ACADEMIC_HISTORY__c){
            prefix = 'Academic';
        }else if(toDoItem.SUBMIT_YOUR_HEALTH_UPDATE_FORM__c){
            prefix = 'HealthUpdate';
        }else if(toDoItem.SIGN_PARTICIPATION_AGREEMENT_BY__c){
            prefix = 'Agreement';
        }
        for(ContentDocumentLink contentLinks :[SELECT id,ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId =: toDoItem.Application__c]){
            contentDocumentsExist.add(contentLinks.ContentDocumentId);
        }
        
        if(!contentDocumentsExist.isEmpty()){
            list<ContentDocument> ContentDocumentsExisting = [SELECT id,Title FROM ContentDocument WHERE id IN: contentDocumentsExist AND Title LIKE : prefix+'%' ORDER BY Title DESC];
            if(!ContentDocumentsExisting.isEmpty()){
                String titleStr = ContentDocumentsExisting[0].Title.replace(prefix,'');
                try{
                    count = integer.valueOf(titleStr) + 1;
                }catch(Exception e){
                    count = 0;
                }
            }
        }
        
        for(ContentDocument conDoc : contentDocuments){
            conDoc.Title = (count > 10) ? prefix + count : prefix + '0' + count;
            count++;
        }
        
        update contentDocuments;
        
        if(contentDocumentIds != null && !contentDocumentIds.isEmpty()){
            //String baseUrl = System.URL.getOrgDomainUrl().toExternalForm();
            //toDoItem.File_link__c = baseUrl + '/' + contentDocumentIds[contentDocumentIds.size() - 1];
            toDoItem.File_link__c = '/' + contentDocumentIds[contentDocumentIds.size() - 1];
            update toDoItem;
            url ='/' + contentDocumentIds[contentDocumentIds.size() - 1];
        }
        return url;
    }
    
    @AuraEnabled
    //fileId - ContentDocument ID
    global static To_Do_Item__c deleteFileSF (string fileId, To_Do_Item__c recId){
        
        delete [SELECT Id 
                FROM ContentDocument 
                WHERE Id  = :fileId];
        if(String.isNotBlank(recId.File_link__c) && recId.File_link__c.contains(fileId)){
            recId.File_link__c = null;
            update recId;
        }
        return recId;
        
    }
    
    @AuraEnabled
    //RecId - To do item record id
    global static void updateParent (To_Do_Item__c recId){
        
        recId.Status__c = 'In Review';
        update recId;
    }
    
    @AuraEnabled
    //RecId - To do item record id
    global static void completeTask(To_Do_Item__c recId){
        recId.Status__c = 'Completed';
        update recId;
    }
    
    @AuraEnabled
    //Complete Due Date Field
    global static void completeDueDate(List<To_Do_Item__c> toDoList){
        list<To_Do_Item__c> listAllNewStage = new list<To_Do_Item__c>();
        for(To_Do_Item__c tdi : toDoList){
            if(tdi.Due_Date__c == null && tdi.When__c == 'After Entering Stage of Portal'){
                tdi.Due_Date__c =  date.today() + Integer.valueOf(tdi.Days__c);
                listAllNewStage.add(tdi);
            }
        }
        if(listAllNewStage.size()>0){
            system.debug('update listAll='+listAllNewStage);
            update listAllNewStage;
        } 
    }
    
    @AuraEnabled
    //RecId - To do item record id
    global static List<DocumentWrapper> getDocumentList (string appId, string stage){
        // Code added to handle picklist translation
        map<String,String> mapPicklistValVsLabel = new map<String,String>();
        mapPicklistValVsLabel.putAll(Afs_UtilityApex.getPicklistValMap('To_Do_Item__c','To_Do_Label__c')); 
        mapPicklistValVsLabel.putAll(Afs_UtilityApex.getPicklistValMap('To_Do_Item__c','When__c')); 
        system.debug('stage='+stage);
        List<DocumentWrapper> docList = new List<DocumentWrapper>();
        list<Application__c> appList = [SELECT id,Program_Offer__c,Status__c,Status_App__c FROM Application__c WHERE id =: appId];
        
        String query = 'SELECT id,Name,RecordType.Name,';
        for(String fieldAPIName : Afs_UtilityApex.getFields('To_Do_Item__c',true)){
            query += fieldAPIName + ' ,';
        }
        query = query.removeEnd(',');
        query += ' FROM To_Do_Item__c WHERE Application__c  = :appId ';
        //AND Stage_of_portal__c = :stage
        
        List<To_Do_Item__c> lst = new list<To_Do_Item__c> ();// = Database.query(query);
        List<To_Do_Item__c> lstAll = new list<To_Do_Item__c> (); // List to hold all items of that stage
        //Configuration FIle 
        String communityId =  Network.getNetworkId();
        String queryStr = 'SELECT id, Name, Afs_ToDoStatusComplete__c,To_Do_Date_Format__c';
        if(!Test.isRunningTest()){
            queryStr += ' FROM AFS_Community_Configuration__c WHERE Unique_Id__c =: communityId';
        }else{
            queryStr += ' FROM AFS_Community_Configuration__c';
        }
        set<String> statusTodDoComplete = new set<String>();
        list<AFS_Community_Configuration__c> listCommunityConfig = new list<AFS_Community_Configuration__c>();
        if(String.isNotBlank(communityId) || Test.isRunningTest()){
            
            listCommunityConfig =  Database.query(queryStr);
            if(!listCommunityConfig.isEmpty()){
                if(String.isNotBlank(listCommunityConfig[0].Afs_ToDoStatusComplete__c)){
                    statusTodDoComplete.addAll(listCommunityConfig[0].Afs_ToDoStatusComplete__c.split(';'));
                }
            }
            
            if(statusTodDoComplete.isEmpty()){
                statusTodDoComplete.add('Completed');
            }
        }
        // QUERY PROGRAMS DESIRED
        // FOR Status__c = Decision
        
        if(appList[0].Status__c == 'Decision'){
            // FIRST GET PAYMENT AND CAMPUS SELECTION
            String queryTemp = query + 'AND Stage_of_portal__c = \'1. Stage: Pre-application\' AND Status__c NOT IN: statusTodDoComplete ORDER BY Due_Date__c NULLS LAST '; // past code: ORDER BY Due_Date__c
            lst = Database.query(queryTemp);
            String queryAllTemp = query + 'AND Stage_of_portal__c = \'1. Stage: Pre-application\' ORDER BY Due_Date__c NULLS LAST '; // past code: ORDER BY Due_Date__c
            lstAll = Database.query(queryAllTemp);
            if(test.isRunningTest() && stage == 'Test'){
                To_Do_Item__c obj = new To_Do_Item__c();
                obj.Name = 'sd'; 
                obj.Type__c = 'Task'; 
                obj.Stage_of_portal__c = '1. Stage: Pre-application'; 
                obj.Application__c  = appId; 
                obj.Status__c = 'Pending'; 
                obj.Due_Date__c = Date.today().addDays(10); 
                lst.add(obj);
                lstAll.add(obj);
            }
            // Change Status Of application to confirmation
            if(lst.isEmpty()){
                // FOR '2. Stage: Pre-selected' STAGE
                queryTemp = query + 'AND Stage_of_portal__c = \'2. Stage: Pre-selected\' AND Status__c NOT IN: statusTodDoComplete ORDER BY Due_Date__c NULLS LAST '; 
                lst = Database.query(queryTemp);
                
                queryAllTemp = query + 'AND Stage_of_portal__c = \'2. Stage: Pre-selected\' ORDER BY Due_Date__c NULLS LAST ';
                lstAll = Database.query(queryAllTemp);
                
                
                if(!lst.isEmpty()){
                    completeDueDate(lstAll);
                    appList[0].Status__c = 'Confirmation';
                    update appList;
                }else{
                    // FIRST GET PAYMENT AND CAMPUS SELECTION
                    queryTemp = query + 'AND Stage_of_portal__c = \'1. Stage: Pre-application\' AND Status__c IN: statusTodDoComplete ORDER BY Due_Date__c NULLS LAST ';
                    lst = Database.query(queryTemp);
                    queryAllTemp = query + 'AND Stage_of_portal__c = \'1. Stage: Pre-application\' ORDER BY Due_Date__c NULLS LAST ';
                    
                    
                    
                    lstAll = Database.query(queryAllTemp); 
                }
                
            }else{
                //Cuando no hay ningun to do item con un stage of portal igual a '1. Stage: Pre-application' y un Status de 'Complete' 
                //Se popula el Campo de Due Date al momento de entrar en esta condicion y si el campo se encuentra vacio 
                completeDueDate(lstAll);
            }
        }else if(appList[0].Status__c == 'Confirmation'){
            // FOR '2. Stage: Pre-selected' STAGE
            String queryTemp = query + 'AND Stage_of_portal__c = \'2. Stage: Pre-selected\' AND Status__c NOT IN: statusTodDoComplete ORDER BY Due_Date__c NULLS LAST ';
            lst = Database.query(queryTemp);
            String queryAllTemp = query + 'AND Stage_of_portal__c = \'2. Stage: Pre-selected\' ORDER BY Due_Date__c NULLS LAST ';
            lstAll = Database.query(queryAllTemp);
            // IF '2. Stage: Pre-selected' ALL ARE COMPLETE THAN CHECK
            // FOR '- Extra documentation needed' STAGE
            System.debug('queryAllTemp: ' + queryAllTemp);
            
            if(lst.isEmpty()){
                queryTemp = query + 'AND Stage_of_portal__c = \'- Extra documentation needed\' ORDER BY Due_Date__c NULLS LAST ';
                lst = Database.query(queryTemp);
                queryAllTemp = query + 'AND Stage_of_portal__c = \'- Extra documentation needed\' ORDER BY Due_Date__c NULLS LAST ';
                lstAll = Database.query(queryAllTemp);
                
                if(lst.isEmpty()){
                    // FOR '2. Stage: Pre-selected' STAGE
                    queryTemp = query + 'AND Stage_of_portal__c = \'2. Stage: Pre-selected\' AND Status__c IN: statusTodDoComplete ORDER BY Due_Date__c NULLS LAST ';
                    lst = Database.query(queryTemp);
                    queryAllTemp = query + 'AND Stage_of_portal__c = \'2. Stage: Pre-selected\' ORDER BY Due_Date__c NULLS LAST ';
                    lstAll = Database.query(queryAllTemp);
                    
                    
                }else{
                    completeDueDate(lstAll); 
                }
            }else{
                completeDueDate(lstAll); 
            }
        }else if(appList[0].Status__c == 'Purchase'){
            // QUERY PROGRAMS DESIRED
            // FOR Accepted
            String queryTemp = query + 'AND Stage_of_portal__c = \'3. Stage: Accepted\'  AND Status__c NOT IN: statusTodDoComplete ORDER BY Due_Date__c NULLS LAST ';
            lst = Database.query(queryTemp);
            String queryAllTemp = query + 'AND Stage_of_portal__c = \'3. Stage: Accepted\' ORDER BY Due_Date__c NULLS LAST ';
            lstAll = Database.query(queryAllTemp);
            // Change Status Of application to Onboarding
            if(lst.isEmpty()){
                // FOR '4. Stage: Get Ready' STAGE
                queryTemp = query + 'AND Stage_of_portal__c = \'4. Stage: Get Ready\' ORDER BY Due_Date__c NULLS LAST ';
                lst = Database.query(queryTemp);
                lstAll.clear();
                lstAll.addAll(lst);
                if(!lst.isEmpty()){
                    completeDueDate(lstAll);
                    appList[0].Status__c = 'Onboarding';
                    update appList;
                }else{
                    // FOR Accepted
                    queryTemp = query + 'AND Stage_of_portal__c = \'3. Stage: Accepted\'  AND Status__c IN: statusTodDoComplete ORDER BY Due_Date__c NULLS LAST';
                    lst = Database.query(queryTemp);
                    queryAllTemp = query + 'AND Stage_of_portal__c = \'3. Stage: Accepted\' ORDER BY Due_Date__c NULLS LAST';
                    lstAll = Database.query(queryAllTemp);
                }
            }else{
                completeDueDate(lstAll);
            }
        }else if(appList[0].Status__c == 'Onboarding'){
            // FOR '4. Stage: Get Ready' STAGE
            String queryTemp = query + 'AND Stage_of_portal__c = \'4. Stage: Get Ready\' ORDER BY Due_Date__c NULLS LAST ';
            lst = Database.query(queryTemp);
            lstAll.addAll(lst);
            completeDueDate(lstAll);
        }
        
        if(lst.size() > 0){
            for(To_Do_Item__c item : lstAll){
                
                System.debug('ITEM: ' + item.Name);
                docList.add(new DocumentWrapper(item,mapPicklistValVsLabel.get(item.To_Do_Label__c),mapPicklistValVsLabel.get(item.When__c),listCommunityConfig[0].To_Do_Date_Format__c));           
            }
            docList.sort();
        }
        
        return docList;
    }
}