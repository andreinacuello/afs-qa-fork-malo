@IsTest
global class RemoteSiteConnectionMockFailed implements HttpCalloutMock {
	global HTTPResponse respond(HTTPRequest request) {
        // Create a Exception response
        CalloutException e = (CalloutException)CalloutException.class.newInstance();
        e.setMessage('Unauthorized endpoint, please check Setup->Security->Remote site settings.');
        throw e;
    }
}