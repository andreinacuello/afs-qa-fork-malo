public class PaymentInstallmentsTriggerHandler {


    public static void process() {
    
        Map<Id, List<Payment_Entry__c>> mapApplicationToPaymentEntry = new Map<Id, List<Payment_Entry__c>>();
        Map<Id, Map<Id,Decimal>> mapApplicationToPaymentInstallment = new Map<Id, Map<Id,Decimal>>();
        
        List<Payment_Entry__c> listPaymentEntry = [SELECT Id, Application__c, Amount__c
                                        FROM Payment_Entry__c  
                                        WHERE Id IN: Trigger.new 
                                        ORDER BY Application__c];
    
        for (Payment_Entry__c paymentEntry : listPaymentEntry) {
            if (!mapApplicationToPaymentEntry.containsKey(paymentEntry.Application__c)) {
                mapApplicationToPaymentEntry.put(paymentEntry.Application__c, new List<Payment_Entry__c>());
            } 
            mapApplicationToPaymentEntry.get(paymentEntry.Application__c).add(paymentEntry);
        }
        
        List<Payment_Installment__c> listInstallments = [SELECT Id, Application__c, Number__c, Amount__c, Paid_Amount__c,
                                        Balance__c  
                                        FROM Payment_Installment__c 
                                        WHERE Application__c IN: mapApplicationToPaymentEntry.keySet()
                                        AND Balance__c != 0
                                        ORDER BY Number__c asc];
        
        
        for (Payment_Installment__c installment : listInstallments) {
            if (!mapApplicationToPaymentInstallment.containsKey(installment.Application__c)) {
                mapApplicationToPaymentInstallment.put(installment.Application__c, new Map<Id,Decimal>());
            } 
            mapApplicationToPaymentInstallment.get(installment.Application__c).put(installment.Id, installment.Balance__c);
        }
        
        List<Payment_Installment_per_Entry__c> listPaymentInstallmentByPaymentEntry = new List<Payment_Installment_per_Entry__c>();
        
        for (Id applicationId : mapApplicationToPaymentEntry.keySet()) {
        
            if (mapApplicationToPaymentInstallment.containsKey(applicationId)) {
                
                for (Payment_Entry__c paymentEntry : mapApplicationToPaymentEntry.get(applicationId)) {
                    
                    Decimal remporalPayment = paymentEntry.Amount__c;
                    
                    for (Id installmentId : mapApplicationToPaymentInstallment.get(applicationId).keySet()) {
                        
                        Decimal balance = mapApplicationToPaymentInstallment.get(applicationId).get(installmentId);
                        
                        if (balance == 0) {
                            continue;
                        } 
                        
                        Payment_Installment_per_Entry__c installmentByPaymentEntry = new Payment_Installment_per_Entry__c();
                        installmentByPaymentEntry.Payment_Installment__c = installmentId;
                        installmentByPaymentEntry.Payment_Entry__c = paymentEntry.Id;
                        
                        // Pagamos toda la cuota y gastamos toda la plata del pago
                        if (remporalPayment == balance) {
                            installmentByPaymentEntry.Amount__c = remporalPayment;
                            mapApplicationToPaymentInstallment.get(applicationId).put(installmentId, 0);
                            listPaymentInstallmentByPaymentEntry.add(installmentByPaymentEntry);
                            break;
                        } else if (remporalPayment < balance) {
                            installmentByPaymentEntry.Amount__c = remporalPayment;
                            mapApplicationToPaymentInstallment.get(applicationId).put(installmentId, balance - remporalPayment);
                            listPaymentInstallmentByPaymentEntry.add(installmentByPaymentEntry);
                            break;
                        } else {
                            installmentByPaymentEntry.Amount__c = balance;
                            remporalPayment = remporalPayment - balance;
                            listPaymentInstallmentByPaymentEntry.add(installmentByPaymentEntry);
                        }
                    }
                }               
            }
        }
        
        if (!listPaymentInstallmentByPaymentEntry.isEmpty()) {
            insert listPaymentInstallmentByPaymentEntry;
        }
        
    }

    public static void dummy(){
        integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;

    }

}