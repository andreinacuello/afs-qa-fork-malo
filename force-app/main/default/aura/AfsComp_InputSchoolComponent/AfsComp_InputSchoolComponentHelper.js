({
    getPossibleLocations: function(component, searchKey, Language) {
        var action = component.get("c.getAddressAutoCompleteMerge");
        action.setParams({
            input: searchKey,
            types: "(regions)",
            langug: Language
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            console.log(state)
            if (state === "SUCCESS") {
                const predictions = JSON.parse(response.getReturnValue());
                var addresses = [];
                console.log(response.getReturnValue());
                if (predictions.length > 0) {
                    for (var i = 0; i < predictions.length; i++) {
                        console.log(predictions[i].description)
                        addresses.push({
                            value: predictions[i].types[0],
                            PlaceId: predictions[i].place_id,
                            country: predictions[i].types.pop(),
                            label: predictions[i].description
                        });
                    }
                    component.set("v.predictions", addresses);
                }
            }
        });
        $A.enqueueAction(action);
    },
    selectedOption: function(component, name, placeId) {
        component.set("v.locationSelected", name);
        component.set("v.predictions", []); // this hides the prediction list once
        var action = component.get("c.getAddressDetails");
        action.setParams({
            PlaceId: placeId,
            lang: ""
        });
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS") {
                const school = this.parseAddressFromGoogle(
                    component,
                    response.getReturnValue()
                );
                console.log(school)
                component.set("v.mapMarkers", [{
                    location: {
                        Latitude: school.BillingLatitude,
                        Longitude: school.BillingLongitude
                    },

                    title: school.Name
                }]);
                component.set("v.School", school);
                component.set("v.zoomLevel", 16);
            }
        });
        $A.enqueueAction(action);
    },

    parseAddressFromGoogle: function(component, addressFromGoogle) {
        var details = JSON.parse(addressFromGoogle).result;
        var address = details.address_components.reduce(
            (acum, currentValue) => {
                currentValue.types.map(type => {
                    switch (type) {
                        case "street_number":
                            acum.street = currentValue.long_name;
                            break;
                        case "route":
                            acum.street = acum.street ?
                                `${acum.street} ${currentValue.long_name}` :
                                currentValue.long_name;
                            break;
                        case "administrative_area_level_1":
                            acum.state = currentValue.long_name;
                            break;
                        case "administrative_area_level_2":
                            acum.city = currentValue.long_name;
                            break;
                        case "locality":
                            acum.city = currentValue.long_name;
                            break;
                        case "country":
                            acum.country = currentValue.long_name;
                            break;
                        case "postal_code":
                            acum.postal_code = currentValue.long_name;
                            break;
                    }
                });
                return acum;
            }, {
                street: null,
                city: null,
                state: null,
                country: null,
                postal_code: null
            }
        );
  
			var schoolName = 'jamon'
    
            if (component.get("v.schoolManualInput")){
                schoolName = component.get("v.schoolNameManualInput")
            }	else{
                schoolName = details.name
            }
  
        return {
            Id: null,
            Name: schoolName,
            BillingStreet: address.street,
            BillingCity: address.city,
            BillingState: address.state,
            BillingPostalCode: address.postal_code,
            BillingLatitude: details.geometry.location.lat,
            BillingLongitude : details.geometry.location.lng,
            BillingCountry: address.country,
            GMPlace_Id__c : details.place_id,
            formatted_address: details.formatted_address,
            Website : details.website,
            Phone: details.international_phone_number
        };
    },

    startTimeOut: function(component) {
        component.set("v.showCantFindSchoolBtn", false);
        clearTimeout(window.timeOut);
        window.timeOut = setTimeout(function() {
            component.set("v.showCantFindSchoolBtn", true);
        }, 4000);
    },

    getSchoolInfo: function(component, schoolApp) {
        var action = component.get("c.getMySchool");
        action.setParams({
            schoolId: schoolApp.School__c ?
                schoolApp.School__c : ""
            
        });
        

        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {

                //hacer verificacion para mostrarlos solo si son distintos de null
                const school = response.getReturnValue();
                component.set("v.School", school);
                if(school.Name){
                    component.set("v.locationSelected",school.Name);
                	component.set("v.mapMarkers", [{
                        location: {
                            Latitude: school.BillingLatitude,
                            Longitude: school.BillingLongitude
                        },
                        title: school.Name
                	}])
                }
            }
        });
        $A.enqueueAction(action);
    }
});