public class ScoringHandler {
    public static List<Scoring_Settings__c> lstAllScoringSettings = Scoring_Settings__c.getAll().values();    
    
    public static List<Scoring_Settings__c> getListScoringSettingByObject(String strObjectName){
        List<Scoring_Settings__c> lstScoringSettingResult = new List<Scoring_Settings__c>();
        for(Scoring_Settings__c ss : lstAllScoringSettings){
            if(ss.isActive__c && ss.Object_Name__c == strObjectName){
                lstScoringSettingResult.add(ss);
            }
        }
        return lstScoringSettingResult;
    }
    
    public static Boolean anyFieldChange(sObject newObj,sObject oldObj,String typeObject){
        Boolean anyChange = false;
        Set<String> setFields = Schema.getGlobalDescribe().get(typeObject).getDescribe().fields.getMap().keySet();
        Set<String> setFieldsUpper = new Set<String>();
        for(String str : setFields){
        	setFieldsUpper.add(str.toUpperCase());
        }
        
        for(Scoring_Settings__c ss : lstAllScoringSettings){
            if(ss.isActive__c && ss.Object_Name__c == typeObject && !ss.Condition__c.equalsIgnoreCase('Childs')){
                if(setFieldsUpper.contains(ss.Field_Api_Name__c.ToUpperCase()) && newObj.get(ss.Field_Api_Name__c) != oldObj.get(ss.Field_Api_Name__c)){
                    anyChange = true;
                }else if(ss.Field_Api_Name__c.contains(',')){
                    for(String field : ss.Field_Api_Name__c.split(',')){
                        if(setFieldsUpper.contains(field.toUpperCase())){
                            anyChange = true;
                            break;
                        }
                    }
                }
                if(anyChange){
                    break;
                }
            }
        }       
        
        return anyChange;
    }
    
    public static Decimal recalculateScore(sObject obj, List<Scoring_Settings__c> lstScoringSetting,Decimal partialScore,String typeObject,sObject objWithRel){
        Decimal finalScore = (partialScore == null) ? 100 : partialScore;
        
        Set<String> setFields = Schema.getGlobalDescribe().get(typeObject).getDescribe().fields.getMap().keySet();
        Set<String> setFieldsUpper = new Set<String>();        
        for(String str : setFields){
        	setFieldsUpper.add(str.toUpperCase());
        }
        
        Set<String> setRelationshipsName = new Set<String>();
        for(Schema.ChildRelationship chRel : Schema.getGlobalDescribe().get(typeObject).getDescribe().getChildRelationships()){
            if(chRel.getRelationshipName() != null){
            	setRelationshipsName.add(chRel.getRelationshipName().toUpperCase());
            }
        }
        
                   
        for(Scoring_Settings__c ss : lstScoringSetting){          
            if(setFieldsUpper.contains(ss.Field_Api_Name__c.toUpperCase())){
                if((ss.Condition__c.equalsIgnoreCase('Blank') && String.isBlank((String)obj.get(ss.Field_Api_Name__c)))
                  	|| (ss.Condition__c.equalsIgnoreCase('Length') && (String)obj.get(ss.Field_Api_Name__c) != null && ss.Parameter_1__c != null && ((String)obj.get(ss.Field_Api_Name__c)).length() >= Integer.ValueOf(ss.Parameter_1__c) && (ss.Parameter_2__c == null || ((String)obj.get(ss.Field_Api_Name__c)).length() <= Integer.ValueOf(ss.Parameter_2__c)))
                    || (ss.Condition__c.equalsIgnoreCase('Boolean') && ss.Parameter_1__c != null  && String.ValueOf((Boolean)obj.get(ss.Field_Api_Name__c)).equalsIgnoreCase(ss.Parameter_1__c))
                    || (ss.Condition__c.equalsIgnoreCase('Value') && (String)obj.get(ss.Field_Api_Name__c) != null && ss.Parameter_1__c != null && ((String)obj.get(ss.Field_Api_Name__c)).equalsIgnoreCase(ss.Parameter_1__c))
                  	|| (ss.Condition__c.equalsIgnoreCase('Not Blank') && !String.isBlank((String)obj.get(ss.Field_Api_Name__c)))){                  	
                        finalScore += ss.points__c;               
                }
            }else if(setRelationshipsName.contains(ss.Field_Api_Name__c.toUpperCase()) && ss.Condition__c.equalsIgnoreCase('Child')){
                sObject auxObj = objWithRel != null ? objWithRel : obj;
                if((auxObj.getSObjects(ss.Field_Api_Name__c) != null && ss.Parameter_1__c != null && (auxObj.getSObjects(ss.Field_Api_Name__c)).size() >= Integer.ValueOf(ss.Parameter_1__c) && (ss.Parameter_2__c == null || (auxObj.getSObjects(ss.Field_Api_Name__c)).size() <= Integer.ValueOf(ss.Parameter_2__c)))
                   || (auxObj.getSObjects(ss.Field_Api_Name__c) == null && ss.Parameter_1__c != null && Integer.ValueOf(ss.Parameter_1__c) == 0)){
                       finalScore += ss.points__c; 	
                }
            }else if(ss.Field_Api_Name__c.contains(',')){
                Integer count = 0;  
                for(String field : ss.Field_Api_Name__c.split(',')){
                    if(setFieldsUpper.contains(field.toUpperCase())){
                        if(ss.Condition__c.equalsIgnoreCase('Count') && (String)obj.get(field) != null && ss.Parameter_1__c != null && ss.Parameter_1__c.toUpperCase().contains(((String)obj.get(field)).toUpperCase())){
                            count++;
                        }
                    }
                }
                if(ss.Condition__c.equalsIgnoreCase('Count') && ss.Parameter_2__c != null && count >= Integer.ValueOf(ss.Parameter_2__c) && (ss.Parameter_3__c == null || count <= Integer.ValueOf(ss.Parameter_3__c))){
                    finalScore += ss.points__c;
                }
            }
        }
        return finalScore;
    }
    
    public static List<String> getFieldsOnScoringSettingByObjectName(String typeObject){
        List<String> lstFields = new List<String>();
        Set<String> setFields = Schema.getGlobalDescribe().get(typeObject).getDescribe().fields.getMap().keySet();
        Set<String> setFieldsUpper = new Set<String>();        
        for(String str : setFields){
        	setFieldsUpper.add(str.toUpperCase());
        }
        
        Set<String> setRelationshipsName = new Set<String>();
        for(Schema.ChildRelationship chRel : Schema.getGlobalDescribe().get(typeObject).getDescribe().getChildRelationships()){
            if(chRel.getRelationshipName() != null){
            	setRelationshipsName.add(chRel.getRelationshipName().toUpperCase());
            }
        }
        
        for(Scoring_Settings__c ss : lstAllScoringSettings){
            if(ss.isActive__c && ss.Object_Name__c == typeObject){
                if(setFieldsUpper.contains(ss.Field_Api_Name__c.toUpperCase()) && !lstFields.contains(ss.Field_Api_Name__c)){
                	lstFields.add(ss.Field_Api_Name__c);
                }else if(setRelationshipsName.contains(ss.Field_Api_Name__c.toUpperCase())){
                    String auxStr = '(SELECT Id FROM ' + ss.Field_Api_Name__c + ')';
                    if(!lstFields.contains(auxStr)){
                        lstFields.add(auxStr);
                    }                    
                }else if(ss.Field_Api_Name__c.contains(',')){
                    for(String field : ss.Field_Api_Name__c.split(',')){
                        if(setFieldsUpper.contains(field.toUpperCase()) && !lstFields.contains(field)){
                            lstFields.add(field);
                        }
                    }
                }
            }
        }
     
        return lstFields;
    }
}