@isTest
public class Afs_IntroductionCtrlTest {

    
     @testSetup
    public static void setup_test(){
        Afs_TestDataFactory.createTriggerSwitch();
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        contact contactParent = Util_Test.create_Conact(acc, 'Applicant','Test contact');
        contact relationContact =  Util_Test.create_Conact(acc, 'General','Test Relation');
        npe4__Relationship__c relation = Util_Test.create_RelationRec(contactParent , relationContact);
        application__c application = Util_Test.create_Applicant( contactParent,po);
       	ContentDocumentLink	 doc= Util_Test.create_Document(application.Id);
    }		
    
    public static testMethod void saveApplicantIntroductionTest(){
        Test.startTest(); 
		List<application__c>  application  = [select Id from application__c limit 1] ;       
        Afs_IntroductionCtrl.saveApplicantIntroduction(application[0], null);
        Test.stopTest(); 
         System.assertEquals(1,application.size());
    }
    
    public static testMethod void saveApplicantIntroductionExpTest(){
        Test.startTest(); 
		List<application__c>  application  = [select Id from application__c limit 1] ;  
         try{
            application[0].id = null;
        	Afs_IntroductionCtrl.saveApplicantIntroduction(application[0], null);
        }catch(Exception ex){           
       	}
        Test.stopTest(); 
         System.assertEquals(1,application.size());
    }
    
     public static testMethod void getAttachmentTest(){
        Test.startTest(); 
		List<application__c>  application  = [select Id from application__c limit 1] ; 
        Afs_IntroductionCtrl.getAttachment(application[0].Id);
        Test.stopTest(); 
        System.assertEquals(1,application.size());
     }
	 public static testMethod void deleteAttachmentTest(){
        Test.startTest(); 
		List<application__c>  application  = [select Id from application__c limit 1] ; 
        List<ContentDocumentLink>  doc  = [select Id from ContentDocumentLink  where LinkedEntityId = : application[0].Id] ; 
       	Afs_IntroductionCtrl.deleteAttachment(doc[0].Id, application[0].Id);
        Test.stopTest(); 
        System.assertEquals(1,application.size());
     }	
 	public static testMethod void deleteAttachmentExpTest(){
        Test.startTest(); 
        List<application__c>  application  = [select Id from application__c limit 1] ; 
        try{
            application__c app  = application[0];
            app.Id = null;
            Afs_IntroductionCtrl.deleteAttachment(null, null);
            
        }catch(Exception ex){           
       	}
    	Test.stopTest(); 
        System.assertEquals(1,application.size());
     }	    

}