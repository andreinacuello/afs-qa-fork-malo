global class WaitListed_Batch implements Database.Batchable<sObject>{
	global final String Query;
 
    global Database.QueryLocator start(Database.BatchableContext BC){
         return Database.getQueryLocator([SELECT Id,Updated_Projected_Sending_Number__c,Applications_Received_To_local__c, 
                                          (SELECT Id, Status__c, Status_App__c, Program_Offer__c FROM Applications__r) FROM Program_Offer__c]);
     }
    
    global void execute(Database.BatchableContext BC, List<Program_Offer__c> scope){
        List<Application__c> lstAppToUpdate = new List<Application__c>();
        for(Program_Offer__c po : scope){
            if(po.Applications_Received_To_local__c < System.today()){
                for(Application__c app : po.Applications__r){                    
                    app.Status_App__c = 'Wait-listed'; 
                }
                lstAppToUpdate.addAll(po.Applications__r);
            }else if(po.Updated_Projected_Sending_Number__c < po.Applications__r.size()){                
                map<Id,Application__c> mapAppByIdToProcess = new map<Id,Application__c>();
                set<Id> setIdAppJustProcessed = new Set<Id>();
                set<String> fieldSet = new set<String>{'Created','Program_Offer__c'};
                Decimal countToPassToWaitListed = po.Applications__r.size() - po.Updated_Projected_Sending_Number__c;
                for(Application__c app : po.Applications__r){
                    if(app.Status__c == 'Decision' || app.Status__c == 'Confirmation' || app.Status__c == 'Purchase'){                        
                        mapAppByIdToProcess.put(app.Id,app);
                    }
                }
                for(Application__History appH : [SELECT ParentId,CreatedDate FROM Application__History WHERE ParentId in :mapAppByIdToProcess.keySet() 
                                                 AND Field in :fieldSet ORDER BY CreatedDate,ParentId Desc]){
                    if(setIdAppJustProcessed.contains(appH.ParentId)){
                        continue;
                    }
                    mapAppByIdToProcess.get(appH.ParentId).Status_App__c = 'Wait-listed';
                    lstAppToUpdate.add(mapAppByIdToProcess.get(appH.ParentId));
                    setIdAppJustProcessed.add(appH.ParentId);
                    if(setIdAppJustProcessed.size() == countToPassToWaitListed){
                        break;
                    }
                }                
            }
        }
        if(lstAppToUpdate.size()>0){
            update lstAppToUpdate;
        }           
    }
    global void finish(Database.BatchableContext BC){
    }
}