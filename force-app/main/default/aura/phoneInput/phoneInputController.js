({
    loadedScript: function(component, event, helper) {
        //After loading the library fill dynamically the combobox
        const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();
        const supportedCodes = phoneUtil.getSupportedRegions();
        const savedNumber = component.get("v.phone");
        let countries = [];
        if (savedNumber !== undefined) {
            const phone = phoneUtil.parse(savedNumber, "");
            const phoneRegion = phoneUtil.getRegionCodeForNumber(phone);
            countries = supportedCodes.map(sc => {
                if (sc === phoneRegion) component.set("v.countryCodeSelected", sc);
                return ({
                    label: `${helper.getCountryName(sc)} (+${phoneUtil.getCountryCodeForRegion(sc)})`,
                    value: sc,
                    selected: (sc ===  phoneRegion ? true : false)
                })
            });
            countries = countries.concat({
                label: "Select",
                value: null,
                selected: false,
                disabled: true
            });
            component.set("v.phoneNumber", phoneUtil.format(phone, libphonenumber.PhoneNumberFormat.INTERNATIONAL).replace("+" + phone.getCountryCode() + " ", ""))
            component.set("v.pnDisabled", false)
        } else {
            countries = supportedCodes.map(sc => ({
                label: `${helper.getCountryName(sc)} (+${phoneUtil.getCountryCodeForRegion(sc)})`,
                value: sc,
                selected: false
            }));
            countries = countries.concat({
                label: "Select",
                value: null,
                selected: true,
                disabled: true
            });
        }
        component.find("InputSelectSingle").set("v.options", countries.sort((a, b) => {
            var nameA = a.label.toUpperCase(); // ignore upper and lowercase
            var nameB = b.label.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
                return -1;
            }
            if (nameA > nameB) {
                return 1;
            }

            // names must be equal
            return 0;
        }));
        //Enable country codes field input.
        component.set("v.ccIsLoading", false);
    },
    countrySelected: function(component, evt, helper) {
        const componentSelect = component.find("InputSelectSingle");
        const countrySelected = componentSelect.get("v.value");
        component.set("v.countryCodeSelected", countrySelected)
        component.set("v.pnDisabled", false)
        const countries = component.find("InputSelectSingle").get("v.options");
        const countriesSelected = countries.map(c => ({
            ...c,
            selected: countrySelected === c.value ? true : false
        }));
        component.find("InputSelectSingle").set("v.options", countriesSelected);
        helper.validateNumber(countrySelected, component.get("v.phoneNumber"), component);
    },
    handleInputChange: function(component, evt, helper) {
        const AsYouTypeFormatter = libphonenumber.AsYouTypeFormatter;
        const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();
        const countryCode = component.get("v.countryCodeSelected");
        //The formatter should be saved as static var.
        //Because this is not possible, every time the functions run, initiate the formatter again
        //and adds each digit as if they were when the user types.
        const formatter = new AsYouTypeFormatter(countryCode);
        const input = component.find("inputCmp").get("v.value");
        const arrayInput = [...input];
        arrayInput.filter(s => !(s === "-" || s === "(" || s === ")" || s === " ")).map(input => {
            formatter.inputDigit(input)
            return input
        })
        helper.validateNumber(countryCode, formatter.currentOutput_, component)
    }
})