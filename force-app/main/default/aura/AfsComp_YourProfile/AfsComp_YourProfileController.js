({
	doInit : function(component, event, helper) {
        /*Required =["NationalityCitizenship__c","","Dual_citizenship__c"]
        clearNonRequired =["Religious_affiliation__c"];           
        if(component.get("v.contactInfo.Dual_citizenship__c") == "Yes"){
               Required.push("SecondCitizenship__c")           
        }else{
            clearNonRequired.push("SecondCitizenship__c");
        }*/
		console.log(component.get("v.configWrapper"));
        component.set("v.Spinner","true"); 
        //label   
       	var configObj = component.get("v.configWrapper");  
       
         helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_Nationality__c,component);
         component.set("v.NationalityHelpTxt",component.get("v.LabelTempVal"));  
        
         helper.getDynamicCustomLabelVal(configObj.Submission_Txt_Dual_Citizen__c,component);
         component.set("v.DualCitizenShipLabel",component.get("v.LabelTempVal")); 
         helper.getDynamicCustomLabelVal(configObj.Submission_Txt_Dual_Citizen_Help_Text__c,component);
         component.set("v.DualCitizenShipHelpTextLabel",component.get("v.LabelTempVal"));
         helper.getDynamicCustomLabelVal(configObj.Submission_Txt_Second_Citizenship__c,component);
         component.set("v.SecondaryCitizenShipLabel",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.Submission_Txt_SecondCitizenship_Helptxt__c,component);
         component.set("v.SecondaryCitizenShipLabelHelpText",component.get("v.LabelTempVal"));
        
         helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_Nationality__c,component);
         component.set("v.NationalityLabel",component.get("v.LabelTempVal"));
        	


        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_ParentGuardian__c,component);
        component.set("v.ParentGuardian",component.get("v.LabelTempVal"));   
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_ParentGuardian__c,component);
        component.set("v.ParentGuardian",component.get("v.LabelTempVal"));    
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_ParentGuardianHelpTxt__c,component);
        component.set("v.ParentGuardianHelpTxt",component.get("v.LabelTempVal")); 
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_ParentName__c ,component);
        component.set("v.ParentName",component.get("v.LabelTempVal")); 
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_Email__c,component);
        component.set("v.Email",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PrimaryPhone__c,component);
        component.set("v.PrimaryPhone",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PrimaryPhoneHelpTxt__c,component);
        component.set("v.PrimaryPhoneHelpTxt",component.get("v.LabelTempVal"));  
        
        
		 helper.fetchPickListVal(component, 'NationalityCitizenship__c,Dual_citizenship__c,LGBTQ_Member__c,Ethnicity__c','Contact');
        helper.fetchPickListVal(component, 'npe4__Type__c','npe4__Relationship__c'); 
         helper.setFields(component, event) 
         
             
       if(configObj.Submission_Fld_Home_Stepbrother_Age__c =='Hide' &&
         configObj.Submission_Fld_Home_Stepbrother_Age__c =='Hide' &&
         configObj.Submission_Fld_Home_Mother__c =='Hide' &&
         configObj.Submission_Fld_Home_Grandfather__c =='Hide' &&
         configObj.Submission_Fld_Home_Father__c =='Hide' &&
         configObj.Submission_Fld_Home_Other__c =='Hide' &&   
         configObj.Submission_Fld_Home_Stepmother__c =='Hide' &&  
         configObj.Submission_Fld_Home_Stepfather__c =='Hide' &&     
         configObj.Submission_Fld_Home_Sister_Stepsister__c =='Hide' &&      
         configObj.Submission_Fld_Sister_Stepsisters_Age__c =='Hide' &&         
         configObj.Submission_Home_Stepbrother__c =='Hide'){
              component.set("v.LiveWithHeader", "false");
          }else{
               component.set("v.LiveWithHeader","true");
          }       
        
        if(configObj.Submission_Fld_LGBTQ_Member__c  =='Hide' &&
           configObj.Submission_Fld_Ethnicity__c  =='Hide' ){
             component.set("v.OptionalQuestion","false"); 
        }else{
              component.set("v.OptionalQuestion","true"); 
        }
        
        var wrapperObj = component.get("v.applicationWrapper");
        var selectedValNew = wrapperObj.applicationObj.How_would_friends_family_describe_you__c;
        component.set("v.selectedValuesFamilyDesc",selectedValNew);
	},
    changeDualCitizenShip :function(component, event, helper) {
        var item = event.currentTarget.id;
        if(item == 'No'){
           var SecondaryCitizen =  component.get('v.contactInfo.SecondCitizenship__c');
            if(SecondaryCitizen != undefined){
                 component.set('v.contactInfo.SecondCitizenship__c','');
            }
        }
        component.set('v.contactInfo.Dual_citizenship__c',item);
    }, 
    changeAccessToRegiousPickList :function(component, event, helper) {
        var item = event.currentTarget.id;
        component.set('v.applicationWrapper.applicationObj.Access_to_religious_services__c',item);
    }, 
    changeHouseholdPetsPickList : function(component, event, helper) {
        var item = event.currentTarget.id;
        component.set('v.applicationWrapper.applicationObj.Ability_to_live_with_household_pets__c',item);
    },
    changeOpenTo  : function(component, event, helper) {
		var Open_to_Same_Sex_Host_Parents = component.get("v.applicationWrapper.applicationObj.Open_to_Same_Sex_Host_Parents__c");
    	var Open_to_Single_Host_Parent =  component.get("v.applicationWrapper.applicationObj.Open_to_Single_Host_Parent__c");
		var Open_to_Sharing_a_host_family = component.get("v.applicationWrapper.applicationObj.Open_to_Sharing_a_host_family__c");;
		var sourceId = event.currentTarget.id;
        console.log(sourceId)
    	if(sourceId =="Open_to_Same"){
    		Open_to_Same_Sex_Host_Parents = !Open_to_Same_Sex_Host_Parents;
		}else if( sourceId =="Open_to_Single"){
    		Open_to_Single_Host_Parent = !Open_to_Single_Host_Parent;
        }else if( sourceId == "Open_to_Sharing_a_host_family__c"){
            Open_to_Sharing_a_host_family = !Open_to_Sharing_a_host_family;
        }	
		component.set("v.applicationWrapper.applicationObj.Open_to_Same_Sex_Host_Parents__c",Open_to_Same_Sex_Host_Parents); 
		component.set("v.applicationWrapper.applicationObj.Open_to_Single_Host_Parent__c",Open_to_Single_Host_Parent); 
		component.set("v.applicationWrapper.applicationObj.Open_to_Sharing_a_host_family__c",Open_to_Sharing_a_host_family); 
        console.log(component.get("v.applicationWrapper.applicationObj.Open_to_Single_Host_Parent__c"));
	},
    setParentAsFirst : function (component, event, helper){
		var parent = event.currentTarget.id;
        component.set("v.contactInfo.Parent_legal_Guardian__c",parent); 
	},
    updateLiveWith : function (component, event, helper){
        var live_with = component.get("v.contactInfo.You_live_with_this_Parent_Guardian__c");
        if(live_with != undefined && live_with !=''){
            component.set("v.contactInfo.You_live_with_this_Parent_Guardian__c", !live_with);
        }else{
            component.set("v.contactInfo.You_live_with_this_Parent_Guardian__c", true);
        }
    },
    updateEmergencyContact : function (component, event, helper){
        var emergencyContact = component.get("v.contactInfo.Parent_Legal_Guardian_Emergency_Contact__c");
        if(emergencyContact != undefined && emergencyContact !=''){
            component.set("v.contactInfo.Parent_Legal_Guardian_Emergency_Contact__c", !emergencyContact);
        }else{
            component.set("v.contactInfo.Parent_Legal_Guardian_Emergency_Contact__c", true);
        }
    },addStepMother :function (component, event, helper){       
		 var stepMother = component.get("v.contactInfo.Home_Stepmother__c");
     	if(stepMother != undefined && stepMother !=null ){
             var newValue = 0
             if(parseInt(stepMother) > 0 ){
                 newValue = 0;
             }else{
                 newValue = 1;
             }	
            component.set("v.contactInfo.Home_Stepmother__c", newValue);
        }else{
            component.set("v.contactInfo.Home_Stepmother__c", 1);
        }
	},
    addStepFather :function (component, event, helper){       
		 var stepFather = component.get("v.contactInfo.Home_Stepfather__c");
     	if(stepFather != undefined && stepFather !=null ){
             var newValue = 0
             if(parseInt(stepFather) > 0 ){
                 newValue = 0;
             }else{
                 newValue = 1;
             }	
            component.set("v.contactInfo.Home_Stepfather__c", newValue);
        }else{
            component.set("v.contactInfo.Home_Stepfather__c", 1);
            
        }
	},
    addGrandMother :function (component, event, helper){       
		var grandMother = component.get("v.contactInfo.Home_Grandmother__c");
     	if(grandMother != undefined && grandMother !=null ){
             var newValue = 0
             if(parseInt(grandMother) > 0 ){
                 newValue = 0;
             }else{
                 newValue = 1;
             }	
             console.log(newValue);
            component.set("v.contactInfo.Home_Grandmother__c", newValue);
        }else{
            component.set("v.contactInfo.Home_Grandmother__c", 1);
        }
	},
    addGrandFather :function (component, event, helper){       
		 var grandFather = component.get("v.contactInfo.Home_Grandfather__c");
     	if(grandFather != undefined && grandFather !=null ){
             var newValue = 0
             if(parseInt(grandFather) > 0 ){
                 newValue = 0;
             }else{
                 newValue = 1;
             }	
            component.set("v.contactInfo.Home_Grandfather__c", newValue);
        }else{
            component.set("v.contactInfo.Home_Grandfather__c", 1);
        }
    },
    addOther : function (component, event, helper){       
		 var other = component.get("v.contactInfo.Home_Other__c");
        
     	if(other != undefined && other !=null ){
             var newValue = 0
             if(parseInt(other) > 0 ){
                 newValue = 0;
                 component.set("v.contactInfo.Home_Other_Relationship__c", '');
             }else{
                 newValue = 1;
             }	
            component.set("v.contactInfo.Home_Other__c", newValue);
        }else{
            component.set("v.contactInfo.Home_Other__c", 1);
        }
    },
    doSaveProfile : function(component ,event ,helper){
        helper.saveCompleteProfile(component,event,helper);
    },
    onUpdateBrotherAge :function(component ,event ,helper){
    	console.log(event.currentTarget.id);
       //console.log component.find(event.currentTarget.id).get("v.value"));
    },
    updateFamilyDesc : function (component, event, helper) {
        
        var selectedVal = component.get("v.selectedValuesFamilyDesc");
        if(selectedVal === undefined){
            selectedVal = "";
        }
        if(selectedVal.length > 0 && selectedVal[selectedVal.length - 1] != ";"){
            selectedVal += ";";
        }
        if(event.currentTarget.className == "testDummy"){
            if(selectedVal.indexOf(event.currentTarget.id) <= -1){
                selectedVal += event.currentTarget.id + ";";
            } 
            event.currentTarget.className = "active";
        }else{
            selectedVal = selectedVal.replace(event.currentTarget.id + ";","");
            event.currentTarget.className = "testDummy";
        }
        component.set("v.selectedValuesFamilyDesc",selectedVal);
    }
})