declare module "@salesforce/apex/Afs_PhotoNavigationController.getUploadedPhoto" {
  export default function getUploadedPhoto(): Promise<any>;
}
declare module "@salesforce/apex/Afs_PhotoNavigationController.getProfilePhoto" {
  export default function getProfilePhoto(): Promise<any>;
}
declare module "@salesforce/apex/Afs_PhotoNavigationController.getEncodedStringList" {
  export default function getEncodedStringList(param: {fileId: any, isProfilePhoto: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_PhotoNavigationController.getEncodedString" {
  export default function getEncodedString(param: {fileId: any, isProfilePhoto: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_PhotoNavigationController.deleteCDL" {
  export default function deleteCDL(param: {documentId: any}): Promise<any>;
}
