/**
 * Created by jotategui on 10/05/2018.
 */

public without sharing class ScholarshipAppTriggerHandler {

    public static void createToDos(Map<Id, Scholarship_Application__c> applications) {

        List<To_Do_Item__c> deleteTo_dos = [SELECT id from To_Do_Item__c WHERE Scholarship_Application__c
                IN :applications.keySet()];
        if (deleteTo_dos != null && deleteTo_dos.size() > 0) {
            delete deleteTo_dos;
        }

        Map<Id,Application__c> ApplicationsMap = new Map<id,Application__c>();
        Set<Id> ProgramOffersId = new Set<Id>();

        system.debug('applications.keySet()='+applications.keySet());
        for(Application__c app : [SELECT Program_Offer__r.id, CreatedDate, Program_Offer__r.From__c, Program_Offer__c, Program_Start_Date__c from Application__c WHERE id
                in (SELECT Application__c FROM Scholarship_Application__c WHERE id in :applications.keySet())]){
            ApplicationsMap.put(app.id,app);
            if(app.Program_Offer__c != null){
            	ProgramOffersId.add(app.Program_Offer__c);
            }

        }


        Map<Id, List<Master_To_Do__c>> MasterTodoGrouped = new Map<Id, List<Master_To_Do__c>>();
        for(Master_To_Do__c masterToDo : [
                SELECT Name, Days__c, Dependant_Task__c, Program_Offer__c, Help__c, Comments__c,
                        Scholarship__c, Template__c, When__c, Description__c,
                        Stage_of_portal__c, Type__c, Link__c, To_Do_Label__c, RecordType.Name
                from Master_To_Do__c
                WHERE Program_Offer__c IN :ProgramOffersId
        ]) {
            Id ProgramOfferId = masterToDo.Program_Offer__c;
            if (!MasterTodoGrouped.containsKey(ProgramOfferId))
                MasterTodoGrouped.put(ProgramOfferId, new List<Master_To_Do__c>());
            MasterTodoGrouped.get(ProgramOfferId).add(masterToDo);
        }

        List<To_Do_Item__c> ToDoInsert = new List<To_Do_Item__c>();
        for (Scholarship_Application__c app : applications.values()) {
            List<To_Do_Item__c> innerinserttodos = new List<To_Do_Item__c>();
            Id ScholarshipId = app.Id;
            Id ProgramOffer = ApplicationsMap.get(app.Application__c).Program_Offer__c;
            innerinserttodos = TodoListHelper.CreateItemsFromMaster(MasterTodoGrouped.get(ProgramOffer), ApplicationsMap.get(app.Application__c), ScholarshipId);
            for (To_Do_Item__c todo : innerinserttodos) {
                ToDoInsert.add(todo);
            }
        }
        insert ToDoInsert;
    }
    
     public static List<To_Do_Item__c> createToDosIntoAppWhenSchAppChangedToApplying(List<Scholarship_Application__c> newList,Map<id,Scholarship_Application__c> mapOld,boolean isUpdate) {
     	List<To_Do_Item__c> lstToDoItemToInsert = new List<To_Do_Item__c>();
     	List<Scholarship_Application__c> lstSchAppToProccess = new List<Scholarship_Application__c>();
     	set<Id> applicationIds = new set<Id>();
     	set<Id> scholarshipIds = new set<Id>();
     	
		for(Scholarship_Application__c schApp : newList){
			if(schApp.Stage__c == 'Applying'  &&  ((isUpdate && schApp.Stage__c != mapOld.get(schApp.Id).Stage__c) || !isUpdate)){
 				lstSchAppToProccess.add(schApp);
 				applicationIds.add(schApp.Application__c);
 				scholarshipIds.add(schApp.Scholarship__c);
			}	
		} 		
     	
     	if(!lstSchAppToProccess.isEmpty()){
     		//Retrieve Application
            Map<Id,Application__c> mapApplicationsById = new Map<Id,Application__c>([Select Id, CreatedDate,Program_Start_Date__c From Application__c where Id = :applicationIds]);
     		//Retrieve Master_To_Do
            Map<Id,List<Master_To_Do__c>> mapMTDListBySchId = new Map<Id,List<Master_To_Do__c>>();
            for(Master_To_Do__c mtd : [SELECT Id,Name, Days__c, Dependant_Task__c, Program_Offer__c, Help__c, Comments__c,
                        Scholarship__c, Template__c, When__c, Description__c,
                        Stage_of_portal__c, Type__c, Link__c, To_Do_Label__c, RecordType.Name From Master_To_Do__c Where Scholarship__c in :scholarshipIds]){
                if(!mapMTDListBySchId.containskey(mtd.Scholarship__c)){
                    mapMTDListBySchId.put(mtd.Scholarship__c,new List<Master_To_Do__c>());
                }
                mapMTDListBySchId.get(mtd.Scholarship__c).add(mtd);
            }
            
            //Process
            for(Scholarship_Application__c schApp : lstSchAppToProccess){
                List<To_Do_Item__c> lstToDoItemTmp = TodoListHelper.CreateItemsFromMaster(mapMTDListBySchId.get(schApp.Scholarship__c), mapApplicationsById.get(schApp.Application__c),  schApp.Id);
                lstToDoItemToInsert.addall(lstToDoItemTmp);
            }
            
            if(lstToDoItemToInsert.size() > 0){
            	Insert lstToDoItemToInsert;
            }
     	}
     	
     	return lstToDoItemToInsert;
     }
    
    //Method which canceled the To_Do__c related to "Application in Scholarship" which status changed to Lost
    public static List<To_Do_Item__c> canceledToDosAppWhenSchAppChangedToLost(List<Scholarship_Application__c> newList,Map<id,Scholarship_Application__c> mapOld,boolean isUpdate){
        List<To_Do_Item__c> lstToDoItemsToUpdate = new List<To_Do_Item__c>();
     	set<Id> SchAppIds = new set<Id>();
        
        //Determine which Scholarship_Application__c meet the conditions
        for(Scholarship_Application__c schApp : newList){
			if(schApp.Stage__c == 'Lost'  &&  ((isUpdate && schApp.Stage__c != mapOld.get(schApp.Id).Stage__c) || !isUpdate)){
 				SchAppIds.add(schApp.Id);
			}	
		}
        
        //Process
        if(SchAppIds.Size() > 0){
            for(To_Do_Item__c toDo : [SELECT Id, Status__c FROM To_Do_Item__c WHERE Scholarship_Application__c IN :SchAppIds]){
                toDo.Status__c = 'Canceled';
                lstToDoItemsToUpdate.add(toDo);
            }
            if(lstToDoItemsToUpdate.Size() > 0){
                Update lstToDoItemsToUpdate;
            }
        }
        
        return lstToDoItemsToUpdate;
    }
    
    /*
     * Date: 11/12/2018
     * Metodo cuando se crea un registro de Scholarship Application para sumar de nuevo el Scholarship 
     * Pre-Application Fee en el Application relacionado al Scholarship Applocation creado
     * @author acuello
     */
    public static void whenSchAppIsCreateSumPreAppfee(List<Scholarship_Application__c> newList){
        system.debug('ScholarshipAppTriggerHandler.whenSchAppIsCreateSumPreAppfee');
        try {
            
            List<Id> listApplicationIds = new List<Id>();
            for(Scholarship_Application__c objSchApp : newList){
                listApplicationIds.add(objSchApp.Application__c);
            }
            
            ApplicationTriggerHandler.sumPreApplicationFeeByListApp(listApplicationIds);
            
        } catch (Exception Ex) {
                System.debug('Exception {');
                System.debug('Message : ' + Ex.getMessage());
                System.debug('Cause : ' + Ex.getCause());
                System.debug('Line Number : ' + Ex.getLineNumber());
                System.debug('Type Name : ' + Ex.getTypeName());
                System.debug('StackTrace : ' + Ex.getStackTraceString());
                System.debug('}');
        }
    }
    
    /*
     * Date: 11/12/2018
     * Metodo cuando se elimina un registro de Scholarship Application para sumar de nuevo el Scholarship 
     * Pre-Application Fee en el Application relacionado al Scholarship Applocation eliminado
     * @author acuello
     */
    public static void whenSchAppIsDeleteSumPreAppfee(List<Scholarship_Application__c> newList){
        system.debug('ScholarshipAppTriggerHandler.whenSchAppIsDeleteSumPreAppfee');
        try {
            
            List<Id> listApplicationIds = new List<Id>();
            for(Scholarship_Application__c objSchApp : newList){
                listApplicationIds.add(objSchApp.Application__c);
            }
            
            ApplicationTriggerHandler.sumPreApplicationFeeByListApp(listApplicationIds);
            
        } catch (Exception Ex) {
                System.debug('Exception {');
                System.debug('Message : ' + Ex.getMessage());
                System.debug('Cause : ' + Ex.getCause());
                System.debug('Line Number : ' + Ex.getLineNumber());
                System.debug('Type Name : ' + Ex.getTypeName());
                System.debug('StackTrace : ' + Ex.getStackTraceString());
                System.debug('}');
        }
    }
    /*
     * Date: 11/12/2018
     * Cuando se hace un update de un registro de Schocarship Application y cambia el stage a lost 
     * Se tiene que hacer de nuevo el conteo del campo de pre application fee
     * @author acuello
     */
    public static void whenSchAppIsUpdateSumPreAppfee(List<Scholarship_Application__c> newList,Map<Id, Scholarship_Application__c> mapSchAppOldValues){
        system.debug('ScholarshipAppTriggerHandler.whenSchAppIsUpdateSumPreAppfee');
        try {
            
            List<Scholarship_Application__c> finalSchAppList = new List<Scholarship_Application__c>();
            for(Scholarship_Application__c objSchApp : newList){
                if(objSchApp.Stage__c != mapSchAppOldValues.get(objSchApp.Id).Stage__c && objSchApp.Stage__c == 'Lost'){
                    finalSchAppList.add(objSchApp);
                }
            }
            
            if(finalSchAppList.size() > 0){
                List<Id> listApplicationIds = new List<Id>();
                for(Scholarship_Application__c objSchApp : finalSchAppList){
                    listApplicationIds.add(objSchApp.Application__c);
                }
                
                ApplicationTriggerHandler.sumPreApplicationFeeByListApp(listApplicationIds);
            }
        } catch (Exception Ex) {
                System.debug('Exception {');
                System.debug('Message : ' + Ex.getMessage());
                System.debug('Cause : ' + Ex.getCause());
                System.debug('Line Number : ' + Ex.getLineNumber());
                System.debug('Type Name : ' + Ex.getTypeName());
                System.debug('StackTrace : ' + Ex.getStackTraceString());
                System.debug('}');
        }
    }
}