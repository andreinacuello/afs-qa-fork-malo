global without sharing class AfsUserManagementController {
    
    global static user getInMemoryUserAccount(string email, string firstName, string lastName, string communityId, Afs_ParentComponentController.loginWrapper wraperObj){
        User returnUser;
        Savepoint sp;
        try{
            
            String queryStr = 'SELECT id, Name, CRMUserOwner__c, Community_App_Queue__c, Afs_SendingPartner__c';
            if(!Test.isRunningTest()){
                queryStr += ' FROM AFS_Community_Configuration__c WHERE Unique_Id__c =: communityId';
            }else{
                queryStr += ' FROM AFS_Community_Configuration__c';
            }
            list<AFS_Community_Configuration__c>  listCommunityConfig;
            if(String.isNotBlank(communityId) || Test.isRunningTest()){
                
               listCommunityConfig  =  Database.query(queryStr);
                  
            }
            /*List<NetworkSelfRegistration> accs = [SELECT AccountID 
                                                  FROM NetworkSelfRegistration 
                                                  WHERE networkid = :communityId];*/
            // Create account of type house hold
            Id recordTypeId = [SELECT id,Name FROM RecordType WHERE SObjectType = 'Account' AND Name= 'Household Account'].id;
            Account accs = new Account();
            // Check added to avoid duplicate creation
            list<Account> accList = [SELECT id FROM Account WHERE Name =: firstName + System.label.AfsLbl_General_Account];
            if(accList.isEmpty()){
                accs.RecordTypeId = recordTypeId;
                if(!listCommunityConfig.isEmpty()){
                    accs.OwnerId = listCommunityConfig[0].CRMUserOwner__c;
                }
                accs.Name = firstName + System.label.AfsLbl_General_Account;
                accs.Community_Unique_Id__c = communityId;
                insert accs; 
            }
            // Check added to avoid duplicate creation
            list<Contact> conList = [SELECT id FROM Contact WHERE email =: email and firstName =: firstName];
            Contact c = new Contact();
            if(conList.isEmpty()){
                
                c.accountId = accs.id;
                c.email = email;
                c.firstName = firstName;
                c.lastName = string.isBlank(lastName) ? firstName : lastName;
                c.Community_Id__c = communityId;
                c.Sending_Partner__c = listCommunityConfig[0].Afs_SendingPartner__c;
                if(!listCommunityConfig.isEmpty()){
                    c.OwnerId = listCommunityConfig[0].CRMUserOwner__c;
                }
                //c.OwnerId = '0053B000001hphJ'; //Manish Test
                sp = Database.setSavepoint();
                
                insert(c);
                
                //intentionally done to avoid flow error
                if(wraperObj != null){
                    c.Birthdate = wraperObj.DOB;
                    c.MiddleName = wraperObj.MiddleName;
                    c.MailingPostalCode = wraperObj.ZipCode;
                    c.MobilePhone = wraperObj.MobileNumber;
                    c.Sending_Partner__c = listCommunityConfig[0].Afs_SendingPartner__c;
                    if(wraperObj.HearAboutUs != Label.AfsLbl_SignUp_None){
                        c.How_did_you_hear_about_AFS__c = wraperObj.HearAboutUs;
                    }
                    c.Terms_Service_Agreement__c = wraperObj.AgreeTerms;
                    c.Keep_me_informed_Opt_in__c = wraperObj.KeepMeInformed;
                    update c;
                }
            }else{
                c = conList[0];
            }
            
            
            
            //Sandbox - trigger fails
            // Check added to avoid duplicate creation
            list<Application__c> appList = [SELECT id FROM Application__c WHERE Applicant__c =:  c.Id];
            if(appList.isEmpty()){
                Application__c app = new Application__c(Applicant__c = c.Id,Status_App__c = null);
                if(!listCommunityConfig.isEmpty()){
                    app.OwnerId = listCommunityConfig[0].Community_App_Queue__c;
                }             
                insert app; 
            }
            
            //TODO: Customize the username and profile. Also check that the username doesn't already exist and
            //possibly ensure there are enough org licenses to create a user. Must be 80 characters or less.
            User u = new User();
            u.username = email; // + Afs_ParentComponentController.getConfiguration(communityId).UserName_Suffix__c;
            u.email = email;
            u.lastName = string.isBlank(lastName) ? firstName : lastName;
            u.firstName = firstName;
            String alias = email;
            //Alias must be 8 characters or less
            if(alias.length() > 4) {
                alias = alias.substring(0, 4);
                alias = alias.replace('@','');
                alias = alias.replace('.','');
                alias = alias.replace('_','');
                alias = alias.replace('-','');
            }
            u.alias = alias;
            //must for community
            if (email.length() > 40){
                u.CommunityNickname = email.substring(0,39);
            }else{
                u.CommunityNickname = email;
            }
            u.languagelocalekey = UserInfo.getLocale();
            u.localesidkey = UserInfo.getLocale();
            u.emailEncodingKey = 'UTF-8';
            u.timeZoneSidKey = 'America/Los_Angeles';
            u.IsActive = true;
            u.contactId = c.Id;                        
            returnUser = u; //return valid user
            
        }catch(Exception ex){
            Afs_UtilityApex.logger(ex.getstacktracestring() + '###' + ex);
             if(sp != null)
                Database.rollback(sp);
            throw ex;
        }
        Afs_UtilityApex.logger('Returned User: ' + returnUser);
        return returnUser;              
    }
    
    global static PageReference createUserAndLogin(user u, string password, string accountId){
        PageReference successLoginUrl;
        try{
            Afs_UtilityApex.logger(u.Alias);
            String userId = Site.createPortalUser(u, accountId, password);
            if (userId != null) { 
                if (password != null && password.length() > 1) {
                    successLoginUrl = Site.login(u.Username, password, '');                
                }
            }
        }catch(Exception ex){
            throw ex;
        }
        return successLoginURL;
    }
    
    //check this impementation setu
    //private static void signUpImplementation(){
        //data from form front-lightning comp
        //String accId = [SELECT AccountID FROM NetworkSelfRegistration WHERE networkid = :communityId][0].AccountId;
        // User usr =   AfsUserManagementController.getInMemoryUserAccount(...);
        // usr.BirthDate__c = lightning birthdate from form;
        // pagereference pr = createUserAndLogin(usr, passwordlightningpage, accid)
    //}
}