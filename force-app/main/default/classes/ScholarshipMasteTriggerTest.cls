@IsTest
private class ScholarshipMasteTriggerTest {
    @TestSetup public static void testSetup(){
        
    }
    private static Scholarship__c create_Scholarship(Account acc){
        Scholarship__c sch = new Scholarship__c();
        sch.name = 'Test Scholarship';
        sch.Indicate_the_AFS_Partner__c = acc.Id;
        sch.Sending_Partner__c = acc.Id;
        return sch;
    }
    
    @IsTest static void insertUpdateDeleteTest(){
        Account acc = Util_Test.create_AFS_Partner();
        Master_To_Do_Template__c toDo = new Master_To_Do_Template__c(Name = 'Test', To_use_on__c = 'Scholarship', 
                                                                     Type__c = 'Standard for Portal');
        insert todo;
        Master_To_Do_Template__c toDo2 = new Master_To_Do_Template__c(Name = 'Test', To_use_on__c = 'Scholarship', 
                                                                     Type__c = 'Standard for Portal');
        insert todo2;
        
        Test.startTest();
        Scholarship__c scho = create_Scholarship(acc);
        scho.To_Do_Template__c = toDo.Id;
        insert scho;
        
        scho.To_Do_Template__c = toDo2.Id;
        update scho;
        
        delete scho;
        Test.stopTest();
    }
}