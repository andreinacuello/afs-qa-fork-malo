global without sharing class Afs_AboutYouController {
	
    @AuraEnabled
    global static String doSubmittApplication(Application__c appObj,String fromWhere){
        String msg = 'Success';
        try{
            if(fromWhere == 'AboutYou'){
                appObj.Status__c = 'Participation Desire';
                appObj.Status_App__c = 'Pre-Applied';
                update appObj;                
            }
            String contactId = [SELECT id,ContactId FROM User WHERE id=: UserInfo.getUserId()].ContactId;
            list<Program_Offer_in_App__c> programOfferList = [SELECT id,Program_Offer__c, Is_the_applicant_Eligible__c,Desired__c  FROM Program_Offer_in_App__c WHERE Application__c =: appObj.id AND Contact__c =: contactId AND Is_the_applicant_Eligible__c = null];
            if(programOfferList.isEmpty()){
                msg = 'Processed';
            }
        }catch(Exception e){
            Afs_UtilityApex.logger(e.getStackTraceString());
            throw new AuraHandledException(e.getMessage());
        }
        
        return msg;
    }
    
    @AuraEnabled
    global static map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> getPicklistValues(String objObject, string fld) {
        list<String> fieldAPINames = fld.split(',');
        map<String,list<Afs_UtilityApex.PicklistWrapperUtil>> mapFieldPicklistVal = new map<String,list<Afs_UtilityApex.PicklistWrapperUtil>>();
        for(String fieldAPIName : fieldAPINames){
            list<Afs_UtilityApex.PicklistWrapperUtil> picklsitValues = new list<Afs_UtilityApex.PicklistWrapperUtil>();
            picklsitValues.addAll(Afs_UtilityApex.getPicklistWrapper(objObject, fieldAPIName.trim()));
            mapFieldPicklistVal.put(fieldAPIName.trim(),picklsitValues);
        }
        
        return mapFieldPicklistVal; 
        
    }
    
}