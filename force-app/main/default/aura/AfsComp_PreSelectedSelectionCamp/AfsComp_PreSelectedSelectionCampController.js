({
	openExternalLink : function(component, event, helper) {
        var programOffer = component.get("v.programOffer");
        component.set("v.isLinkedClicked","true");
		window.open(programOffer.Camp_URL__c,'_blank');
	},
    
    doSaveRecord : function(component, event, helper) {
        component.set("v.Spinner","true");
        if(component.get("v.isLinkedClicked") == "false" || component.get("v.isLinkedClicked") == false){
            component.set("v.Spinner","false"); 
            var toastEvent = $A.get("e.force:showToast");
            var messageTemplate = $A.get("$Label.c.AfsLbl_Error_LinkMustBeClicked");
            toastEvent.setParams({ 
                type : "Error",
                duration : 5000,
                message: messageTemplate
            });
            toastEvent.fire();
            return;
        }
        helper.onSaveRecords(component, event);
	},
    
    doInit : function(component, event, helper) {
        component.set("v.Spinner","true"); 
        var configObj = component.get("v.configWrapper");
        helper.getDynamicCustomLabelVal(configObj.SelectionCamp_Txt_Description__c,component);
        component.set("v.SelectionCamp_Txt_Description",component.get("v.LabelTempVal"));
		helper.onInit(component,event);  
	},
    
    backToList : function (component, event, helper){
        component.set("v.parentToggle",false);
    }
})