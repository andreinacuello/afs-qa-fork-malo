declare module "@salesforce/apex/Afs_AboutYouController.doSubmittApplication" {
  export default function doSubmittApplication(param: {appObj: any, fromWhere: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_AboutYouController.getPicklistValues" {
  export default function getPicklistValues(param: {objObject: any, fld: any}): Promise<any>;
}
