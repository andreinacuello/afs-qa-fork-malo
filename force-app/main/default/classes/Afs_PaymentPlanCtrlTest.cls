@isTest(SeeAllData=false)
public class Afs_PaymentPlanCtrlTest {
	@testSetup
    public static void setup_test(){
		Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
    }	
    
    public static testMethod void getPaymentInstallmentListTest(){
        Test.startTest(); 
            List<application__c>  application  = [select Id from application__c limit 1] ;       
            System.assertEquals(true,Afs_PaymentPlanCtrl.getPaymentInstallmentList(application[0].id).isEmpty());
        Test.stopTest(); 
    }
    
    public static testMethod void getPaymentEntryListTest(){
        Test.startTest(); 
            List<application__c>  application  = [select Id from application__c limit 1] ;    
            System.assertEquals(true,Afs_PaymentPlanCtrl.getPaymentEntryList(application[0].id).isEmpty());
        Test.stopTest(); 
    }
    
    public static testMethod void updateTaskTest(){
        
        Test.startTest(); 
		List<application__c>  application  = [select Id from application__c limit 1] ;    
        To_Do_Item__c obj = new To_Do_Item__c();
        obj.Name = 'sd'; 
        obj.Type__c = 'Filling Field'; 
        obj.Stage_of_portal__c = '1. Stage: Pre-application'; 
        obj.Application__c  = application[0].id; 
        obj.Status__c = 'Pending'; 
        obj.Due_Date__c = Date.today().addDays(10); 
        insert obj;
        Afs_PaymentPlanCtrl.updateTask(obj.id);
        System.assertEquals(1,application.size());
        Test.stopTest(); 
         
    }
}