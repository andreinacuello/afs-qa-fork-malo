@isTest
public class ApplicationTriggersHandlerTest {
    @testSetup
    public static void setup_method(){
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
    }
    
    static testMethod void Test1() {
        
        Date myDate = Date.today();
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'Dynamic' limit 1];
        Application__c myApplication = new Application__c(Create_Installments__c = false , Amount__c = 1000);
        Test.StartTest();
        insert myApplication;
        Test.StopTest();
    
    }
    
    static testMethod void Test2() {
        
        ApplicationTriggersHandler.dummy();
        
        Date myDate = Date.today();
        RecordType rt = [SELECT Id FROM RecordType WHERE DeveloperName = 'Dynamic' limit 1];
        Application__c myApplication = new Application__c(Create_Installments__c = false , Amount__c = 1000);
        Test.StartTest();
        insert myApplication;
        Test.StopTest();
        
    
    }

}