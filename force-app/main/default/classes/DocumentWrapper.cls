global class DocumentWrapper Implements Comparable {
    
    @AuraEnabled
    global To_Do_Item__c item {get;set;}
    @AuraEnabled
    global String displayName {get; set;}
    @AuraEnabled
    global Boolean blockAction {get; set;}
    @AuraEnabled
    global Boolean isDocumentAction {get; set;}
    @AuraEnabled
    global String strToDoLabel {get; set;}
    @AuraEnabled 
    global String strWhenLabel {get; set;}

    
    global DocumentWrapper(To_Do_Item__c doc,String ToDoLabel,String WhenLbl, String DateFormat){
        this.item = doc;
        this.blockAction = false;
        String month;
        String day;
        String year;
        
        if(item.Due_Date__c != null){
            if(item.Due_Date__c.month() < 10){
                month = '0' + item.Due_Date__c.month();
            }else{
                month = String.valueOf(item.Due_Date__c.month());
            }
            
            if(item.Due_Date__c.day() < 10){
                day = '0' + item.Due_Date__c.day();
            }else{
                day = String.valueOf(item.Due_Date__c.day());
            }
            
            
            year = String.valueOf(item.Due_Date__c.year());
        }
        String formattedDate  = (item.Due_Date__c != null ? 
                                 (DateFormat == 'mm-dd-yyyy' ? 
                                  month + '/' + day + '/' + year :
                                  day + '/' + month + '/' + year) : '');
        
        this.displayName = (item.To_Do_Label__c != null ? ToDoLabel.remove('BY') : item.Name.remove('BY'));    
        if(!formattedDate.equals('')){
            this.displayName = Label.DUE + ' ' + formattedDate + ' - ' + this.displayName;
        }
        
        
        
        if(item.Status__c != 'Pending' && item.Status__c != 'Rejected'){
            this.blockAction = true;
        }
        
        isDocumentAction = false;
        
        if(item.INTRODUCE_YOURSELF_BY__c){
            isDocumentAction = true;
        }else if(item.ATTEND_YOUR_PRE_DEPARTURE_ORIENTATION__c){
            isDocumentAction = true;
        }else if(item.COMPLETE_YOUR_TRAVEL_INFORMATION_BY__c){
            isDocumentAction = true;
        }else if(item.REVIEW_YOUR_PAYMENT_PLAN_AND_MAKE_YOUR_F__c){
            isDocumentAction = true;
        }else if(item.SUBMIT_YOUR_HEALTH_INFORMATION_BY__c){
            isDocumentAction = true;
        }
        
        strToDoLabel = ToDoLabel;
        strWhenLabel = WhenLbl;
    }
    
    global Integer compareTo(Object compareTo) {
        DocumentWrapper docW = (DocumentWrapper)compareTo;
        Integer returnValue = 0;

        if(docW.item.Due_Date__c != NULL && item.Due_Date__c != NULL){
            if(docW.item.Due_Date__c == item.Due_Date__c){
                if(docW.strToDoLabel > strToDoLabel){
                    returnValue = -1;
                }
                if(docW.strToDoLabel < strToDoLabel){
                    returnValue = 1;
                }
            }
            if (docW.item.Due_Date__c < item.Due_Date__c){
                returnValue = 1;
            }
            if (docW.item.Due_Date__c > item.Due_Date__c){
                returnValue = -1;
            }

        }
        else if(docW.item.Due_Date__c != NULL && item.Due_Date__c == NULL){
            returnValue = 1;
        }
        else if(docW.item.Due_Date__c == NULL && item.Due_Date__c != NULL){
            returnValue = -1;
        }
        else if(docW.item.Due_Date__c == NULL && item.Due_Date__c == NULL){
            if(docW.displayName > displayName){
                returnValue = -1;
            }
            if(docW.displayName < displayName){
                returnValue = 1;
            }
        }        
        return returnValue;
    }
    
}