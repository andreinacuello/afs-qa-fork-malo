@isTest(SeeAllData=false)
public class Afs_OpportunityTriggerTest {	
    
    public static testMethod void deleteOppTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        List<application__c>  application  = [select Id from application__c limit 1] ; 
        List<Account>  AccountList  = [select Id from Account limit 1] ;
        opportunity op = new opportunity();
        op.AccountId = AccountList[0].id;
        op.CloseDate = Date.today().addDays(1);
        op.StageName = 'Participation Desire' ;
        op.Name = 'A-0572-Esmeralda Rodriguez';
        insert op;
        
        Afs_Triggers_Switch__c afsTriggerSwitch = Afs_Triggers_Switch__c.getInstance('Afs_Trigger');
        afsTriggerSwitch.By_Pass_Opportunity_Trigger__c = false;
        update afsTriggerSwitch;
        Test.startTest();
            try{
                delete op;
            }catch(Exception e){
                system.assertEquals(true, e != null);
            }
            System.assertEquals(true,[SELECT id FROM application__c] != null);
        Test.stopTest(); 
    }
    
    public static testMethod void updateOppTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        List<application__c>  application  = [select Id from application__c limit 1] ; 
        List<Account>  AccountList  = [select Id from Account limit 1] ;
        opportunity op = new opportunity();
        op.AccountId = AccountList[0].id;
        op.CloseDate = Date.today().addDays(1);
        op.StageName = 'Participation Desire' ;
        op.Name = 'A-0572-Esmeralda Rodriguez';
        insert op;
        
        Afs_Triggers_Switch__c afsTriggerSwitch = Afs_Triggers_Switch__c.getInstance('Afs_Trigger');
        afsTriggerSwitch.By_Pass_Opportunity_Trigger__c = false;
        update afsTriggerSwitch;
        Test.startTest();
            try{
                update op;
            }catch(Exception e){
                system.assertEquals(true, e != null);
            }
            System.assertEquals(true,[SELECT id FROM application__c] != null);
        Test.stopTest(); 
    }
    
    
}