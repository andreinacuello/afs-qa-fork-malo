/**
 * Created by jotategui on 08/05/2018.
 */

trigger ProgOfferMasterTrigger on Program_Offer__c (after insert, after update, before insert, before update,
        after delete, before delete) {
system.debug(GL_SF_Integrations_Batch.isExecutingBatchIntegrations);
            if(GL_SF_Integrations_Batch.isExecutingBatchIntegrations){
                return;
            }
            
    //automatic relationship with master to do template based on hosting program
    if(Trigger.isBefore){
    	if(Trigger.isInsert){
    		
    		Set<Id> hostingProgramIds = new Set<Id>();
    		
    		for(Program_Offer__c PO : Trigger.new){
    			if(PO.To_Do_Template__c == null){
    				hostingProgramIds.add(PO.Hosting_Program__c);
    			}	
    		}
    		
    		/*
    			IPbwNH16-FRA = Hosting Program Name
    			-----------------------------------
    			IP = Duration__c
    			bw = Program_Content__c 
    			NH = Hemisphere__c (cycle)
    			2016 = Year__c
    			-
    			FRA = Host_IOC__c (formula)
    		*/
    		
    		
    		Map<Id, Hosting_Program__c> hostingProgramMap = new Map<id, Hosting_Program__c>([Select id, 
																						    		Duration__c, 
																						    		Hemisphere__c, 
																						    		Program_Content__c,
																						    		Year__c, 
																						    		Host_IOC__c 
																				    		from Hosting_Program__c 
																				    		where Id in:hostingProgramIds]);
    	
    		Set<String> customSettingFormulaSet = new Set<String>();
    		for(Program_Offer__c PO : Trigger.new){
    			if(PO.To_Do_Template__c == null && hostingProgramMap.containsKey(PO.Hosting_Program__c) == true){
    				
    				String originalName = PO.Name;
    				String formattedName = PO.Name;
    				String Duration = hostingProgramMap.get(PO.Hosting_Program__c).Duration__c;
    				String Program_Content =  hostingProgramMap.get(PO.Hosting_Program__c).Program_Content__c;
    				String Hemisphere = hostingProgramMap.get(PO.Hosting_Program__c).Hemisphere__c;
    				String Year = hostingProgramMap.get(PO.Hosting_Program__c).Year__c;
    				String Host_IOC = hostingProgramMap.get(PO.Hosting_Program__c).Host_IOC__c;
    				String Send_IOC = (PO.Send_IOC__c != null)? PO.Send_IOC__c : '';
    				
    				originalName = (Send_IOC != null) ? originalName.remove(Send_IOC) : originalName;
    				originalName = originalName.remove('-');
    				originalName = originalName.remove(Duration);
    				originalName = originalName.remove(Program_Content);
    				originalName = originalName.remove('-');
    				originalName = (Host_IOC != null) ? originalName.remove(Host_IOC) : originalName;
    				    				
    				formattedName = formattedName.replace(originalName, '____');
    				
    				if(!customSettingFormulaSet.contains(formattedName)){
    					customSettingFormulaSet.add(formattedName);	
    				}
    				
    			}
    		}
    		
    		
    		List<MasterToDoListUtil__c> masterToDoListUtilList = [select Formula__c, Master_To_Do_ID__c from MasterToDoListUtil__c where Formula__c in:customSettingFormulaSet];
    		Map<String, MasterToDoListUtil__c> masterToDoListUtilMap = new Map<String, MasterToDoListUtil__c>();
    		
    		for(MasterToDoListUtil__c MTDLU: masterToDoListUtilList){
    			masterToDoListUtilMap.put(MTDLU.Formula__c, MTDLU);
    		} 
    		
    		for(Program_Offer__c PO : Trigger.new){
    			if(PO.To_Do_Template__c == null && hostingProgramMap.containsKey(PO.Hosting_Program__c) == true){

    				String originalName = PO.Name;
    				String formattedName = PO.Name;
    				String Duration = hostingProgramMap.get(PO.Hosting_Program__c).Duration__c;
    				String Program_Content =  hostingProgramMap.get(PO.Hosting_Program__c).Program_Content__c;
    				String Host_IOC = hostingProgramMap.get(PO.Hosting_Program__c).Host_IOC__c;
    				String Send_IOC = PO.Send_IOC__c;
    				
    				originalName = (Send_IOC != null) ? originalName.remove(Send_IOC) : originalName;
    				originalName = originalName.remove('-');
    				originalName = originalName.remove(Duration);
    				originalName = originalName.remove(Program_Content);
    				originalName = originalName.remove('-');
    				originalName = (Host_IOC != null) ? originalName.remove(Host_IOC) : originalName;
    				    				
    				formattedName = formattedName.replace(originalName, '____');
    				
    				if(masterToDoListUtilMap.containsKey(formattedName)){
    					PO.To_Do_Template__c = masterToDoListUtilMap.get(formattedName).Master_To_Do_ID__c;
    				}
    				
    			}
    		}
    		
    	}
    }

    if(Trigger.isAfter){
        if(Trigger.isInsert){
            ProgramOfferTriggerHandler.afterInsert(Trigger.newMap,Trigger.oldMap);
        }else if(Trigger.isUpdate){
            ProgramOfferTriggerHandler.afterUpdate(Trigger.newMap,Trigger.oldMap);
        }

    }
}