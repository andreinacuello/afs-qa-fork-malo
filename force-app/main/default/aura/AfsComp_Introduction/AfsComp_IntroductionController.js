({
	doSave : function(component, event, helper) {
         component.set("v.Spinner","true"); 
		helper.saveIntroduction(component);
        window.scrollTo(0, 0);
	},
    
    onInit :function(component, event, helper){
    	//helper.getAttachmentList(component);
    	window.scrollTo(0, 0);
    },

    handleUploadFinished :function(component, event, helper){
        
        //get file list
    	 component.set("v.Spinner", true);
         var uploadedFiles = event.getParam("files"); 
         var files = component.get("v.Files");
         //var files = [];
         for(var i  = 0 ; i< uploadedFiles.length ; i ++){
            var filObj = new Object();
            filObj.ContentDocumentId = uploadedFiles[i].documentId;
            filObj.Title = uploadedFiles[i].name;
            files.push(filObj);
         }
        if(files.length == 0){
            component.set("v.isFileAdded",false);                
        }else{
            component.set("v.isFileAdded",true);  //file present
        }
        component.set("v.Files",files);
        //helper.getFiles(component, event);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "Files Uploaded successfully."
        });
        toastEvent.fire();
        component.set("v.Spinner", false);
    },
    
    deleteFile :function(component, event, helper){
        component.set("v.Spinner","true"); 
    	helper.deleteFile(component, event);
    },
    
    backToList : function(component, event, helper) {
        component.set("v.parentToggle",false);
    }
})