public with sharing class RemoteSiteParser {
    public static String ApplicationToParticipantApp(Application__c application){
	    String JsonParticipantApp = '';
	    
	    ParticipantApp participantApp = new ParticipantApp();
	    participantApp.sf_participantapp_id = application.Id;
	    participantApp.id = application.Global_Link_Participant_App_ID__c;
	    participantApp.sf_person_id = application.Applicant__c;
	    participantApp.person_id = application.Applicant__r.Global_Link_Person_ID__c;
	    participantApp.sf_family_id = application.Applicant__r.AccountId;
	    participantApp.family_id = application.Applicant__r.Account.Global_Link_Family_ID__c; 
	    participantApp.owner_ioc = application.Applicant__r.Account.IOC_Code__c;
	    participantApp.ioc_code = application.Applicant__r.Account.IOC_Code__c;
	    
	    participantApp.stage = 'Application';
	    participantApp.status = 'Open';	            
       
        //participantApp.mobile_phone = application.Applicant__r.MobilePhone; No esta en participationApp
	    //participantApp.afs_rumor = application.Applicant__r.How_did_you_hear_about_AFS__c; No esta en participationApp
	    participantApp.agreement_ind = application.Applicant__r.Terms_Service_Agreement__c ? 'Y':'N';//Esta en gris en la API
	    //participantApp.keep_me_informed = application.Applicant__r.Keep_me_informed_Opt_in__c; No esta en participationApp
	    //participantApp.english_code = application.Applicant__r.MailingPostalCode; No esta en participationApp
	    participantApp.native_zip = application.Applicant__r.MailingPostalCode; // No actualiza quizas haya que pegarle a Adress
        
        List<String> lstPrefCountry = new List<String>();
        if(!String.isBlank(application.Additional_destination_1__c)){
            lstPrefCountry.add(application.Additional_destination_1__c);
        }
        if(!String.isBlank(application.Additional_destination_2__c)){
            lstPrefCountry.add(application.Additional_destination_2__c);
        }
        if(!String.isBlank(application.Additional_destination_3__c)){
            lstPrefCountry.add(application.Additional_destination_3__c);
        }
        participantApp.preferred_country = String.join(lstPrefCountry,'|');

		participantApp.What_school_do_you_attend = application.Applicant__r.What_school_do_you_attend__c;
		participantApp.graduation_year = convertDateToYYYYMMDD(application.Applicant__r.When_would_did_you_graduate__c);
        participantApp.describe_yourself_as_a_student = String.ValueOf(application.Applicant__r.Describe_yourself_as_a_student__c);
        
        List<String> lstlanguage = new List<String>();
        if(!String.isBlank(application.applicant__r.Language_1__c) && !String.isBlank(application.applicant__r.Language_1_Proficiency__c)){
            lstlanguage.add(application.applicant__r.Language_1__c + ',' + application.applicant__r.Language_1_Proficiency__c);
        }
        if(!String.isBlank(application.applicant__r.Language_2__c) && !String.isBlank(application.applicant__r.Language_2_Proficiency__c)){
            lstlanguage.add(application.applicant__r.Language_2__c + ',' + application.applicant__r.Language_2_Proficiency__c);
        }
        if(!String.isBlank(application.applicant__r.Language_3__c) && !String.isBlank(application.applicant__r.Language_3_Proficiency__c)){
            lstlanguage.add(application.applicant__r.Language_3__c + ',' + application.applicant__r.Language_3_Proficiency__c);
        }
        if(!String.isBlank(application.applicant__r.Language_4__c) && !String.isBlank(application.applicant__r.Language_4_Proficiency__c)){
            lstlanguage.add(application.applicant__r.Language_4__c + ',' + application.applicant__r.Language_4_Proficiency__c);
        }
        participantApp.language_desc_memo = String.join(lstlanguage,'/');
        
        participantApp.bv_record_locator = application.BV_Record_Locator__c;
        participantApp.bv_airline = application.BV_Airline__c;
        participantApp.bv_flight_number = application.BV_Flight_Number__c;
        participantApp.bv_departure_time = application.BV_Departure_Time__c != null ? application.BV_Departure_Time__c.format('yyyy-MM-dd HH:mm:ss'):'';
        participantApp.bv_arrival_time = application.BV_Arrival_Time__c != null ? application.BV_Arrival_Time__c.format('yyyy-MM-dd HH:mm:ss'):'';
        participantApp.bv_departure_airport = application.BV_Departure_Airport__c;
        participantApp.bv_arrival_airport = application.BV_Arrival_Airport__c;
        participantApp.bv_domestic_travel_arrangements = application.BV_Domestic_Travel_Arrangements__c;
        participantApp.bv_arrival_to_gateway_city_with_who = application.BV_Arrival_to_gateway_city_who_accompany__c;
        participantApp.bv_gateway_city = application.BV_Gateway_City__c;
        participantApp.bv_arrive_in_prior_to_the_start = application.BV_Arrive_in_prior_to_the_start__c;
        participantApp.bv_additional_comments = application.BV_Additional_Comments__c;
        participantApp.bv_contact_first_name = application.BV_Contact_First_Name__c;
        participantApp.bv_contact_last_name = application.BV_Contact_Last_Name__c;
        participantApp.bv_contact_phone = application.BV_Contact_Phone__c;
        participantApp.bv_contact_phone_type = application.BV_Contact_Phone__c;
        participantApp.bv_contact_relationship = application.BV_Contact_Relationship__c;
        participantApp.cbh_record_locator = application.CBH_Record_Locator__c;
        participantApp.cbh_airline = application.CBH_Airline__c;
        participantApp.cbh_flight_number = application.CBH_Flight_Number__c;
        participantApp.cbh_departure_time = application.CBH_Departure_Time__c != null ? application.CBH_Departure_Time__c.format('yyyy-MM-dd HH:mm:ss'):'';
        participantApp.cbh_arrival_time = application.CBH_Arrival_Time__c != null ? application.CBH_Arrival_Time__c.format('yyyy-MM-dd HH:mm:ss'):'';
        participantApp.cbh_departure_airport = application.CBH_Departure_Airport__c;
        participantApp.cbh_arrival_airport = application.CBH_Arrival_Airport__c;
        participantApp.cbh_domestic_travel_arrangements = application.CBH_Domestic_Travel_Arrangements__c;
        participantApp.cbh_arrival_to_gateway_city_with_who = application.CBH_Arrival_to_gateway_city_with_who__c;
        participantApp.cbh_gateway_city = application.CBH_Gateway_City__c;
        participantApp.cbh_arrive_in_prior_to_the_start = application.CBH_Arrive_in_prior_to_the_start__c;
        participantApp.cbh_additional_comments = application.CBH_Additional_Comments__c;
        participantApp.cbh_contact_first_name = application.CBH_Contact_First_Name__c;
        participantApp.cbh_contact_last_name = application.CBH_Contact_Last_Name__c;
        participantApp.cbh_contact_phone = application.CBH_Contact_Phone__c;
        participantApp.cbh_contact_phone_type = application.CBH_Contact_Phone__c;
        participantApp.cbh_contact_relationship = application.CBH_Contact_Relationship__c;
        participantApp.are_you_attending_university_college = application.Applicant__r.AAre_you_attending_University_College_o__c;
        participantApp.name_of_university_college_school = application.Applicant__r.Name_of_University_College_School__c;
        participantApp.criminal_convictions = application.Applicant__r.Criminal_Convictions__c;
        participantApp.no_restrictions_or_allergies = application.Applicant__r.No_restrictions_or_allergies__c ? 'Y' : 'N';
        participantApp.limited_in_the_activities_i_can_do = application.Applicant__r.Limited_in_the_activities_I_can_do__c ? 'Y' : 'N';
        participantApp.have_allergies = application.Applicant__r.Have_allergies__c ? 'Y' : 'N';
        participantApp.take_medications = application.Applicant__r.Take_medications__c ? 'Y' : 'N';
        participantApp.can_t_live_with_pets_s = application.Applicant__r.Can_t_live_with_pets_s__c ? 'Y' : 'N';
        participantApp.can_t_live_with_a_smoker = application.Applicant__r.Can_t_live_with_a_smoker__c ? 'Y' : 'N';
        participantApp.medical_condition_comments  = application.Applicant__r.Medical_Condition_Comments__c;
        participantApp.dietary_restrictions = application.Applicant__r.No_dietary_restrictions__c ? 'Y' : 'N';
        participantApp.have_food_allergies = application.Applicant__r.Have_food_allergies__c ? 'Y' : 'N';
        participantApp.vegetarian = application.Applicant__r.Vegetarian__c ? 'Y' : 'N';
        participantApp.vegan = application.Applicant__r.Vegan__c ? 'Y' : 'N';
        participantApp.kosher = application.Applicant__r.Kosher__c ? 'Y' : 'N';
        participantApp.halal = application.Applicant__r.Halal__c ? 'Y' : 'N';
        participantApp.celiac = application.Applicant__r.Celiac__c ? 'Y' : 'N';
        participantApp.dietary_comments = application.Applicant__r.Dietary_Comments__c;
        participantApp.willing_to_change_diet_during_program = application.Applicant__r.Willing_to_change_diet_during_program__c;
        participantApp.treated_for_any_health_issues_physical = application.Applicant__r.treated_for_any_health_issues_physical__c;
        participantApp.health_issue_description = application.Applicant__r.Health_Issue_Description__c;
        participantApp.what_are_you_looking_for = application.What_are_you_looking_for__c;
        participantApp.what_do_you_want_out_of_this_experience = application.What_do_you_want_out_of_this_experience__c;
        participantApp.how_would_friends_family_describe_you = application.How_would_friends_family_describe_you__c;
        participantApp.tell_us_about_yourself = application.Tell_us_about_yourself__c;
        participantApp.program_you_participated = application.Program_you_participated__c;
        participantApp.dual_citizenship = application.Applicant__r.Dual_citizenship__c;
        participantApp.home_mother = application.Applicant__r.Home_Mother__c;
        participantApp.home_father = application.Applicant__r.Home_Father__c;
        participantApp.home_stepmother = application.Applicant__r.Home_Stepmother__c;
        participantApp.home_stepfather = application.Applicant__r.Home_Stepfather__c;
        participantApp.home_sister_stepsister = application.Applicant__r.Home_Sister_Stepsister__c;
        participantApp.home_brother_stepbrother = application.Applicant__r.Home_Brother_Stepbrother__c;
        participantApp.home_grandmother = application.Applicant__r.Home_Grandmother__c;
        participantApp.home_grandfather = application.Applicant__r.Home_Grandfather__c;
        participantApp.home_other = application.Applicant__r.Home_Other__c;
        participantApp.home_other_relationship = application.Applicant__r.Home_Other_Relationship__c;
        participantApp.instagram = application.Applicant__r.Instagram__c;
        participantApp.linkedin = application.Applicant__r.LinkedIn__c;
        participantApp.facebook = application.Applicant__r.Facebook__c;
        participantApp.twitter = application.Applicant__r.Twitter__c;
        participantApp.snapchat = application.Applicant__r.Snapchat__c;
        participantApp.youtube = application.Applicant__r.YouTube__c;
        participantApp.practice_religion_ind = application.Access_to_religious_services__c == 'Required' ? 'Y' : 'N';
        participantApp.pets = application.Ability_to_live_with_household_pets__c != null ? application.Ability_to_live_with_household_pets__c.substring(0,2) : null;
        participantApp.open_to_same_sex_host_parents = application.Open_to_Same_Sex_Host_Parents__c ? 'Y' : 'N';
        participantApp.open_to_single_host_parent = application.Open_to_Single_Host_Parent__c ? 'Y' : 'N';
        participantApp.open_to_sharing_a_host_family = application.Open_to_Sharing_a_host_family__c ? 'Y' : 'N';
        participantApp.lgbtq_member = application.Applicant__r.LGBTQ_Member__c;
        participantApp.why_is_global_competence_important_for_students = application.Why_is_Global_Competence_important_for_s__c;
        participantApp.how_to_be_a_global_citizen = application.How_to_be_a_Global_Citizen__c;
        participantApp.things_to_share = application.Things_to_share__c;
        participantApp.describe_a_normal_day_in_your_life = application.Describe_a_normal_day_in_your_life__c;
        participantApp.what_you_bring_to_your_afs_experience = application.What_you_bring_to_your_AFS_experience__c;
        participantApp.food_thoughts = application.Food_thoughts__c;
        participantApp.favorite_travel_destinations_books = application.Favorite_travel_destinations_books__c;
        participantApp.video_for_host_family = application.Video_for_Host_Family__c;
        participantApp.tshirt_info = application.Applicant__r.Tshirt__c;



	    JsonParticipantApp = JSON.serialize(participantApp);
	    
	    return JsonParticipantApp;
	}
	  
	public static String ContactToPerson(Contact contact){
	    String JsonPerson = '';
	    system.debug('contact='+contact);
	    Person newPerson = new Person();
	    newPerson.sf_person_id = contact.Id;
	    newPerson.id = contact.Global_Link_Person_ID__c;
	    newPerson.family_id = contact.Account.Global_Link_Family_ID__c; 
	    newPerson.sf_family_id = contact.AccountId;
	    newPerson.owner_ioc = contact.Account.IOC_Code__c;
	    newPerson.ioc_code = contact.Account.IOC_Code__c;
	    newPerson.title = contact.Title;
	    newPerson.native_title = contact.Title;
	    
	    newPerson.english_firstname = contact.FirstName;
	    newPerson.english_lastname = contact.LastName;
	    newPerson.native_firstname = contact.FirstName;
	    newPerson.native_lastname = contact.LastName;
	    
	    newPerson.sex = (contact.Gender__c != null) ? contact.Gender__c.substring(0, 1) : 'M';//Que valores se esperan?
	    newPerson.preferred_email = contact.Email;
        newPerson.date_of_birth = contact.Birthdate;
	    
	    JsonPerson = JSON.serialize(newPerson);
	    return JsonPerson;
	}
	
	public static String AttachmentToAttachmentRawData(Attachment attachment){
	    String JsonPerson = '';
	    
	    AttachmentRawData newAttachmentRawData = new AttachmentRawData();
		newAttachmentRawData.id = RemoteSiteParser.generateNewUUID();
		newAttachmentRawData.sf_attachment_id = attachment.Id;
		newAttachmentRawData.master_record_id = 'D2B8B2FC-72A7-4679-A504-7DAD78B596B1';
		newAttachmentRawData.mime_type = attachment.ContentType;
		newAttachmentRawData.raw_data = EncodingUtil.Base64Encode(attachment.Body);
	    
	    JsonPerson = JSON.serialize(newAttachmentRawData);
	    return JsonPerson;
	}
  
  	public static ApiResponse DeserializeResponse(String response){
      
		ApiResponse person = (ApiResponse) System.JSON.deserialize(response, ApiResponse.class);
      
		return person;
    }
    
    public static String generateNewUUID(){
		Blob b = Crypto.GenerateAESKey(128);
    	String h = EncodingUtil.ConvertTohex(b);
    	String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
    
    	return guid.toUppercase();
    }
  
	public class ApiResponse{
	      public String status {get; set;}
	      public String id {get; set;}
	      public String errorMsg {get; set;}
	}
	
	public class AttachmentRawData{
	      public String id {get; set;}
	      public String sf_attachment_id {get; set;}
	      public String master_record_id {get; set;}
	      public String mime_type {get; set;}
	      public String raw_data {get; set;}
	}
  
    public class Person{
      public String sf_person_id {get; set;}
      public String sf_family_id {get; set;}
      public String id {get; set;}
      public String family_id {get; set;}
      public String ioc_code {get; set;}
      public String owner_ioc {get; set;}
      public String current_address_id {get; set;}
      public String title {get; set;}
      public String native_title {get; set;}
      public String english_firstname {get; set;}
      public String english_lastname {get; set;}
      public String native_firstname {get; set;}
      public String native_lastname {get; set;}
      public String sex {get; set;}
      public Date date_of_birth {get; set;}
      public String english_zip {get; set;} 
      public String preferred_email {get; set;}
      public String mobile_phone {get; set;}
      public String keep_me_informed {get; set;}
      public Date keep_me_informed_date {get; set;}
      public String invisible_to_volunteer {get; set;}
      public String live_at_home_ind {get; set;}
      public String person_role {get; set;}
      public String source {get; set;}
      public Date created_date {get; set;}
      public String created_by {get; set;}
      public Date last_modified_date {get; set;}
      public String last_modified_by {get; set;}
      public String duplicate_source {get; set;}
      public String bad_address_ind {get; set;}
    }
    
    public class ParticipantApp{
      public String sf_participantapp_id {get; set;}
      public String id {get; set;}
      
      public String sf_person_id {get; set;}
      public String person_id {get; set;}
      
      public String sf_family_id {get; set;}
      public String family_id {get; set;}
      
      public String owner_ioc {get; set;}
      public String ioc_code {get; set;}
      
      public String stage {get; set;} //Application, Admission, Preparation, Participation
      public String status {get; set;} //Open, Closed
      
      public String english_firstname {get; set;}
      public String native_firstname {get; set;}
      public String english_lastname {get; set;}
      public String native_last_name {get; set;}
      public String english_code {get; set;}
      public String native_zip {get; set;}
      public String mobile_phone {get; set;}
      public String preferred_country {get; set;}
      public String afs_rumor {get; set;}
      public String agreement_ind {get; set;}
      public String keep_me_informed {get; set;}
      public String What_school_do_you_attend {get; set;}
      public String school {get; set;}
      public String school_id {get; set;}
      public String graduation_year {get; set;}
      public String describe_yourself_as_a_student {get; set;}
      public String are_you_attending_university_college {get; set;}
      public String name_of_university_college_school {get; set;}
      public String language_desc_memo {get; set;}
      public String criminal_convictions {get; set;}
      public String physical_restrictions_ind {get; set;}
      public String no_restrictions_or_allergies {get; set;}
      public String limited_in_the_activities_i_can_do {get; set;}
      public String have_allergies {get; set;}
      public String take_medications {get; set;}
      public String can_t_live_with_pets_s {get; set;}
      public String can_t_live_with_a_smoker {get; set;}
      public String medical_condition_comments  {get; set;}
      public String dietary_restrictions_ind {get; set;}
      public String dietary_restrictions {get; set;}
      public String have_food_allergies {get; set;}
      public String vegetarian {get; set;}
      public String vegan {get; set;}
      public String kosher {get; set;}
      public String halal {get; set;}
      public String celiac {get; set;}
      public String dietary_comments {get; set;}
      public String willing_to_change_diet_during_program {get; set;}
      public String treated_for_any_health_issues_physical {get; set;}
      public String health_issue_description {get; set;}
      public String smoker_ind {get; set;}
      public String what_are_you_looking_for {get; set;}
      public String what_do_you_want_out_of_this_experience {get; set;}
      public String how_would_friends_family_describe_you {get; set;}
      public String tell_us_about_yourself {get; set;}
      public String program_you_participated {get; set;}
      public String dual_citizenship {get; set;}
      public String second_citizenship {get; set;}
      public String emergency_contact {get; set;}
      public String live_at_home_ind {get; set;}
      public String this_is_your_emergency_contact {get; set;}
      public Decimal home_mother {get; set;}
      public Decimal home_father {get; set;}
      public Decimal home_stepmother {get; set;}
      public Decimal home_stepfather {get; set;}
      public Decimal home_sister_stepsister {get; set;}
      public Decimal home_brother_stepbrother {get; set;}
      public Decimal home_sister_stepsisters_age {get; set;}
      public Decimal home_brother_stepbrothers_age {get; set;}
      public Decimal home_grandmother {get; set;}
      public Decimal home_grandfather {get; set;}
      public Decimal home_other {get; set;}
      public String home_other_relationship {get; set;}
      public String instagram {get; set;}
      public String linkedin {get; set;}
      public String facebook {get; set;}
      public String twitter {get; set;}
      public String snapchat {get; set;}
      public String youtube {get; set;}
      public String practice_religion_ind {get; set;}
      public String pets {get; set;}
      public String open_to_same_sex_host_parents {get; set;}
      public String open_to_single_host_parent {get; set;}
      public String open_to_sharing_a_host_family {get; set;}
      public String lgbtq_member {get; set;}
      public String why_is_global_competence_important_for_students {get; set;}
      public String how_to_be_a_global_citizen {get; set;}
      public String things_to_share {get; set;}
      public String describe_a_normal_day_in_your_life {get; set;}
      public String what_you_bring_to_your_afs_experience {get; set;}
      public String food_thoughts {get; set;}
      public String favorite_travel_destinations_books {get; set;}
      public String video_for_host_family {get; set;}
      public String tshirt_info {get; set;}
      public String bv_record_locator {get; set;}
      public String bv_airline {get; set;}
      public String bv_flight_number {get; set;}
      public String bv_departure_time {get; set;}
      public String bv_arrival_time {get; set;}
      public String bv_departure_airport {get; set;}
      public String bv_arrival_airport {get; set;}
      public String bv_domestic_travel_arrangements {get; set;}
      public String bv_arrival_to_gateway_city_with_who {get; set;}
      public String bv_gateway_city {get; set;}
      public String bv_arrive_in_prior_to_the_start {get; set;}
      public String bv_additional_comments {get; set;}
      public String bv_contact_first_name {get; set;}
      public String bv_contact_last_name {get; set;}
      public String bv_contact_phone {get; set;}
      public String bv_contact_phone_type {get; set;}
      public String bv_contact_relationship {get; set;}
      public String cbh_record_locator {get; set;}
      public String cbh_airline {get; set;}
      public String cbh_flight_number {get; set;}
      public String cbh_departure_time {get; set;}
      public String cbh_arrival_time {get; set;}
      public String cbh_departure_airport {get; set;}
      public String cbh_arrival_airport {get; set;}
      public String cbh_domestic_travel_arrangements {get; set;}
      public String cbh_arrival_to_gateway_city_with_who {get; set;}
      public String cbh_gateway_city {get; set;}
      public String cbh_arrive_in_prior_to_the_start {get; set;}
      public String cbh_additional_comments {get; set;}
      public String cbh_contact_first_name {get; set;}
      public String cbh_contact_last_name {get; set;}
      public String cbh_contact_phone {get; set;}
      public String cbh_contact_phone_type {get; set;}
      public String cbh_contact_relationship {get; set;}
  
      public List<Person> familyMembers {get; set;}
    }
    
    public static String convertDateToYYYYMMDD(Date datEntry){
        String strDate;
        if(datEntry != null){
            String year = String.ValueOf(datEntry.year());
            String month = String.ValueOf(datEntry.month());
            month = month.length() == 1 ? '0'+ month : month;
            String day = String.ValueOf(datEntry.month());
            day = day.length() == 1 ? '0'+ day : day;
            strDate = year + month + day;
       }
        return strDate;
    }  
    
    public static String eventInterestToPerson(Contact con, Application__c app){
        String JsonPerson = '';
	   
	    Person newPerson = new Person();
        newPerson.sf_person_id = con.Id;
        newPerson.id = con.Global_Link_Person_ID__c;
        newPerson.sf_family_id = con.Account.Global_Link_Family_ID__c;
	    newPerson.english_firstname = con.FirstName;
	    newPerson.english_lastname = con.LastName;
	    newPerson.native_firstname = con.FirstName;
	    newPerson.native_lastname = con.LastName;	    
	    newPerson.preferred_email = con.Email;
        newPerson.date_of_birth = con.Birthdate;
        newPerson.english_zip = con.MailingPostalCode;
	    newPerson.mobile_phone = con.mobilephone;
        newPerson.keep_me_informed = con.Keep_me_informed_Opt_in__c ? 'Y' : 'N';
        newPerson.keep_me_informed_date = System.Today();
        newPerson.ioc_code = app.Sending_Partner__r.IOC_Code__c;
        newPerson.owner_ioc = app.Sending_Partner__r.IOC_Code__c;
        newPerson.invisible_to_volunteer = 'N';
        newPerson.live_at_home_ind = 'Y';
        newPerson.person_role = 'Active';
        newPerson.source = 'SF Application';
        newPerson.owner_ioc = app.Sending_Partner__r.IOC_Code__c;
        newPerson.created_date = System.Today();
        //newPerson.created_by = is necessary or GL automatic assign?
        newPerson.last_modified_date = System.Today();
        //newPerson.last_modified_by = is necessary or GL automatic assign?
        newPerson.duplicate_source = 'SF Application';
        newPerson.bad_address_ind = 'N';
        
	    JsonPerson = JSON.serialize(newPerson);
	    return JsonPerson;
    }
	
    public static String eventInterestToServiceAndOAJson(Contact con, Application__c app){
         String strJSON = '';
	   
	    ServiceAndOAInterest newSOA = new ServiceAndOAInterest();
        newSOA.Id = app.GL_Service_Id__c;
        newSOA.sf_service_id = app.Id;
        newSOA.person_id = con.Global_Link_Person_ID__c;
		newSOA.agreement_ind = con.Terms_Service_Agreement__c ? 'Y' : 'N';
        newSOA.agreement_date = System.Today();
        newSOA.service_type = 'Sending';
        newSOA.status = 'Open';
        newSOA.inquiry_source = 'SF Application';
        //newSOA.responsible_org = the name of the recordtype or ID?
        //newSOA.owner = the name of the recordtype or ID?
        //newSOA.created_by = is necessary or GL automatic assign?
        newSOA.last_modified_date = System.Today();
        //newSOA.last_modified_by = is necessary or GL automatic assign?
        newSOA.last_modified_by_owner_date = System.Today();
        newSOA.post_selection_status_code = 'New';
        newSOA.post_selection_status_date = System.Today();
        newSOA.last_updated_date = System.Today();
        //newSOA.last_updated_by = is necessary or GL automatic assign?    
            
        strJSON = JSON.serialize(newSOA);
        return strJSON;
    }
    
    public class ServiceAndOAInterest{   
        public String Id{get;set;}
        public String sf_service_id{get;set;}
        public String person_id{get;set;}
        public String agreement_ind{get;set;}
        public Date agreement_date{get;set;}
        public String service_type{get;set;}
        public String status{get;set;}
        public String inquiry_source{get;set;}
        public String  responsible_org{get;set;}
        public String owner{get;set;}
        public String created_by{get;set;}
        public Date last_modified_date{get;set;}
        public String last_modified_by{get;set;}
        public Date last_modified_by_owner_date{get;set;}
        public String post_selection_status_code{get;set;}
        public Date post_selection_status_date{get;set;}
        public Date last_updated_date{get;set;}
        public String last_updated_by{get;set;}
    }
}