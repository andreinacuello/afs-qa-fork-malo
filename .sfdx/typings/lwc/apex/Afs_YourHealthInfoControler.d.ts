declare module "@salesforce/apex/Afs_YourHealthInfoControler.getPicklistValues" {
  export default function getPicklistValues(param: {objObject: any, fld: any, isRadioList: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_YourHealthInfoControler.getHealthInfoRecord" {
  export default function getHealthInfoRecord(param: {strContactId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_YourHealthInfoControler.saveRecord" {
  export default function saveRecord(param: {objHealth: any, recId: any, isDraft: any}): Promise<any>;
}
