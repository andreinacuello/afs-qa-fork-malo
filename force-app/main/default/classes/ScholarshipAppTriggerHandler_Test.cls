@isTest
public class ScholarshipAppTriggerHandler_Test {
    @testSetup
    public static void setup_method(){
        Account acc = Util_Test.create_AFS_Partner();
        Hosting_Program__c hp = Util_Test.create_Hosting_Program(acc);
        Program_Offer__c po = Util_Test.create_Program_Offer(acc,hp);
        Scholarship__c sch = Util_Test.create_Scholarship(acc);
        Master_To_Do__c mtd = Util_Test.create_Master_To_Do(null,sch,null,'Custom_for_Partner');
        Insert mtd;
        Contact con = Util_Test.create_Conact(acc, 'Applicant','Test contact');
        AFS_Triggers_Switch__c swi = Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
        Application__c app = Util_Test.create_Applicant(con,po);
        Scholarship_Application__c schApp = Util_Test.create_Scholarship_Application(sch,app);
        Insert schApp;
    }
    
    public static testMethod void createToDosIntoAppWhenSchAppChangedToApplying_Test(){
        Scholarship_Application__c schApp = [SELECT Id, Stage__c,Application__c FROM Scholarship_Application__c Limit 1];
        
        Test.startTest();
        schApp.Stage__c = 'Applying';
        Update schApp;
        Test.stopTest();
        
        List<To_Do_Item__c> lstToDoItem = [SELECT id FROM To_Do_Item__c WHERE Application__c = :schApp.Application__c];
        System.assertEquals(1, lstToDoItem.Size());
        
    }
    
    public static testMethod void canceledToDosAppWhenSchAppChangedToLost_Test(){
        Scholarship_Application__c schApp = [SELECT Id, Stage__c,Application__c FROM Scholarship_Application__c Limit 1];
        
        Test.startTest();
        schApp.Stage__c = 'Applying';
        Update schApp;
        List<To_Do_Item__c> lstToDoItem = [SELECT id FROM To_Do_Item__c WHERE Scholarship_Application__c = :schApp.Id];
        System.assertEquals(1, lstToDoItem.Size());
        schApp.Stage__c = 'Lost';
        Update schApp;
        Test.stopTest();
        
        lstToDoItem = [SELECT id, Status__c FROM To_Do_Item__c WHERE Scholarship_Application__c = :schApp.Id];
        System.assertEquals('Canceled', lstToDoItem[0].Status__c);
       
    }
    
    @isTest static void whenSchAppIsCreateSumPreAppfee_test() {
		Test.startTest();
        Account newAccount1 = Util_Test.create_AFS_Partner2('Test account 1'); //It's already insert
        
        Contact newContact = Util_Test.create_Conact(newAccount1,'Applicant','Test newContact'); //It's already insert
        
        Hosting_Program__c newHostingProgram1 = Util_Test.create_Hosting_Program(newAccount1); //It's already insert
        
        Id idtestproofff =  Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        Program_Offer__c newProgramOffer1 = Util_Test.create_Program_Offer2(newAccount1,newHostingProgram1,'Test-programoff-1',idtestproofff);

        Id RecordTypeIdApp =  Schema.SObjectType.application__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        application__c newApplication = Util_Test.create_Applicant2(newContact,newProgramOffer1,RecordTypeIdApp);
        
        Scholarship__c newScholarship1 = Util_Test.create_Scholarship2(newAccount1,50,'Test Scholarship 1');
        List<Scholarship__c> newScholarshipList = new List<Scholarship__c>();
        newScholarshipList.add(newScholarship1);
        insert newScholarshipList;
        
        Sch_in_Program__c newSchInPro1 = Util_Test.create_ScholarshipProgram(newScholarship1,newProgramOffer1);
        
        Scholarship_Application__c newSchApp1 = Util_Test.create_ScholarshipApplication2(newScholarship1,newContact,newApplication,'Applying');
        List<Scholarship_Application__c> newSchAppList = new List<Scholarship_Application__c>();
        newSchAppList.add(newSchApp1);
        
        
        insert newSchAppList;
        
        
        application__c finalApp = [SELECT Id, Scholarship_Pre_Application_Fee__c FROM application__c WHERE Id =: newSchAppList[0].Application__c];
        system.assertEquals(50, finalApp.Scholarship_Pre_Application_Fee__c);     
		Test.stopTest();   
    }
    
    @isTest static void whenSchAppIsDeleteSumPreAppfee_test() {
		Test.startTest();
        Account newAccount1 = Util_Test.create_AFS_Partner2('Test account 1'); //It's already insert
         Account newAccount2 = Util_Test.create_AFS_Partner2('Test account 2');
        Contact newContact = Util_Test.create_Conact(newAccount1,'Applicant','Test newContact'); //It's already insert
        
        Hosting_Program__c newHostingProgram1 = Util_Test.create_Hosting_Program(newAccount1); //It's already insert
        
        Id idtestproofff =  Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        Program_Offer__c newProgramOffer1 = Util_Test.create_Program_Offer2(newAccount1,newHostingProgram1,'Test-programoff-1',idtestproofff);
        //Program_Offer__c po = Util_Test.create_Program_Offer(newAccount1,newHostingProgram1);

        Id RecordTypeIdApp =  Schema.SObjectType.application__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        application__c newApplication = Util_Test.create_Applicant2(newContact,newProgramOffer1,RecordTypeIdApp);
        
        Scholarship__c newScholarship1 = Util_Test.create_Scholarship2(newAccount1,20,'Test Scholarship 1');
        Scholarship__c newScholarship2 = Util_Test.create_Scholarship2(newAccount2,10,'Test Scholarship 2');
        List<Scholarship__c> newScholarshipList = new List<Scholarship__c>();
        newScholarshipList.add(newScholarship1);
        newScholarshipList.add(newScholarship2);
        insert newScholarshipList;
        
        Sch_in_Program__c newSchInPro1 = Util_Test.create_ScholarshipProgram(newScholarship1,newProgramOffer1);
        
        Scholarship_Application__c newSchApp1 = Util_Test.create_ScholarshipApplication2(newScholarship1,newContact,newApplication,'Applying');
        Scholarship_Application__c newSchApp2 = Util_Test.create_ScholarshipApplication2(newScholarship2,newContact,newApplication,'Won');
        
        List<Scholarship_Application__c> newSchAppList = new List<Scholarship_Application__c>();
        newSchAppList.add(newSchApp1);
        newSchAppList.add(newSchApp2);
        insert newSchAppList;
        
       
        Account newAccount3 = Util_Test.create_AFS_Partner2('Test account 3');
        
        
        delete newSchAppList;
        Test.stopTest();
        
        application__c finalApp = [SELECT Id, Scholarship_Pre_Application_Fee__c FROM application__c WHERE Id =: newApplication.Id ];
        
        system.assertEquals(0, finalApp.Scholarship_Pre_Application_Fee__c);        
    }
    
    @isTest static void whenSchAppIsUpdateSumPreAppfee_test() {
		Test.startTest();
        Account newAccount1 = Util_Test.create_AFS_Partner2('Test account 1'); //It's already insert
         Account newAccount2 = Util_Test.create_AFS_Partner2('Test account 2');
        Contact newContact = Util_Test.create_Conact(newAccount1,'Applicant','Test newContact'); //It's already insert
        
        Hosting_Program__c newHostingProgram1 = Util_Test.create_Hosting_Program(newAccount1); //It's already insert
        
        Id idtestproofff =  Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        Program_Offer__c newProgramOffer1 = Util_Test.create_Program_Offer2(newAccount1,newHostingProgram1,'Test-programoff-1',idtestproofff);
        //Program_Offer__c po = Util_Test.create_Program_Offer(newAccount1,newHostingProgram1);

        Id RecordTypeIdApp =  Schema.SObjectType.application__c.getRecordTypeInfosByName().get('Flagship').getRecordTypeId();
        application__c newApplication = Util_Test.create_Applicant2(newContact,newProgramOffer1,RecordTypeIdApp);
        
        Scholarship__c newScholarship1 = Util_Test.create_Scholarship2(newAccount1,20,'Test Scholarship 1');
        Scholarship__c newScholarship2 = Util_Test.create_Scholarship2(newAccount2,10,'Test Scholarship 2');
        List<Scholarship__c> newScholarshipList = new List<Scholarship__c>();
        newScholarshipList.add(newScholarship1);
        newScholarshipList.add(newScholarship2);
        insert newScholarshipList;
        
        Sch_in_Program__c newSchInPro1 = Util_Test.create_ScholarshipProgram(newScholarship1,newProgramOffer1);
        
        Scholarship_Application__c newSchApp1 = Util_Test.create_ScholarshipApplication2(newScholarship1,newContact,newApplication,'Applying');
        Scholarship_Application__c newSchApp2 = Util_Test.create_ScholarshipApplication2(newScholarship1,newContact,newApplication,'Won');
        
        List<Scholarship_Application__c> newSchAppList = new List<Scholarship_Application__c>();
        newSchAppList.add(newSchApp1);
        newSchAppList.add(newSchApp2);
        insert newSchAppList;
        
       
        Account newAccount3 = Util_Test.create_AFS_Partner2('Test account 3');
        
        newSchAppList[0].stage__c = 'Lost';
        update newSchAppList[0];
        
        
        application__c finalApp = [SELECT Id, Scholarship_Pre_Application_Fee__c FROM application__c WHERE Id =: newApplication.Id ];
        System.debug('finalApp.Scholarship_Pre_Application_Fee__c: ' + finalApp.Scholarship_Pre_Application_Fee__c);
        //system.assertEquals(20, finalApp.Scholarship_Pre_Application_Fee__c);   
        
        Test.stopTest();
    }
    
}