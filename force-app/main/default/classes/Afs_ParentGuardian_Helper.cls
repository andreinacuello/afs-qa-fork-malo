public without sharing class Afs_ParentGuardian_Helper {
    
    public static  void deleteParentGuardian(String reationId){
    	List<npe4__Relationship__c>	  relationShipList = [select id,npe4__RelatedContact__c from   npe4__Relationship__c  where Id =: reationId];
        
        set<Id> IdContactSetTodelete = new set<Id>();
        for(npe4__Relationship__c relationShipRec : relationShipList ){
            	IdContactSetTodelete.add(relationShipRec.npe4__RelatedContact__c);
            
        }
       
        List<contact>	relatedContactList = [select id from  contact  where Id in : IdContactSetTodelete];
        
        if(relationShipList.size() > 0 ){
            delete relationShipList;
        }
        if(relatedContactList.size() > 0 ){
          delete relatedContactList;
        }
     }

}