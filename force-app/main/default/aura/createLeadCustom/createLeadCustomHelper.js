({
    onSuccess: function(component,payload){    
        var resultToast = $A.get("e.force:showToast");
        
        console.log('payload.id=',payload.id);
        resultToast.setParams({
            "type":"success",
            "title": "Success!",
            "message": "Record Saved Successfully"
        });
        resultToast.fire();
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
            "recordId": payload.id
        });
        navEvt.fire();
    },
    onError: function(component){
        var payload = event.getParams().error;
        var resultToast = $A.get("e.force:showToast");
        resultToast.setParams({
            "type":"error",
            "title": "Record was not saved",
            "message": payload.body.message
        });
        resultToast.fire();
    },
    cancel: function(component){
        var homeEvent = $A.get("e.force:navigateToObjectHome");
        homeEvent.setParams({
            "scope": "Lead"
        });
        homeEvent.fire();
    }
})