({
	createClone : function(component, event, helper) {
        var getRecordName = component.get("v.selectRecordName");
        var getRecordId = component.get("v.recordId");
        helper.createCloneHelper(component, event,getRecordName, getRecordId);
	}
})