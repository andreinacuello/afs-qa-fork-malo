@isTest
public class ZipCodeAssigments_Queueable_Test {
    public static testMethod void execute_test(){
    	SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete','{"response":[{"statusCode":"200","errorMsg":null}],"zipcodeAssignments":[{"id":"AD37C54C-D974-43C7-9C9C-570AA4BC0D9C","ioc_code":"ARG","zip_from":"11000","zip_to":"99999","assigned_org_id":"A5FCD8D3-C346-4E83-94CD-1A35CD5DFFCC"}]}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        List<String> lstIOCs = new List<String>{'ARG'};
        
        
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AFS_Chapters').getRecordTypeId();
        Account acc = new Account(RecordTypeId = recordTypeId, Name = 'Test', IOC_Code__c = 'ARG', Global_Link_External_Id__c = 'A5FCD8D3-C346-4E83-94CD-1A35CD5DFFCC'); 
		Insert acc;

		Test.startTest();
        System.enqueueJob(new ZipCodeAssigments_Queueable(lstIOCs,0));
		Test.stopTest();
		
        acc = [SELECT Zip_From__c, Zip_To__c From Account WHERE Id = :acc.Id];
        
		System.Assert(String.valueOf(acc.Zip_From__c) == '11000' && String.valueOf(acc.Zip_To__c) == '99999');        
    }    
}