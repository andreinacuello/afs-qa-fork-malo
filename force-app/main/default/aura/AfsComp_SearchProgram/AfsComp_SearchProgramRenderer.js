({
	afterRender : function(component, helper) {
        this.superAfterRender();
        component.set("v.mapOfValues", {}); 
        var cmpEvent = component.getEvent("programsearchevent");        
        cmpEvent.setParams({
            "MapofPiclistValues" : component.get("v.mapOfValues")});
        cmpEvent.fire();
    }
})