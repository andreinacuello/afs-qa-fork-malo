global without sharing class Afs_ParentGuardianCtrl {
     @AuraEnabled
    global static List<ParentGuardianWrapper> getParentGuardianList (String contactID){
        List<npe4__Relationship__c> parentGaurdianRelationList	 =	[select id,npe4__Type__c,npe4__Contact__c,npe4__RelatedContact__c,They_live_together__c,This_is_your_emergency_contact__c
                                                            from npe4__Relationship__c  where npe4__Contact__c =: contactID order by createddate  asc];
       
       
        set<Id> relatedContactSet = new set<Id>();
        
        for(npe4__Relationship__c parentRelationRec : parentGaurdianRelationList ){
            relatedContactSet.add(parentRelationRec.npe4__RelatedContact__c);
        }
        
        List<ParentGuardianWrapper> parentGuardianWrapperList  = new  List<ParentGuardianWrapper>();
        
        map<Id,contact> idToContactMap = new map<Id,contact>([select Id,Email,Phone,FirstName,Lastname,AccountId,Maiden_Name__c from contact  where Id in : relatedContactSet]);
        
       	for(npe4__Relationship__c parentRelationRec : parentGaurdianRelationList ){
            if(idToContactMap.containsKey(parentRelationRec.npe4__RelatedContact__c)){
                ParentGuardianWrapper parentWrapper = new ParentGuardianWrapper(idToContactMap.get(parentRelationRec.npe4__RelatedContact__c),parentRelationRec);
                parentGuardianWrapperList.add(parentWrapper);
            }else{
                ParentGuardianWrapper parentWrapper = new ParentGuardianWrapper(null,parentRelationRec);
                parentGuardianWrapperList.add(parentWrapper);
            }

       	}
        return parentGuardianWrapperList;
    }
    
    @AuraEnabled
    global static void deleteParentGuardian(String reationId){
    
    		Afs_ParentGuardian_Helper.deleteParentGuardian(reationId);
    }
    
    global class ParentGuardianWrapper{
        @AuraEnabled
        global Contact relatedContact {get;set;}
        @AuraEnabled
        global npe4__Relationship__c relation{get;set;}
        @AuraEnabled
        global string parentSelected{get;set;} 
        public ParentGuardianWrapper(Contact RelatedContact ,npe4__Relationship__c Relation){
            this.relatedContact = RelatedContact;
            this.relation = Relation;
            if(Relation.npe4__Type__c != null && Relation.npe4__Type__c != ''){
                if(Relation.npe4__Type__c !='Father' &&  Relation.npe4__Type__c !='Mother'){
                    this.parentSelected = 'Other'; 
                }else{
                   this.parentSelected = relation.npe4__Type__c;
                   this.relation.npe4__Type__c = '';
                }
            }
        }
        
    }
       
}