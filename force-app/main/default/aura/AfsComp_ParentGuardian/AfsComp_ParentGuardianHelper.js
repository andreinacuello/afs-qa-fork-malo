({
	getParentGuardianList : function(component,helper) {
		var action = component.get("c.getParentGuardianList");
        var  contactId = 	component.get("v.contactInfo.Id");
        //0033B00000Mqk4z
         action.setParams({"contactID" : contactId });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
               
               var  parentData = response.getReturnValue();
                if($A.util.isEmpty(parentData)){
                  	 this.addParentBlankRec(component,helper);
                }else{  
                    component.set("v.ParentGuardianWrapper",parentData);
                }

              
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    setData : function(component,helper) {
    		
    
    },
     getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    },
    addParentBlankRec : function(component,helper) {
        var relatedContact = {'FirstName': '', 'LastName' : '', 'Email':'' ,'Phone': '', 'AccountId' : ''};
        	var relation = {
                'npe4__Type__c': '',
                'npe4__Contact__c': '', 
                'npe4__RelatedContact__c':null, 
                'They_live_together__c': false,
                'This_is_your_emergency_contact__c' :false
                }; 
             
         var parentRec = {"relatedContact" :relatedContact , "relation" : relation , "parentSelected" : ""};
         
         var ParentGuardianWrapper =  component.get("v.ParentGuardianWrapper");
         ParentGuardianWrapper.push(parentRec);
         component.set("v.ParentGuardianWrapper",JSON.parse(JSON.stringify(ParentGuardianWrapper)));
       		        
    },
    removeParent : function (component,helper,index){
        	debugger;
         var ParentGuardianWrapper =  component.get("v.ParentGuardianWrapper");
            
                if($A.util.isEmpty(ParentGuardianWrapper[index].relation.Id)){
                    ParentGuardianWrapper.splice(index,1);
                    component.set("v.ParentGuardianWrapper",ParentGuardianWrapper);
                    return ;
                }else{
            
					var RelationId = ParentGuardianWrapper[index].relation.Id;   
    				component.set("v.Spinner","true");
     				var action =  component.get("c.deleteParentGuardian");
    				action.setParams({
                        "reationId": RelationId
                    });

					action.setCallback(this, function(response) {          
                        if (response.getState() == "SUCCESS") {  
                            ParentGuardianWrapper.splice(index,1);
                   			component.set("v.ParentGuardianWrapper",ParentGuardianWrapper);
                            component.set("v.Spinner","false"); 
                        }else{
                            var toastEvent = $A.get("e.force:showToast");
                            var messageTemplate = response.getError();
                            toastEvent.setParams({ 
                                type : "Error",
                                duration : 5000,
                                message: messageTemplate
                            });
                            toastEvent.fire();
                            component.set("v.Spinner","false"); 
                        }
                    });
                    $A.enqueueAction(action);

		}
         
    }, 
    updateSelectedParent: function (component,helper,index,selectedValue){
   		var ParentGuardianWrapper =  component.get("v.ParentGuardianWrapper");
        ParentGuardianWrapper[index].parentSelected = selectedValue;
       
        component.set("v.ParentGuardianWrapper",ParentGuardianWrapper);
    },
     updateLiveWith :function (component,helper,index){
       var ParentGuardianWrapper =  component.get("v.ParentGuardianWrapper");
        
        var They_live_together = ParentGuardianWrapper[index].relation.They_live_together__c;
        if(!$A.util.isEmpty(They_live_together) && They_live_together == false ){
         	ParentGuardianWrapper[index].relation.They_live_together__c = true;
        }else if(!$A.util.isEmpty(They_live_together) && They_live_together == true){
            ParentGuardianWrapper[index].relation.They_live_together__c = false;
        }
        else{ 
            ParentGuardianWrapper[index].relation.They_live_together__c = true;
        }
        component.set("v.ParentGuardianWrapper",ParentGuardianWrapper);
    },
    updateEmergencyContact: function(component, helper,index) {
		debugger; 
        var ParentGuardianWrapper =  component.get("v.ParentGuardianWrapper");
        var This_is_your_emergency_contact = ParentGuardianWrapper[index].relation.This_is_your_emergency_contact__c;
        if(!$A.util.isEmpty(This_is_your_emergency_contact) && This_is_your_emergency_contact == false ){
         	ParentGuardianWrapper[index].relation.This_is_your_emergency_contact__c =true;
        }else if(!$A.util.isEmpty(This_is_your_emergency_contact) && This_is_your_emergency_contact == true){
            ParentGuardianWrapper[index].relation.This_is_your_emergency_contact__c =false;
        }
        else{
            ParentGuardianWrapper[index].relation.This_is_your_emergency_contact__c = true;
        }
        
        component.set("v.ParentGuardianWrapper",ParentGuardianWrapper);
    }
    
})