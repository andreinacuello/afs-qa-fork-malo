({
	getSelectedPrograms: function(component,event) {
        var appicationWrapper = component.get("v.applicationWrapper");
        var headerText = component.get("v.contactInfo.FirstName") ;
        headerText += ' you might go to';
        component.set("v.headerText",headerText);
        if(appicationWrapper != undefined &&  appicationWrapper !=null){
            if(appicationWrapper.applicationObj.Applicant__c != undefined){
        		var action = component.get("c.getSelectedProgramList");
                action.setParams({'applicant_Id' : appicationWrapper.applicationObj.Id});
                console.log(appicationWrapper.applicationObj.Applicant__c)
                action.setCallback(this, function(response) {
                    if (response.getState() == "SUCCESS") {
                         console.log(component.get("v.ProfileHedareLabel"));
                        
                        component.set("v.contentList",response.getReturnValue());
                        //component.set("v.navigateWrapper",response.getReturnValue());
                        //component.set("v.Spinner","false");
                    }else{
                       throwError(response.getError()[0].message);
                    }
                });
       			 $A.enqueueAction(action);
                
            }
        }
	},
    populateNationalityHeader:function(component,event) {
    	var headerText = component.get("v.contactInfo.FirstName") ;
        headerText  += " help us get to know you better, complete your personal details and add pictures!";
       
		var contentText = component.get("v.contactInfo.Email") ;   
         component.set("v.headerText",headerText);
         component.set("v.contentText",contentText);
    },
    populateGlobalCompentenceHeader:function(component,event) {
        var headerText = "Your Global Compentence Course";
		var contentText = "A step-by-step guide for your cultural adventure";   
         component.set("v.headerText",headerText);
         component.set("v.contentText",contentText);
    },
    populateScholaShipHeader :function(component,event) {
    	var headerText = component.get("v.contactInfo.FirstName") ;
        headerText  += " these are the scholarships available to you";
       
		 var contentText = component.get("v.contactInfo.Email") ;   
         component.set("v.headerText",headerText);
         component.set("v.contentText",contentText);
	},
    throwError : function(errorMessage){
         	var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: errorMessage
         		});
            toastEvent.fire();
    }
})