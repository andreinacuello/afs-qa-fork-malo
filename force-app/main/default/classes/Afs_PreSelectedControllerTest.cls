@isTest(SeeAllData=false)
public class Afs_PreSelectedControllerTest {
    @testSetup
    public static void setup_test(){
        /*Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createContentDocumentLinksContact(0);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(0);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();*/
    }
    
    @isTest
    static void getProgramOfferedTest() {  
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
        Afs_TestDataFactory.createContentDocumentLinksContact(0);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(0);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
             list<Program_Offer__c> prOfferList = [SELECT id FROM Program_Offer__c];
             system.assertEquals(true,Afs_PreSelectedController.getProgramOffered(prOfferList[0].id) != null);
            system.assertEquals(true,Afs_PreSelectedController.getProgramOffered('') != null);
        Test.stopTest();
    }
    
    @isTest
    static void getProgramOfferedAcceptedTest() {  
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
        Afs_TestDataFactory.createContentDocumentLinksContact(0);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(0);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
             list<Program_Offer__c> prOfferList = [SELECT id FROM Program_Offer__c];
             system.assertEquals(true,Afs_PreSelectedController.getProgramOfferedAccepted(prOfferList[0].id) != null);
        Test.stopTest();
    }
    
    @isTest
    static void isAppProcessedTest() {  
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
        Afs_TestDataFactory.createContentDocumentLinksContact(0);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(0);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
             list<Application__c> application = [SELECT id FROM Application__c];
             list<Contact> contactList = [SELECT id FROM Contact];
             system.assertEquals(false,Afs_PreSelectedController.isAppProcessed(contactList[0].id,application[0].id));
        Test.stopTest();
    }
    
    @isTest
    static void doChangeRequestTest() {  
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
        Afs_TestDataFactory.createContentDocumentLinksContact(0);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(0);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
            
            list<Application__c> application = [SELECT id,Program_Offer__c FROM Application__c];
            try{
                system.assertEquals(false,Afs_PreSelectedController.doChangeRequest(application[0]) != null);
            }catch(Exception e){
                system.assertEquals(true,e.getMessage() != null);
            }
        
            To_Do_Item__c obj = new To_Do_Item__c();
            obj.Name = 'sd'; 
            obj.Type__c = 'Filling Field'; 
            obj.Stage_of_portal__c = '2. Stage: Pre-selected'; 
            obj.Application__c  = application[0].id; 
            obj.Status__c = 'Pending'; 
            obj.Due_Date__c = Date.today().addDays(10); 
            insert obj;
            Id profile = [select id from profile where name='Applicant Community Member Profile'].id;
            Contact con = [SELECT id FROM Contact];
            User user = new User(alias = 'test123', email='test123@noemail.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                                 ContactId = con.Id,
                                 timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user;
            system.runAs(user) {
                system.assertEquals(true,Afs_PreSelectedController.doChangeRequest(application[0]) != null);
            }
        Test.stopTest();
    }
    
    @isTest
    static void payAndSaveTest() {
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete', '{}');
        Test.setMock(HttpCalloutMock.class,fakeResponse);
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
        Afs_TestDataFactory.createContentDocumentLinksContact(0);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(0);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
            try{
                Afs_PreSelectedController.payAndSave('','','');
            }catch(Exception e){
                system.assertEquals(true,e.getMessage() != null);
            }
            list<Application__c> application = [SELECT id,Program_Offer__c FROM Application__c];
            list<Program_Offer__c> programOfferList = [SELECT id FROM Program_Offer__c];
            list<Contact> contactList = [SELECT id FROM Contact];
            list<Program_Offer_in_App__c> prList = new list<Program_Offer_in_App__c>();
            for(Program_Offer_in_App__c obj : [SELECT id,Program_Offer__c, Is_the_applicant_Eligible__c,Desired__c FROM Program_Offer_in_App__c]){
                obj.Is_the_applicant_Eligible__c = System.label.AfsLbl_Application_Eligible;
                prList.add(obj);
            }
        
            update prList;
        
            Id profile = [select id from profile where name='Applicant Community Member Profile'].id;
            Contact con = [SELECT id FROM Contact];
            User user = new User(alias = 'test123', email='test123@noemail.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                                 ContactId = con.Id,
                                 timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user;
            system.runAs(user) {
                try{
                    Afs_PreSelectedController.payAndSave(contactList[0].id,application[0].id,programOfferList[0].id);
                }catch(Exception e){
                    system.assertEquals(true,e.getMessage() != null);
                }
                
                system.assertEquals(true, true);
            }
        Test.stopTest();
    }
    
    @isTest
    static void getProgramWrapperTest() {  
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
        Afs_TestDataFactory.createContentDocumentLinksContact(0);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(0);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
            try{
                Afs_PreSelectedController.getProgramWrapper('','');
            }catch(Exception e){
                system.assertEquals(true,e.getMessage() != null);
            }
            list<Application__c> application = [SELECT id,Program_Offer__c FROM Application__c];
            list<Program_Offer__c> programOfferList = [SELECT id FROM Program_Offer__c];
            list<Contact> contactList = [SELECT id FROM Contact];
            list<Program_Offer_in_App__c> prList = new list<Program_Offer_in_App__c>();
            for(Program_Offer_in_App__c obj : [SELECT id,Program_Offer__c, Is_the_applicant_Eligible__c,Desired__c FROM Program_Offer_in_App__c]){
                obj.Is_the_applicant_Eligible__c = System.label.AfsLbl_Application_Eligible;
                prList.add(obj);
            }
        
            update prList;
        
            Id profile = [select id from profile where name='Applicant Community Member Profile'].id;
            Contact con = [SELECT id FROM Contact];
            User user = new User(alias = 'test123', email='test123@noemail.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                                 ContactId = con.Id,
                                 timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user;
            system.runAs(user) {
                Afs_PreSelectedController.getProgramWrapper(contactList[0].id,application[0].id);
                system.assertEquals(true, true);
            }
        Test.stopTest();
    }
    
}