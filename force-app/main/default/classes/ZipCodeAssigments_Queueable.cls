/*Queueable To Process ZipCodeAssigments and Update Account Chapters*/
public class ZipCodeAssigments_Queueable implements Queueable,Database.AllowsCallouts{   
    
    public String iocCode;
    public Integer index;
    List<String> lstIOCCodes;
    
    public ZipCodeAssigments_Queueable(List<String> lstIOCs, Integer i){
        this.iocCode = lstIOCs[i];
        this.lstIOCCodes = lstIOCs;
        this.index = i;
    }
    
    public void execute(QueueableContext context) {
        try{
            Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AFS_Chapters').getRecordTypeId();
            Map<String,Account> mapAccount = new Map<String,Account>();
            Map<Id,Account> mapAccountToUpdate = new Map<Id,Account>();
            //Get Account Chapters to related IOC
            for(Account acc : [SELECT Id, Global_Link_External_Id__c FROM Account WHERE recordTypeId = :recordTypeId AND IOC_Code__c = :iocCode]){
                mapAccount.put(acc.Global_Link_External_Id__c,acc);
            }
            
            
            //Get ZipCodeAssigments to related IOC
            RemoteSiteManager rsm = new RemoteSiteManager();
            rsm.createZipcodeAssignments(iocCode);
            
            //Process
            for(RemoteSiteModel.ZipcodeAssignments zpa : rsm.remoteSiteModel.zipcodeAssignments){
                try{
                    if(mapAccount.containsKey(zpa.assigned_org_id)){                        
                        Account acc = mapAccount.get(zpa.assigned_org_id);
                        acc.Zip_From__c = Decimal.ValueOf(zpa.zip_from);
                        acc.Zip_To__c = Decimal.valueOf(zpa.zip_To);
                        mapAccountToUpdate.put(acc.Id,acc);
                        
                    }
                }catch(Exception ex){
                    system.debug(Logginglevel.error,ex.getMessage());
                }
            }
            
            //Update Account Chapters
            if(!mapAccountToUpdate.isEmpty()){
            	List<Database.SaveResult> lstSR = Database.Update(mapAccountToUpdate.values(),false);
                for(Integer i = 0;i < lstSR.Size(); i++){                
                    Database.SaveResult sr = lstSR[i];
                    if(!sr.isSuccess()){
                        system.debug(logginglevel.error,mapAccountToUpdate.values()[i] + ' --> ' + String.join(sr.getErrors(),' / '));
                    }
                }
            }
            
            index++;
            //Execute next queueable with next IOC
            if(!Test.isRunningTest() && index < lstIOCCodes.size()){
               System.enqueueJob(new ZipCodeAssigments_Queueable(lstIOCCodes,index)); 
            }
            

        }catch(Exception exc){
            system.debug(logginglevel.error,exc.getMessage());
        }
    }
}