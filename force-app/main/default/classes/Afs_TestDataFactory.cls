@isTest
public with sharing class Afs_TestDataFactory {
    public static list<Account> accountList;
    public static list<Contact> contactList;
    public static list<Application__c> applicationList;
    public static list<Program_Offer__c> programOfferList;
    public static list<Program_Offer_in_App__c> programOfferINAppList;
    public static list<Scholarship__c> schList;
    public static void createAccountRecords(Integer numAccts){
        accountList = new List<Account>();   
        id Rid = [Select Id From RecordType Where DeveloperName = 'AFS_Partner'].id;
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestAccount' + i,RecordTypeId = Rid);
            accountList.add(a);
        }
        insert accountList;
    }
    
    public static void createPartnerAccountRecords(Integer numAccts){
        accountList = new List<Account>();    
        Id idAfsPartnerAcc = [SELECT id,DeveloperName FROM RecordType WHERE SObjectType = 'Account' AND DeveloperName ='AFS_Partner' LIMIT 1].id;    
        for(Integer i=0;i<numAccts;i++) {
            Account a = new Account(Name='TestAccount' + i,RecordTypeId = idAfsPartnerAcc, IOC_Code__c = 'Test IOC Code ',Country__c = 'Argentina');
            accountList.add(a);
        }
        insert accountList;
    }
    
    public static void createContactRecords(Integer numAccts){
        contactList = new List<Contact>();
        id rId = [SELECT id FROM RecordType WHERE SObjectType = 'Contact' AND Name = 'Applicant'].id;
        for (Integer j=0;j<numAccts;j++) {
            Account acct = accountList[j];   
             contactList.add(new Contact(firstname='Test'+j,
                                     lastname='Test'+j,
                                     AccountId=acct.Id,email='user@appohm.com',RecordTypeId = rId));
        }
        // Insert all contacts for all accounts
        insert contactList;
    }
    
    public static void createUserWithContact(){
        
         Id p = [SELECT id FROM Profile WHERE Name = 'Applicant Community Member Profile'].id;
         User user = new User(alias = 'test123', email='user@appohm.com',
                emailencodingkey='UTF-8', lastname = contactList[0].lastName, firstName = contactList[0].firstName, languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = contactList[0].Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert user;
    }
    
    public static void createTriggerSwitch(){
        // insert custom setting
        Afs_Triggers_Switch__c afsTriggerSwitch = new Afs_Triggers_Switch__c();
        afsTriggerSwitch.Name = 'Afs_Trigger';
        afsTriggerSwitch.By_Pass_Application_Trigger__c = true;
        afsTriggerSwitch.By_Pass_Opportunity_Trigger__c = true;
        afsTriggerSwitch.ByPass_ScholarshipApplication_Trigger__c = true;
        insert afsTriggerSwitch;
    }
    
    public static void createConfigurationObj(){
        // Insert configuration object
		AFS_Community_Configuration__c configuraionObject = new AFS_Community_Configuration__c();
        configuraionObject.Afs_SendingPartner__c = accountList[0].id;
        configuraionObject.Name = 'Afs Applicant Community Test';
        configuraionObject.Community_URL__c = 'https://dev02-afstest.cs66.force.com/applicant';
        configuraionObject.Unique_Id__c = UserInfo.getUserId().subString(0,15);
        configuraionObject.Community_App_Queue__c = UserInfo.getUserId();
        configuraionObject.CRMUserOwner__c = UserInfo.getUserId();
        configuraionObject.Afs_ToDoStatusComplete__c = 'Completed;Pending';
        configuraionObject.Community_Guest_User_Owner__c = UserInfo.getUserId();
        insert configuraionObject;
    }
    
    public static void createApplicationRecords(Integer numAccts){
        
		
        applicationList = new List<Application__c>();
        for (Integer j=0;j<numAccts;j++) {
            Contact con = contactList[j];   
             applicationList.add(new Application__c(Applicant__c = con.id, Program_Offer__c = programOfferList[j].id));
        }
        // Insert all contacts for all accounts
        insert applicationList;
    }
    
    public static void createProgramOfferRecords(Integer numAccts){
        // INSERT ONE HOSTING PROGRAM
        Hosting_Program__c  hostProgram = new Hosting_Program__c();
        hostProgram.Name = 'Host New Program';
        hostProgram.Duration__c = 'YP';
        hostProgram.Hemisphere__c = 'NH';
        hostProgram.Program_Content__c = 'bp';
        hostProgram.Year__c = String.valueOf(Date.today().year());
        hostProgram.Host_Partner__c = accountList[0].id;
        hostProgram.App_Received_From__c = Date.today().addDays(-40);
        //hostProgram.Host_IOC__c = 'GUA';
        insert hostProgram;
        
        programOfferList = new List<Program_Offer__c>();
        for (Integer j=0;j<numAccts;j++) {
             programOfferList.add(new Program_Offer__c(Name = 'Test1-' + j,Hosting_Program__c = hostProgram.id,Area_of_interest__c='Academic',Durations__c='YP',To__c = Date.today().addDays(30),From__c = Date.today().addDays(-30),Program_Type__c = 'High school',Total_Cost_numeric__c = 2000,Sending_Partner__c = accountList[0].id,Show_in_portal__c = true,Applications_Received_To_local__c = Date.today().addDays(40),Applications_Received_From_local__c = Date.today().addDays(-10)));
             programOfferList.add(new Program_Offer__c(Name = 'Test2-' + j,Hosting_Program__c = hostProgram.id,Area_of_interest__c='Education & Youth',Durations__c='SM',To__c = Date.today().addDays(30),From__c = Date.today().addDays(-30),Program_Type__c = 'Volunteering', Total_Cost_numeric__c = 3000,Sending_Partner__c = accountList[0].id,Show_in_portal__c = true,Applications_Received_To_local__c = Date.today().addDays(40),Applications_Received_From_local__c = Date.today().addDays(-10)));
             programOfferList.add(new Program_Offer__c(Name = 'Test3-' + j,Hosting_Program__c = hostProgram.id,Area_of_interest__c='Host Family',Durations__c='YP',To__c = Date.today().addDays(30),From__c = Date.today().addDays(-30),Program_Type__c = 'University', Total_Cost_numeric__c = 4000,Sending_Partner__c = accountList[0].id,Show_in_portal__c = true,Applications_Received_To_local__c = Date.today().addDays(40),Applications_Received_From_local__c = Date.today().addDays(-10)));      
        }
        // Insert all contacts for all accounts
        insert programOfferList;
    }
    
    public static void createContentDocumentLinks(Integer numAccts){
        
        Blob beforeblob=Blob.valueOf('Unit Test Attachment Body');

        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;          
        insert cv;         

        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        list<ContentDocumentLink> contentLinkList = new list<ContentDocumentLink>();
        for(Program_Offer__c prg : programOfferList){
            ContentDocumentLink contentlink=new ContentDocumentLink();
            contentlink.LinkedEntityId = prg.Id; 
            contentlink.ContentDocumentId= testcontent.ContentDocumentId;
            contentlink.ShareType= 'V';
            contentLinkList.add(contentlink);
        }
        
        insert contentLinkList;
    }
    
    public static void createContentDocumentLinksContact(Integer numAccts){
        
        Blob beforeblob=Blob.valueOf('Unit Test Attachment Body');

        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;  
        insert cv;         

        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        list<ContentDocumentLink> contentLinkList = new list<ContentDocumentLink>();
        for(Contact prg : contactList){
            ContentDocumentLink contentlink=new ContentDocumentLink();
            contentlink.LinkedEntityId = prg.Id; 
            contentlink.ContentDocumentId=testcontent.ContentDocumentId;
            contentlink.ShareType= 'V';
            contentlink.Visibility = 'AllUsers'; 
            contentLinkList.add(contentlink);
        }
        insert contentLinkList;
    }
    
    
    public static void createContentDocumentLinksProgramOffer(Integer numAccts){
        
        Blob beforeblob=Blob.valueOf('Unit Test Attachment Body');

        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;  
        insert cv;         

        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        list<ContentDocumentLink> contentLinkList = new list<ContentDocumentLink>();
        for(Program_Offer__c prg : programOfferList){
            ContentDocumentLink contentlink=new ContentDocumentLink();
            contentlink.LinkedEntityId = prg.Id; 
            contentlink.ContentDocumentId=testcontent.ContentDocumentId;
            contentlink.ShareType= 'V';
            contentlink.Visibility = 'AllUsers'; 
            contentLinkList.add(contentlink);
        }
        insert contentLinkList;
    }
    
	  
    public static void createProfilePhoto(Integer numAccts){
            
        Blob beforeblob=Blob.valueOf('Unit Test Attachment Body');
        
        ContentVersion cv = new ContentVersion();
        cv.title = 'test content trigger';      
        cv.PathOnClient ='test';           
        cv.VersionData =beforeblob;
        cv.Afs_ProfilePhoto__c = true;
        insert cv;         
        
        ContentVersion testContent = [SELECT id, ContentDocumentId FROM ContentVersion where Id = :cv.Id];
        list<ContentDocumentLink> contentLinkList = new list<ContentDocumentLink>();
        for(Contact prg : contactList){
            ContentDocumentLink contentlink=new ContentDocumentLink();
            contentlink.LinkedEntityId = prg.Id; 
            contentlink.ContentDocumentId=testcontent.ContentDocumentId;
            contentlink.ShareType= 'V';
            contentlink.Visibility = 'AllUsers'; 
            contentLinkList.add(contentlink);
        }
        insert contentLinkList;
    }
    
    public static void createProgramOfferedAndApplicationLink(){
        Application__c app = applicationList[0];
        Contact con = contactList[0];
        programOfferINAppList = new list<Program_Offer_in_App__c>();
        for(Program_Offer__c progrmOffr : programOfferList){
            programOfferINAppList.add(new Program_Offer_in_App__c(Application__c = app.id,Program_Offer__c = progrmOffr.id,Contact__c = con.id));
        }                     
        insert programOfferINAppList;
    }
    
    public static void create_Scholarship(){
        schList = new list<Scholarship__c> ();
        Scholarship__c sch = new Scholarship__c();
        sch.name = 'Test Scholarship';
        sch.Sending_Partner__c = accountList[0].id;
        sch.Applicant_Eligibility_Specifications__c  = 'Academic Merit Based Candidates; Diversity Merit Based Candidates; Children Grandchildren of Employees; Grandchildren of Employees; Close Relatives of Employees; Children of Members; Grandchildren of Members; Close Relatives of Members; Children of Customers; Grandchildren of Customers';
        sch.Scholarship_Application_Deadline__c  = System.now().addDays(5);
        sch.Scholarship_Amount_Available__c = 'Partial 10%';
        sch.Min_GPA__c  = '12';
        schList.add(sch);
        Insert schList;
    }
    
    public static void create_ScholarshipApplication(){
     	Scholarship_Application__c schApp = new Scholarship_Application__c();
        schApp.Scholarship__c = schList[0].Id;
        schApp.Applicant__c = contactList[0].Id;
        schApp.application__C = applicationList[0].id;
        Insert schApp;    
    }
    
    public static void create_ScholarshipProgram(){
    	Sch_in_Program__c schProgram = new Sch_in_Program__c();
        schProgram.Scholarship__c= schList[0].Id;
        schProgram.Program_Offer__c= programOfferList[0].Id;
        Insert schProgram;
    }
    
    public static void create_PaymentInstallments(){
        Payment_Installment__c paymentInstallments = new Payment_Installment__c();
        paymentInstallments.Amount__c= 100;
        paymentInstallments.Application__c= applicationList[0].Id;
        paymentInstallments.Number__c = 0;
        Insert paymentInstallments;
    }
   
}