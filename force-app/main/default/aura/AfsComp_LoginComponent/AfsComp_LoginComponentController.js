({
	doInit : function(component, event, helper) {
        helper.onInit(component,event);
		helper.getDetails(component,event);
	},
    
    onLogin : function(component, event, helper) {
        component.set("v.Spinner",true);
        var loginWrapper = component.get("v.loginWrapper");
        var messageTemplateStr;
        
        // Check if email is valid 
        if(loginWrapper.Email === undefined || loginWrapper.Email == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_EmailRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_EmailRequire");
            }
        }else{
            var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if(!re.test(loginWrapper.Email)){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_EmailNoValid");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_EmailNoValid");
                }
            }
        }
        // Check if password is not too short
        if(loginWrapper.Password === undefined || loginWrapper.Password == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_PasswordRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_PasswordRequire");
            }
        }else{
            
            if(loginWrapper.Password.length < 8){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_PasswordWeek");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_PasswordWeek");
                }
            }
        }
        
        if(messageTemplateStr != undefined){
            var toastEvent = $A.get("e.force:showToast");
            var messageTemplate = messageTemplateStr;
            toastEvent.setParams({ 
                type : "Error",
                message: messageTemplate
            });
            toastEvent.fire();
            component.set("v.Spinner",false);
        }else{
            helper.doLogin(component,event);
        }
	},
    
    toggleMenu : function(component, event, helper) {
        var flag = component.get("v.toggleMenuIcon");
        if(flag){
            component.set("v.toggleMenuIcon",false);
            component.set("v.toggleMenuClass",true); 
        }else{
            component.set("v.toggleMenuIcon",true);
            component.set("v.toggleMenuClass",false);
        }
	},
    
    onForgetPassWord : function(component, event, helper) {
        var forgetPasswordUrl = component.get("v.forgetPasswordUrl");
        window.location.href = forgetPasswordUrl;
	}
    
})