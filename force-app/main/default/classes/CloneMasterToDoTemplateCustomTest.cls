@isTest
public class CloneMasterToDoTemplateCustomTest {
	
    @isTest static void createClone(){
        Account accountTest = Util_Test.create_AFS_Partner(); // Ya se inserto
        Hosting_Program__c hostingProgramTest = Util_Test.create_Hosting_Program(accountTest); // Ya se inserto
        Scholarship__c newScholarship = Util_Test.create_Scholarship(accountTest); // Ya se inserto
        Program_Offer__c programOfferTest = Util_Test.create_Program_Offer(accountTest,hostingProgramTest); // Ya esta inserto
        List<Master_To_Do_Template__c> masterTemplate = Util_Test.createListMasterToDoTemplate(1);
        insert masterTemplate;
        Master_To_Do__c newToDoItem = Util_Test.create_Master_To_Do(masterTemplate[0],newScholarship,programOfferTest,'Standard_for_Program_Offer');
        insert newToDoItem;
        
        Test.startTest();
        Master_To_Do_Template__c newMastercreated = CloneMasterToDoTemplateCustom.newMasterToDoTemplate('Clone record',masterTemplate[0].Id);
        Test.stopTest();
        
        system.debug('newMastercreated = '+newMastercreated);
        system.assertEquals('Clone record', newMastercreated.Name);
    }
    
}