@isTest
public class WaitedList_Schedulable_Test {
	public static testMethod void SchedulerMethod_Test() {
    	Test.startTest();
        	WaitedList_Schedulable.SchedulerMethod();
        Test.stopTest();
        CronJobDetail cjd = [SELECT Id, Name, JobType FROM CronJobDetail WHERE Name = 'WaitListed_Batch'];
		system.AssertEquals('WaitListed_Batch',cjd.Name);        
       
    }
    
   	public static testMethod void execute_Test() {
        Test.startTest();
        	System.schedule('WaitListed_Batch_Test','0 0 0 ? * * *', new WaitedList_Schedulable());
        Test.stopTest();
               CronJobDetail cjd = [SELECT Id, Name, JobType FROM CronJobDetail WHERE Name = 'WaitListed_Batch_Test'];
		system.AssertEquals('WaitListed_Batch_Test',cjd.Name);
    }
}