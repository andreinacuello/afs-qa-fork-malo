@isTest(SeeAllData=false)
public class Afs_DisplayProgramSearchControlerTest {
    public static User user;
    
    @isTest
    static void getDestinationsTest() { 
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        
        Test.startTest();
            list<Afs_DisplayProgramSearchResultController.DisplayPicklistValueWrapper> wrapperList = new list<Afs_DisplayProgramSearchResultController.DisplayPicklistValueWrapper>();
            sObject program = new Program_Offer__c();    
            wrapperList = Afs_DisplayProgramSearchResultController.getDestinations (program , 'Destination__c'); 
            system.assertEquals(true,true);
        Test.stopTest();
    }
    
    @isTest
    static void getCurrentUserNameTest() { 
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
            Id profile = [select id from profile where name='Applicant Community Profile Login'].id;
            Contact con = [SELECT id FROM Contact];
            user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            system.runAs(user) {
                system.assertEquals(true,Afs_DisplayProgramSearchResultController.getCurrentUserName().size() > 0);
            }  
        Test.stopTest();
    }
    
    @isTest
    static void getProgramWrapperEXcpTest() { 
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        
        Test.startTest();
        Afs_TestDataFactory.createContentDocumentLinks(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
      
            list<Program_Offer_in_App__c> prList = new list<Program_Offer_in_App__c>();
            for(Program_Offer_in_App__c obj : [SELECT id,Program_Offer__c, Is_the_applicant_Eligible__c,Desired__c FROM Program_Offer_in_App__c]){
                obj.Is_the_applicant_Eligible__c = System.label.AfsLbl_Application_Eligible;
                prList.add(obj);
            }
            
            update prList;
            Id profile = [select id from profile where name='Applicant Community Profile Login'].id;
            Contact con = [SELECT id FROM Contact];
            user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            system.runAs(user) {
                list<Program_Offer__c> prg = [SELECT id,Name FROM Program_Offer__c];
                map<String,List<Object>> mapFilters = new map<String,List<Object>>();
                Object obj = 'Albania';
                List<Object> objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Destinations__c',objList);
                obj = 'YP';
                objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Durations__c',objList);
                obj = 'High school';
                objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Program_Type__c',objList);
                obj = 'Host Family';
                objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Area_Of_Interest__c',objList);
                   // system.assertEquals(false,Afs_DisplayProgramSearchResultController.getProgramWrapper(mapFilters,prg[0].Name,true).size() > 0);
                	//system.assertEquals(false,Afs_DisplayProgramSearchResultController.getProgramWrapper(null,'',true).size() > 0);
                
            }  
        Test.stopTest();
    }
    
     @isTest
    static void getProgramWrapperTest() { 
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createContentDocumentLinks(1);
        Afs_TestDataFactory.createContentDocumentLinksProgramOffer(1);
        
        Test.startTest();
        list<Program_Offer__c> prList1 = new list<Program_Offer__c>();
        for(Program_Offer__c oibj1 :  [SELECT id,Applications_Received_To_local__c FROM Program_Offer__c ]){
            oibj1.Applications_Received_To_local__c = Date.today().addDays(10);
            prList1.add(oibj1);
        }
        update prList1;
            list<Program_Offer_in_App__c> prList = new list<Program_Offer_in_App__c>();
            for(Program_Offer_in_App__c obj : [SELECT id,Program_Offer__c, Is_the_applicant_Eligible__c,Desired__c FROM Program_Offer_in_App__c]){
                obj.Is_the_applicant_Eligible__c = System.label.AfsLbl_Application_Eligible;
                prList.add(obj);
            }
            
            update prList;
            Id profile = [select id from profile where name='Applicant Community Profile Login'].id;
            Contact con = [SELECT id FROM Contact];
            user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            system.runAs(user) {
                list<Program_Offer__c> prg = [SELECT id,Name FROM Program_Offer__c];
                map<String,List<Object>> mapFilters = new map<String,List<Object>>();
                Object obj = 'Albania';
                List<Object> objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Destinations__c',objList);
                obj = 'YP';
                objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Durations__c',objList);
                obj = 'High school';
                objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Program_Type__c',objList);
                obj = 'Host Family';
                objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Area_Of_Interest__c',objList);
                Afs_DisplayProgramSearchResultController.getProgramWrapper(mapFilters,prg[0].Name,true);
                    system.assertEquals(true,true);
                Afs_DisplayProgramSearchResultController.getProgramWrapper(null,'',true);
                	system.assertEquals(true,true);
                
            }  
        Test.stopTest();
    }
    
    
    @isTest
    static void saveAndContinueProgramTest() { 
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.createContentDocumentLinks(1);
        Test.startTest();
            Id profile = [select id from profile where name='Applicant Community Profile Login'].id;
            Contact con = [SELECT id FROM Contact];
            user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            system.runAs(user) {
                list<Program_Offer__c> prg = [SELECT id FROM Program_Offer__c];
                list<String> progrmOfr = new list<String> ();
                progrmOfr.add(prg[0].id);
                progrmOfr.add(prg[1].id);
                system.assertEquals(true,Afs_DisplayProgramSearchResultController.saveAndContinueProgram(new list<String>(),progrmOfr,'Argentina;Argentina;Argentina') != null);
                map<String,List<Object>> mapFilters = new map<String,List<Object>>();
                Object obj = 'Albania';
                List<Object> objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Destinations__c',objList);
                obj = 'YP';
                objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Durations__c',objList);
                obj = 'High school';
                objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Program_Type__c',objList);
                obj = 'Host Family';
                objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Area_Of_Interest__c',objList);
                Afs_DisplayProgramSearchResultController.getProgramWrapper(mapFilters,'',true);
                system.assertEquals(true,true);
                list<String> interestList  = new list<String>();
                for(Program_Offer_in_App__c interest : [SELECT id,Program_Offer__c FROM Program_Offer_in_App__c]){
                    interestList.add(interest.Program_Offer__c);
                }
                Afs_DisplayProgramSearchResultController.saveAndContinueProgram(interestList,progrmOfr,'Argentina;Argentina;Argentina');
                system.assertEquals(true,true);
            }  
        Test.stopTest();
    }
    
    @isTest
    static void saveAndContinueProgramNoAppTest() { 
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createContentDocumentLinks(1);
        
        Afs_TestDataFactory.createApplicationRecords(1);
        Test.startTest();
            Id profile = [select id from profile where name='Applicant Community Profile Login'].id;
            Contact con = [SELECT id FROM Contact];
            user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = profile, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            system.runAs(user) {
                list<Program_Offer__c> prg = [SELECT id FROM Program_Offer__c];
                list<String> progrmOfr = new list<String> ();
                progrmOfr.add(prg[0].id);
                progrmOfr.add(prg[1].id);
                system.assertEquals(true,Afs_DisplayProgramSearchResultController.saveAndContinueProgram(new list<String>(),progrmOfr,'Argentina;Argentina;Argentina') != null);
                map<String,List<Object>> mapFilters = new map<String,List<Object>>();
                Object obj = 'Albania';
                List<Object> objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Destinations__c',objList);
                obj = 'YP';
                objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Durations__c',objList);
                obj = 'High school';
                objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Program_Type__c',objList);
                obj = 'Host Family';
                objList = new list<Object>();
                objList.add(obj);
                mapFilters.put('Area_Of_Interest__c',objList);
                Afs_DisplayProgramSearchResultController.getProgramWrapper(mapFilters,'',true);
                system.assertEquals(true,true);
                list<String> interestList  = new list<String>();
                for(Program_Offer_in_App__c interest : [SELECT id,Program_Offer__c FROM Program_Offer_in_App__c]){
                    interestList.add(interest.Program_Offer__c);
                }
                Afs_DisplayProgramSearchResultController.saveAndContinueProgram(interestList,progrmOfr,'Argentina;Argentina;Argentina');
                system.assertEquals(true, true);
                Afs_DisplayProgramSearchResultController.DisplayProgramWrapper dpW = new Afs_DisplayProgramSearchResultController.DisplayProgramWrapper(new Program_Offer__c (), '', true, 1, true,false);
                Afs_DisplayProgramSearchResultController.DisplayPicklistValueWrapper dpvW = new Afs_DisplayProgramSearchResultController.DisplayPicklistValueWrapper('', '', false, 2,'');

            }  
        Test.stopTest();
    }
    
}