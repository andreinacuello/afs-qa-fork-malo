declare module "@salesforce/apex/Afs_IntroductionCtrl.saveApplicantIntroduction" {
  export default function saveApplicantIntroduction(param: {application: any, recId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_IntroductionCtrl.deleteAttachment" {
  export default function deleteAttachment(param: {AttchmentId: any, Id: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_IntroductionCtrl.getAttachment" {
  export default function getAttachment(param: {Id: any}): Promise<any>;
}
