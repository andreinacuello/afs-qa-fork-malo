({
	doInit : function(component, event, helper) {
       
        var configObj = component.get("v.configWrapper"); 
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_ParentGuardian__c,component);
        component.set("v.ParentGuardian",component.get("v.LabelTempVal"));      
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_ParentGuardianHelpTxt__c,component);
        component.set("v.ParentGuardianHelpTxt",component.get("v.LabelTempVal")); 
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_ParentName__c ,component);
        component.set("v.ParentName",component.get("v.LabelTempVal")); 
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_Email__c,component);
        component.set("v.Email",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PrimaryPhone__c,component);
        component.set("v.PrimaryPhone",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PrimaryPhoneHelpTxt__c,component);
        component.set("v.PrimaryPhoneHelpTxt",component.get("v.LabelTempVal")); 
        
	    helper.getParentGuardianList(component,helper);
	},
    setParentValue : function(component, event, helper) {
    	var index =  event.currentTarget.getAttribute("data-value");
       	var selectedValue = event.currentTarget.id ;
        helper.updateSelectedParent(component,helper,index,selectedValue);
    },
    addParentBlankRec : function(component, event, helper) {
    	helper.addParentBlankRec(component,helper);
    },
    removeParent  : function(component, event, helper) {
       //var result = confirm("Are you  sure, You want to Remove parent/guardian Information. Change is irreversible ?");
       // if (result == true) {
        var index =  event.currentTarget.getAttribute("data-value");
        helper.removeParent(component,helper,index);
            //Logic to delete the item
        //}
       
    },
    updateEmergencyContact : function(component, event, helper) {
    	var index =  event.currentTarget.getAttribute("data-value");
        helper.updateEmergencyContact(component,helper,index);
    },
    updateLiveWith : function(component, event, helper) {
    	var index =  event.currentTarget.getAttribute("data-value");
        helper.updateLiveWith(component,helper,index);
    }
})