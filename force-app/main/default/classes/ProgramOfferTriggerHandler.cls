/**
* Created by jotategui on 08/05/2018.
*/

public with sharing class ProgramOfferTriggerHandler {
    
    public static void afterInsert(Map<Id, Program_Offer__c> mapNew,Map<Id, Program_Offer__c> mapOld){
        createMasterToDos(mapNew.values(),mapOld,false);
    }
    
    public static void afterUpdate(Map<Id, Program_Offer__c> mapNew,Map<Id, Program_Offer__c> mapOld){
        createMasterToDos(mapNew.values(),mapOld,true);
    }
    
    public static void createMasterToDos(List<Program_Offer__c> lstNew, Map<Id, Program_Offer__c> mapOld, boolean isUpdate){
        
        Map<Id, Program_Offer__c> programs = new Map<Id, Program_Offer__c>();
        
        for(Program_Offer__c PO : lstNew){
            if((!isUpdate && PO.To_Do_Template__c != null ) || (isUpdate && PO.To_Do_Template__c != mapOld.get(PO.Id).To_Do_Template__c)){
                programs.put(PO.Id,PO);
            }
        }
        
        if(programs.size() > 0){
            if(isUpdate)
                TodoListHelper.deleteOldMasterToDos(programs.keySet());
            
            set<id> templatesId = new Set<Id>();
            
            for(Program_Offer__c PO : programs.values()){
                if(PO.To_Do_Template__c != null){
                    templatesId.add(PO.To_Do_Template__c);
                }
            }    
            
            Map<Id,List<Master_To_Do__c>> MasterTodoGrouped = new Map<Id,List<Master_To_Do__c>>();
            if(templatesId.size() > 0){
                for(Master_To_Do__c masterToDo : [SELECT Name, Days__c,  Dependant_Task__c, Program_Offer__c, Help__c, Comments__c,
                                                  Scholarship__c, Template__r.Id, When__c,Description__c, Type__c, Link__c, Stage_of_portal__c, RecordType.Name, To_Do_Label__c from Master_To_Do__c WHERE Template__r.Id IN :templatesId]){
                                                      Id TemplateId = masterToDo.Template__r.Id;
                                                      if(!MasterTodoGrouped.containsKey(TemplateId))
                                                          MasterTodoGrouped.put(TemplateId, new List<Master_To_Do__c>());
                                                      MasterTodoGrouped.get(TemplateId).add(masterToDo);
                                                  }
            }
            
            if(MasterTodoGrouped.size() > 0){
                List<Master_To_Do__c> MasterToDoInsert = new List<Master_To_Do__c>();
                for(Program_Offer__c PO : programs.values()){
                    if(PO.To_Do_Template__c != null){
                        List<Master_To_Do__c> programMaster_to_dos = TodoListHelper.cloneMasterToDo(MasterTodoGrouped.get(PO.To_Do_Template__c),PO.id,'Program');
                        for(Master_To_Do__c todo : programMaster_to_dos){
                            MasterToDoInsert.add(todo);
                        }
                    }    
                }
                if(masterToDoInsert.size() > 0){
                    insert masterToDoInsert;
                }
            }            
            
        }
    }
}