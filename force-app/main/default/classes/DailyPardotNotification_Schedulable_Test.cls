@isTest
private class DailyPardotNotification_Schedulable_Test {
    private static testMethod void method1(){ 
        string d = '0 0 0 3 9 ? 2078';
        String jobId = System.schedule('testBasicScheduledApex', d, new DailyPardotNotification_Schedulable());
        
        // Get the information from the CronTrigger API object
      	CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

      	// Verify the job has not run
     	System.assertEquals(0, ct.TimesTriggered);

      	// Verify the next time the job will run
      	System.assertEquals('2078-09-03 00:00:00', String.valueOf(ct.NextFireTime));
    }
}