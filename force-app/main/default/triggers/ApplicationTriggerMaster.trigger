/**
 * Created by jotategui on 09/05/2018.
 */

trigger ApplicationTriggerMaster on Application__c (before insert, before update, after insert, after update, after delete) {

    if(Trigger.isAfter){
        if(Trigger.isInsert){
			ApplicationTriggerHandler.afterInsert(Trigger.newMap);
            /*Map<id,Application__c> AppsUpdates = new Map<id,Application__c>();
            for(Application__c PO : Trigger.new){
                if(PO.Program_Offer__c != null){
                    AppsUpdates.put(PO.id, PO);
                }
            }
            if(AppsUpdates.Size() > 0){
            	ApplicationTriggerHandler.createToDos(AppsUpdates, Trigger.isUpdate);
            }
            ApplicationTriggersHandler.createInstallments();*/

        }else if(Trigger.isUpdate){
            ApplicationTriggerHandler.afterUpdate(Trigger.newMap,Trigger.oldMap);
            /*Map<id,Application__c> AppsUpdates = new Map<id,Application__c>();
            for(Application__c PO : Trigger.new){
                if(trigger.newMap.get(PO.id).Program_Offer__c != trigger.oldMap.get(PO.id).Program_Offer__c){
                    AppsUpdates.put(PO.id, PO);
                }
            }
            if(AppsUpdates.Size() > 0){
            	ApplicationTriggerHandler.createToDos(AppsUpdates, Trigger.isUpdate);
            }
            ApplicationTriggersHandler.createInstallments();*/
        }

    }else if(Trigger.isBefore){
		if(Trigger.isInsert){
			ApplicationTriggerHandler.beforeInsert(Trigger.new);
		}else if(trigger.isUpdate){
			ApplicationTriggerHandler.beforeUpdate(Trigger.newMap,Trigger.oldMap);
		}
    }
    /*if(Trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
        System.debug(trigger.oldMap);
        ApplicationTriggerHandler.CopyProgramStartDate(Trigger.new);
    }*/
    
}