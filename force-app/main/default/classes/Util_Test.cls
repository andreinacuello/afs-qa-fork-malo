@isTest
public class Util_Test {
    
    public static String STRING_500 = 'In Trailhead, learning topics are organized into modules, which are broken up into units. To finish a unit, you earn points by completing a quiz or a challenge. A quiz checks your knowledge with multiple-choice questions, while a challenge tests your skills by getting your hands dirty in a Salesforce org. Once you’ve finished all of the units in a module, you get a shiny new badge for your profile. Trails are a group of modules that provide guided learning paths suited to specific roles or needs';
    public static String STRING_160 = 'In Trailhead, learning topics are organized into modules, which are broken up into units. To finish a unit, you earn points by completing a quiz or a challenge.';
        
    public static Account create_AFS_Partner(){
        Account acc = new Account();
        acc.name = 'Test Account';
        acc.IOC_Code__c = 'Test IOC Code';
        acc.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AFS_Partner').getRecordTypeId();
        Insert acc;
        return acc;
    }
    
    public static Account create_AFS_Partner2(String accountName){
        Account acc = new Account();
        acc.name = accountName;
        acc.IOC_Code__c = 'Test IOC Code';
        acc.RecordTypeid = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AFS_Partner').getRecordTypeId();
        Insert acc;
        return acc;
    }
    
    public static Scholarship__c create_Scholarship(Account acc){
        Scholarship__c sch = new Scholarship__c();
        sch.name = 'Test Scholarship';
        sch.Indicate_the_AFS_Partner__c = acc.Id;
        sch.Sending_Partner__c = acc.Id;
        Insert sch;
        return sch;
    }
    
    public static Scholarship__c create_Scholarship2(Account acc, Decimal numeroDecimal,String nameScholarship){
        Scholarship__c sch = new Scholarship__c();
        sch.name = nameScholarship;
        sch.Pre_Application_Fee_Required__c = true;
        sch.Rre_Application_Fee_Amount__c = numeroDecimal;
        sch.Indicate_the_AFS_Partner__c = acc.Id;
        sch.Sending_Partner__c = acc.Id;
        return sch;
    }
    
    public static Scholarship_Application__c create_ScholarshipApplication(Scholarship__c sch,contact con , application__C application){
     	Scholarship_Application__c schApp = new Scholarship_Application__c();
        schApp.Scholarship__c = sch.Id;
        schApp.Applicant__c = con.Id;
        schApp.application__C = application.id;
        Insert schApp;
        return schApp;
    
    }
    public static Scholarship_Application__c create_ScholarshipApplication2(Scholarship__c sch,contact con , application__C application,String stage){
     	Scholarship_Application__c schApp = new Scholarship_Application__c();
        schApp.Scholarship__c = sch.Id;
        schApp.Applicant__c = con.Id;
        schApp.Application_Date__c = date.today().addDays(2);
        schApp.Stage__c = stage;
        schApp.application__C = application.id;
        return schApp;
    
    }
    public static Sch_in_Program__c create_ScholarshipProgram(Scholarship__c sch,Program_Offer__c  po){
    	Sch_in_Program__c schProgram = new Sch_in_Program__c();
        schProgram.Scholarship__c= sch.Id;
        schProgram.Program_Offer__c= po.Id;
        Insert schProgram;
        return schProgram;
    }
    
    
    public static Hosting_Program__c create_Hosting_Program(Account acc){
        Hosting_Program__c hp = new Hosting_Program__c();
        hp.name = 'Test Hosting Program';
        hp.Host_Partner__c = acc.Id;
        hp.Duration__c = 'YP';
        hp.Program_Content__c = 'as';
        hp.RecordTypeId = Schema.SObjectType.Hosting_Program__c.getRecordTypeInfosByDeveloperName().get('X18').getRecordTypeId(); //flagship
        Insert hp;
        return hp;
    }
    
    public static Program_Offer__c create_Program_Offer(Account acc,Hosting_Program__c hp){
        Program_Offer__c po = new Program_Offer__c();
        po.name = 'Test Program Offer';
        po.Hosting_Program__c = hp.Id;
        po.Sending_Partner__c = acc.Id;
        po.Applications_Received_To_local__c = System.today() + 30;
        po.RecordTypeId = Schema.SObjectType.Program_Offer__c.getRecordTypeInfosByDeveloperName().get('Minors').getRecordTypeId();
        Insert po;
        return po;
    }
    
    public static Program_Offer__c create_Program_Offer2(Account acc,Hosting_Program__c hp, String programOffName, Id recordtype){
        Program_Offer__c po = new Program_Offer__c();
        po.name = programOffName;
        po.Durations__c = 'SM';
        po.Hosting_Program__c = hp.Id;
        po.From__c = Date.newInstance(2020, 2, 17);
        po.To__c = Date.newInstance(2020, 6, 17);
        po.Sending_Partner__c = acc.Id;
        po.Flexible_Age_Range__c = 'Yes';
        po.Applications_Received_To_local__c = System.today() + 30;
        po.RecordTypeId = recordtype; //flagship
        Insert po;
        return po;
    }
    
     public static Account create_HouseHoldAcc(){
        Account acc = new Account();
        acc.name = 'Test House Hold Account';
        acc.IOC_Code__c = 'Test IOC Code';
        acc.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('HH_Account').getRecordTypeId();
        Insert acc;
        return acc;
    }
    public static contact create_Conact(Account acc,String Recordtype,String Name){
        contact con = new contact();
        con.lastName = Name;
        con.firstname ='Test' +  Name;
        con.RecordTypeId = Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get(Recordtype).getRecordTypeId();
        con.accountId = acc.id;
        Insert con;
        return con;
    }
   public static npe4__Relationship__c create_RelationRec(contact aplicantContact , contact relationContact){ 
    	npe4__Relationship__c relation = new npe4__Relationship__c();
        relation.npe4__Contact__c = aplicantContact.Id;
        relation.npe4__RelatedContact__c = relationContact.Id;
       	relation.npe4__Type__c ='Father';
        Insert relation;
        return relation ;
       
   }
    
   public static application__c create_Applicant(contact aplicantContact,Program_Offer__c po){
      application__c application = new application__c();
       try{
       application.Applicant__c = aplicantContact.Id;
       application.Program_Offer__c = po.Id;
       application.RecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByDeveloperName().get('Flagship').getRecordTypeId();
       insert application;
        
       }catch(Exception ex){
           system.assert(false,ex.getStackTraceString() + ' -- ' + ex.getMessage());
       }
       
       return application;
   }
    
    public static application__c create_Applicant2(contact aplicantContact,Program_Offer__c po,Id RecordTypeIdApp){
      application__c application = new application__c();
       try{
       application.Applicant__c = aplicantContact.Id;
       application.Program_Offer__c = po.Id;
       application.RecordTypeId = RecordTypeIdApp;
           system.debug('application.Applicant__c='+application.Applicant__c);
           system.debug('application.Program_Offer__c='+application.Program_Offer__c);
           system.debug('application.RecordTypeId='+application.RecordTypeId);
       insert application;
       }catch(Exception ex){
           System.debug('Exception {');
                System.debug('Message : ' + Ex.getMessage());
                System.debug('Cause : ' + Ex.getCause());
                System.debug('Line Number : ' + Ex.getLineNumber());
                System.debug('Type Name : ' + Ex.getTypeName());
                System.debug('StackTrace : ' + Ex.getStackTraceString());
                System.debug('}');
           //system.assert(false,ex.getStackTraceString() + ' -- ' + ex.getMessage());
       }
       
       return application;
   }
    
    public static ContentDocumentLink create_Document(String ApplicationID){
       
        ContentVersion contentVersion = new ContentVersion();
        contentVersion.Title = 'Penguins';
        contentVersion.PathOnClient = 'Penguins.jpg';
        contentVersion.VersionData = Blob.valueOf('Test Content');
        contentVersion.IsMajorVersion = true;
        insert contentVersion;    
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
    
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = ApplicationID;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        return cdl;
    }
    
    public static AFS_Triggers_Switch__c create_AFS_Triggers_Switch(String name,Boolean boolApp,Boolean boolOpp){
        AFS_Triggers_Switch__c swi = new AFS_Triggers_Switch__c();
        swi.name = name;
        swi.By_Pass_Application_Trigger__c = boolApp;
        swi.By_Pass_Opportunity_Trigger__c = boolOpp;
        Insert swi;
        return swi;
    }
    
    public static List<Master_To_Do_Template__c> createListMasterToDoTemplate(integer num){
        List<Master_To_Do_Template__c> newMasterTemplateList = new List<Master_To_Do_Template__c>();
        for(Integer i=0;i<num;i++){
            Master_To_Do_Template__c mt = new Master_To_Do_Template__c();
            mt.Name = 'Test mt '+i;
            mt.To_use_on__c = 'Program Offer';
            newMasterTemplateList.add(mt);
        }
        return newMasterTemplateList;
    }
    
    public static Master_To_Do__c create_Master_To_Do(Master_To_Do_Template__c temp,Scholarship__c sch,Program_Offer__c prog, String recordTypeDevName){
        Master_To_Do__c mtd = new Master_To_Do__c();
        mtd.name = 'Test Master To Do';
        if(sch != null){
       		mtd.Scholarship__c = sch.Id;
    	}
        if(prog != null){
			mtd.Program_Offer__c = prog.Id;
        }
        if(temp != null){
        	mtd.Template__c = temp.Id;
        }
        mtd.RecordTypeId = Schema.SObjectType.Master_To_Do__c.getRecordTypeInfosByDeveloperName().get(recordTypeDevName).getRecordTypeId();
        return mtd;
    }
    
    public static Scholarship_Application__c create_Scholarship_Application(Scholarship__c sch,Application__c app){
        Scholarship_Application__c schApp = new Scholarship_Application__c();
        schApp.Application_Date__c = date.today().addDays(2);
        schApp.Stage__c = 'Applying';
        schApp.Scholarship__c = sch.Id;
        schApp.Application__c = app.Id;
        
        return schApp;
    }
    
    public static Contact createContact(String name, Account acc){
        Contact con = new Contact();
        con.lastName = name;
        
        if(acc != null){
        	con.AccountId = acc.Id;
        }
        return con;
    }
    
    public static Application__c createApplication(Contact con,Program_Offer__c po){
       Application__c app = new Application__c();
       if(con != null){
           app.Applicant__c = con.Id;
       }
       if(po != null){
           app.Program_Offer__c = po.Id;
       }
       app.RecordTypeId = Schema.SObjectType.Application__c.getRecordTypeInfosByDeveloperName().get('Flagship').getRecordTypeId();
       return app;
    }
	
    public static Scoring_Settings__c createScoringSettingsRecord(String strName, String strObj, String strField, String strCondition, String param1, String param2, String param3, Decimal pts){
        Scoring_Settings__c ss = new Scoring_Settings__c();
        ss.Name = strName;
        ss.Object_Name__c = strObj;
        ss.Field_Api_Name__c = strField;
        ss.Condition__c = strCondition;
        ss.Parameter_1__c = param1;
        ss.Parameter_2__c = param2;
        ss.Parameter_3__c = param3;
        ss.Points__c = pts;
        return ss;
    }
    
    public static GlobalAPI__c createGlobalAPI(String adm,String endp, String sc, String year){
        GlobalAPI__c ga = new GlobalAPI__c();
        ga.AdminKey__c = adm;
        ga.Enabled_Integration__c = true;
        ga.EndPoint__c = endp;
        ga.SecretKey__c = sc;
        ga.Year__c = year;
        return ga;
    }
}