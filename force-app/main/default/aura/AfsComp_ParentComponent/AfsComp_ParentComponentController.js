({
	doInit : function(component, event, helper) {
		helper.onInit(component, event);
        helper.getProfilePic(component, event);
        helper.getLoginWrapper(component, event);
        helper.getContact(component, event);
        helper.getApplication(component, event);
        helper.getNavigateWrapper(component, event);        
	},
	
	doLanguageChange : function(component, event, helper) {
        if(event.currentTarget != undefined){
            var languageActive = component.get("v.languageSelected");
            if(languageActive != event.currentTarget.id){
                component.set("v.Spinner",true);
				component.set("v.languageSelected",event.currentTarget.id);
            	helper.onLanguageChange(component, event);                
            }
        }
		
	},
    
    updateContact : function(component, event, helper) {
        var loginWrapper = component.get("v.loginWrapper");
        var configWrapper = component.get("v.configWrapper");
        var messageTemplateStr;
        component.set("v.Spinner","true");
        if(loginWrapper.FirstName === undefined || loginWrapper.FirstName == ""){
            messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_FirstNameRequire");
        }
        
        if(loginWrapper.LastName === undefined || loginWrapper.LastName == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_LastNameRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_LastNameRequire");
            }
        }
        
        if(loginWrapper.Email === undefined || loginWrapper.Email == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_EmailRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_EmailRequire");
            }
        }else{
            var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if(!re.test(loginWrapper.Email)){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_EmailNoValid");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_EmailNoValid");
                }
            }
        }
        
        if(loginWrapper.DOB === undefined || loginWrapper.DOB == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_DOBRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_DOBRequire");
            }
        }
        
        if(loginWrapper.ZipCode === undefined || loginWrapper.ZipCode == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_ZipCodeRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_ZipCodeRequire");
            }
        }
        
        if(configWrapper.LoginComp_Fld_Mobile__c == 'Required'){
            if(loginWrapper.MobileNumber === undefined || loginWrapper.MobileNumber == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_MobileNumberRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_MobileNumberRequire");
                }
            }
        }
        
        if(loginWrapper.MobileNumber === 'invalid') {
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }
        }
        
        if(configWrapper.LoginComp_Fld_PromoCode__c == 'Required'){
            if(loginWrapper.PromoCode === undefined || loginWrapper.PromoCode == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_PromoCodeRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_PromoCodeRequire");
                }
            }
        }
        var none = $A.get("$Label.c.AfsLbl_SignUp_None");
        if(loginWrapper.HearAboutUs === undefined || loginWrapper.HearAboutUs == none){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_HearAboutUsRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_HearAboutUsRequire");
            }
        }
        
        if(loginWrapper.AgreeTerms == false){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Login_ErrMsgCheckbox");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Login_ErrMsgCheckbox");
            }
        }
        
        if(messageTemplateStr != undefined){
            var toastEvent = $A.get("e.force:showToast");
            var messageTemplate = messageTemplateStr;
            toastEvent.setParams({ 
                type : "Error",
                duration : 5000,
                message: messageTemplate
            });
            toastEvent.fire();
            component.set("v.Spinner","false");
            return;
        }
        
        helper.onUpdateUserContact(component, event);
	},
    
    handleUpdateSideProfileImageEvent : function(component, event, helper) {
		component.set("v.imageUrl",event.getParam("imgUrl"));       
	},
    
    toggleMenu : function(component, event, helper) {
        var flag = component.get("v.toggleMenuIcon");
        if(flag){
            component.set("v.toggleMenuIcon",false);
            component.set("v.toggleMenuClass",true); 
        }else{
            component.set("v.toggleMenuIcon",true);
            component.set("v.toggleMenuClass",false);
        }
	},
    
    checkAgreeTerms : function(component, event, helper) {
		var loginWrapper = component.get("v.loginWrapper");
        if(loginWrapper.AgreeTerms){
            loginWrapper.AgreeTerms = false;
        }else{
            loginWrapper.AgreeTerms = true;
        }
        component.set("v.loginWrapper",loginWrapper);
	},
    
    checkKeepMeInformed : function(component, event, helper) {
        var loginWrapper = component.get("v.loginWrapper");
        if(loginWrapper.KeepMeInformed){
            loginWrapper.KeepMeInformed = false;
        }else{
            loginWrapper.KeepMeInformed = true;
        }
        component.set("v.loginWrapper",loginWrapper);
	},
    
    navigateToSelectAProgram : function(component, event, helper) {
        var navigateWrapper = component.get("v.navigateWrapper");
        navigateWrapper.isSelectProgram = true;
        navigateWrapper.isSelectProgramSideBar = true;
        navigateWrapper.isProgramDetail = false;
        navigateWrapper.isProgramDetailSideBar = false;
        navigateWrapper.isAboutYou = false;
        navigateWrapper.isAboutYouSideBar = false;
        component.set("v.navigateWrapper",navigateWrapper);
	},
    
    navigateToProgramDetail : function(component, event, helper) {
        var navigateWrapper = component.get("v.navigateWrapper");
        navigateWrapper.isSelectProgram = false;
        navigateWrapper.isSelectProgramSideBar = false;
        navigateWrapper.isProgramDetail = true;
        navigateWrapper.isProgramDetailSideBar = true;
        navigateWrapper.isAboutYou = false;
        navigateWrapper.isAboutYouSideBar = false;
        component.set("v.navigateWrapper",navigateWrapper);
	},
    
    navigateToAboutYou : function(component, event, helper) {
        var navigateWrapper = component.get("v.navigateWrapper");
        navigateWrapper.isSelectProgram = false;
        navigateWrapper.isSelectProgramSideBar = false;
        navigateWrapper.isProgramDetail = false;
        navigateWrapper.isProgramDetailSideBar = false;
        navigateWrapper.isAboutYou = true;
        navigateWrapper.isAboutYouSideBar = true;
        component.set("v.navigateWrapper",navigateWrapper);
	}
    
})