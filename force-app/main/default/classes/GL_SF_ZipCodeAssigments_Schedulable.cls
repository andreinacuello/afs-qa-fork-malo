/*Integration Class to Retrieve ZipCodeAssigments from GlobalLink to Salesforce*/
global class GL_SF_ZipCodeAssigments_Schedulable implements Schedulable{    
    global void execute(SchedulableContext sc) {
        List<String> lstIOCCodes = new List<String>();
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('AFS_Chapters').getRecordTypeId();
        //Get all IOC Codes
        List<AggregateResult> lstAccount = [SELECT IOC_Code__c FROM Account WHERE RecordTypeId = :recordTypeId GROUP BY IOC_Code__c];
        for(AggregateResult agg : lstAccount){
            lstIOCCodes.add((String)agg.get('IOC_Code__c'));           
        }
        //Call First Queueable
        System.enqueueJob(new ZipCodeAssigments_Queueable(lstIOCCodes,0));
    }
	
}