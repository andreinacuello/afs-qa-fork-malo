({
	doInit : function(component, event, helper) {
        helper.onInit(component, event);
	},
	
	doLanguageChange : function(component, event, helper) {
        if(event.currentTarget != undefined){
            var languageActive = component.get("v.languageSelected");
            if(languageActive != event.currentTarget.id){
                component.set("v.Spinner",true);
				component.set("v.languageSelected",event.currentTarget.id);
				component.set("v.islanguageSelectedByUser", true);
            	helper.onLanguageChange(component, event, false);                
            }
        }
	},
	
    showDetail : function(component, event, helper) {
        helper.getDetails(component,event);
	},
    
    toggleMenu : function(component, event, helper) {
        var flag = component.get("v.toggleMenuIcon");
        if(flag){
            component.set("v.toggleMenuIcon",false);
            component.set("v.toggleMenuClass",true); 
        }else{
            component.set("v.toggleMenuIcon",true);
            component.set("v.toggleMenuClass",false);
        }
	},
	
	
	checkAgreeTerms : function(component, event, helper) {
		var AgreeSocialTerms = component.get("v.AgreeSocialTerms");
        if(AgreeSocialTerms){
            AgreeSocialTerms = false;
        }else{
            AgreeSocialTerms = true;
        }
        component.set("v.AgreeSocialTerms",AgreeSocialTerms);
	},
	
	signUpCommunityFromFBButton  : function(component, event, helper) {
		
        helper.navigateToSocial(component, component.get("v.authFBURL"));
	},
	
	signUpCommunityFromGmailButton  : function(component, event, helper) {
		
        helper.navigateToSocial(component, component.get("v.authGoogleURL"));
	}
	
})