/**
 *  Apex Class to have all utility methods
 */
global with sharing class Afs_UtilityApex {
	
    global static list<String> getPicklistVal(String objectName, string fld){
        SObject objObject = Schema.getGlobalDescribe().get(objectName).newSObject() ;
    	Schema.sObjectType objType = objObject.getSObjectType();               
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();                
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap(); 
        list<String> valueList = new list<String>();
        for(Schema.PicklistEntry picklistVal : fieldMap.get(fld).getDescribe().getPickListValues()){
        	if(!picklistVal.getValue().equalsIgnoreCase('None')){
            	valueList.add(picklistVal.getLabel());
        	}
        }
        return valueList;
    }
    
    global static map<String,String> getPicklistValMap(String objectName, string fld){
        SObject objObject = Schema.getGlobalDescribe().get(objectName).newSObject() ;
    	Schema.sObjectType objType = objObject.getSObjectType();               
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();                
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap(); 
        map<String,String> valueMap = new map<String,String>();
        for(Schema.PicklistEntry picklistVal : fieldMap.get(fld).getDescribe().getPickListValues()){
        	if(!picklistVal.getValue().equalsIgnoreCase('None')){
            	valueMap.put(picklistVal.getValue(),picklistVal.getLabel());
        	}
        }
        return valueMap;
    }
    
    global static list<PicklistWrapperUtil> getPicklistWrapper(String objectName, string fld){
        SObject objObject = Schema.getGlobalDescribe().get(objectName).newSObject() ;
    	Schema.sObjectType objType = objObject.getSObjectType();               
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();                
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap(); 
        list<PicklistWrapperUtil> valueList = new list<PicklistWrapperUtil>();
        system.debug(' --@@- >' + fld);
        for(Schema.PicklistEntry picklistVal : fieldMap.get(fld).getDescribe().getPickListValues()){
            if(!picklistVal.getValue().equalsIgnoreCase('None')){
            	valueList.add( new PicklistWrapperUtil(picklistVal.getLabel(),false,picklistVal.getValue()));
            }
        }
        return valueList;
    }
    
    global static list<String> getFields (String selectedObject, Boolean getOnlyCustomFields){
        list<String> lstfieldname = new list<String>();
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(selectedObject).getDescribe().fields.getMap();
        for(Schema.SObjectField sfield : fieldMap.Values()){
            schema.describefieldresult dfield = sfield.getDescribe();
            if(getOnlyCustomFields){
                if(dfield.getname().contains('__c')){
                    lstfieldname.add(dfield.getname());
                }
            }else{
                lstfieldname.add(dfield.getname());
            }
            
        }
        return lstfieldname;
    }
    
    global static void logger(Object msg){
        System.debug('@@@@AFS-Community Log: ' + Network.getNetworkId() + ', DETAILS: ' + msg);        
    }
    
    global static map<String,String> getAccountCountryLabel(){
        // Code added to get destinations translation
        SObject objObjectAcc = Schema.getGlobalDescribe().get('Account').newSObject() ;
        Schema.sObjectType objType = objObjectAcc.getSObjectType();               
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();                
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap(); 
        
        map<String,String> valueMap = new map<String,String>();
        for(Schema.PicklistEntry picklistVal : fieldMap.get('Country__c').getDescribe().getPickListValues()){
            valueMap.put(String.valueOf(picklistVal.getValue()),String.valueOf(picklistVal.getLabel()));
        }
        return valueMap;
    }
    
    /**
     * Desc : Future method to update user 
     * 	      used in Setting section
     */
    @future
    global static void updateUser(id userId){
        User userObj = [SELECT id,FirstName,MiddleName,LastName,Email,ContactId FROM User WHERE id=: userId]; 
        Contact contactInfo = [SELECT id,FirstName,MiddleName,LastName,Email FROM Contact WHERE id=: userObj.ContactId];
        userObj.FirstName = contactInfo.FirstName; 
        userObj.LastName = contactInfo.LastName;
        userObj.Email = contactInfo.Email;
        userObj.MiddleName = contactInfo.MiddleName;
        update userObj;
    }
    
    // Wrapper to handle picklist values
    global class PicklistWrapperUtil implements Comparable{
    	@AuraEnabled
        global Boolean isSelected {get;set;}
        @AuraEnabled
        global String fieldVal {get;set;}
        @AuraEnabled
        global String fieldLabel {get;set;}
        
        global PicklistWrapperUtil(String lbl, boolean flag,String val){
            fieldVal = val;
            isSelected = flag;
            fieldLabel = lbl;
        }
        
        global Integer compareTo(Object ObjToCompare) {
	        return fieldLabel.CompareTo(((PicklistWrapperUtil)ObjToCompare).fieldLabel);
	    }
    } 
}