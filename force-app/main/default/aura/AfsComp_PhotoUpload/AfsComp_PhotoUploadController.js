({
    doInit : function (cmp, event, helper) {
        //cmp.set("v.Spinner", "true");
        //helper.onInit(cmp,event);
    },
    
    handleUploadFinished : function (cmp, event, helper) {
        var uploadedFiles = event.getParam("files");
        helper.getBase64String(cmp, event,uploadedFiles[0].documentId);
    },
    
    closeModel: function(cmp, event, helper) {
        cmp.set("v.isOpen", "false");
    },
    
    openUploadModal: function(cmp, event, helper) {
        	cmp.set("v.isOpen", "true");
    },
    changeProfilePhoto: function(cmp, event, helper) {
        // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
        //cmp.set("v.showDP", "false");
    }
    
    
})