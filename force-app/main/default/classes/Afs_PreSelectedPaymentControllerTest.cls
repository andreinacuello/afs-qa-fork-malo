@isTest(SeeAllData=false)
public class Afs_PreSelectedPaymentControllerTest {
    @testSetup
    public static void setup_test(){
        
    }	
    
    public static testMethod void getApplicationProgramTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Test.startTest();        
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.create_PaymentInstallments();
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        List<Program_Offer__c>  application  = [select Id from Program_Offer__c limit 1] ;       
        System.assertEquals(true,Afs_PreSelectedPaymentController.getApplicationProgram(application[0].id) != null);
        Test.stopTest(); 
    }
    
    public static testMethod void getApplicationFeeTest(){
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Test.startTest();        
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.create_PaymentInstallments();
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        List<application__c>  application  = [select Id from application__c limit 1] ;       
        System.assertEquals(true,Afs_PreSelectedPaymentController.getApplicationFee(application[0].id) != null);
        Test.stopTest(); 
    }
    
    public static testMethod void updateApplicationTest(){
        
        Afs_TestDataFactory.createTriggerSwitch();
        Afs_TestDataFactory.createPartnerAccountRecords(1);
        Afs_TestDataFactory.createContactRecords(1);
        Afs_TestDataFactory.createConfigurationObj();
        Afs_TestDataFactory.createProgramOfferRecords(1);
        Test.startTest();
        Afs_TestDataFactory.createApplicationRecords(1);
        Afs_TestDataFactory.create_PaymentInstallments();
        Afs_TestDataFactory.createProgramOfferedAndApplicationLink();
        List<application__c>  application  = [select Id from application__c limit 1] ; 
        To_Do_Item__c obj = new To_Do_Item__c();
        obj.Name = 'sd'; 
        obj.Type__c = 'Filling Field'; 
        obj.Stage_of_portal__c = '1. Stage: Pre-application'; 
        obj.Application__c  = application[0].id; 
        obj.Status__c = 'Pending'; 
        obj.Due_Date__c = Date.today().addDays(10); 
        insert obj;
        Afs_PreSelectedPaymentController.updateApplication('',application[0].id,obj.id);
        System.assertEquals(false,application.isEmpty());
        Test.stopTest(); 
    }
    
}