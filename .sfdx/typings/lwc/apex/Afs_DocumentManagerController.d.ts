declare module "@salesforce/apex/Afs_DocumentManagerController.getDownloadForm" {
  export default function getDownloadForm(param: {recId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_DocumentManagerController.getFiles" {
  export default function getFiles(param: {recId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_DocumentManagerController.updateDocumentLink" {
  export default function updateDocumentLink(param: {contentDocumentIds: any, toDoItem: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_DocumentManagerController.deleteFileSF" {
  export default function deleteFileSF(param: {fileId: any, recId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_DocumentManagerController.updateParent" {
  export default function updateParent(param: {recId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_DocumentManagerController.completeTask" {
  export default function completeTask(param: {recId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_DocumentManagerController.completeDueDate" {
  export default function completeDueDate(param: {toDoList: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_DocumentManagerController.getDocumentList" {
  export default function getDocumentList(param: {appId: any, stage: any}): Promise<any>;
}
