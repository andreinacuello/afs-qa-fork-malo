public class ApplicationsToProgramOfferRollupsHelper {
   /* public  static void recalculateStageFields (Application__c application){*/
    	/*Set<Id> programOfferIdSet = new Set<Id>();
    	
	    for(Application__c application: applicationList){
	    	if(application.Program_Offer__c != null && programOfferIdSet.contains(application.Program_Offer__c) == false){
	    		programOfferIdSet.add(minuta.Program_Offer__c);
	    	}
	    }*/
	    
	    /*List<Program_Offer__c> programOfferList = [Select Id, Product_Desire__c, Participation_Desire__c, Decision__c, Confirmation__c, Purchase__c, Onboarding__c, Participation__c, Cancelled__c from Program_Offer__c where Id =:application.Program_Offer__c];
	    
	    List<Application__c> applicationChildsList = [Select Id, Program_Offer__c, Status__c from Application__c where Program_Offer__c =: application.Program_Offer__c];
	    
	    Integer Product_Desire = 0;
	    Integer Participation_Desire = 0;
	    Integer Decision = 0;
	    Integer Confirmation = 0;
	    Integer Purchase = 0;
	    Integer Onboarding = 0;
	    Integer Participation = 0;
	    Integer Cancelled = 0;
	    
	    for(Application__c applicationChild: applicationChildsList){
	    	if(applicationChild.Status__c == 'Product Desire'){
	    		Product_Desire++;
	    	}else if(applicationChild.Status__c == 'Participation Desire'){
	    		Participation_Desire++;
	    	}else if(applicationChild.Status__c == 'Decision'){
	    		Decision++;
	    	}else if(applicationChild.Status__c == 'Confirmation'){
	    		Confirmation++;
	    	}else if(applicationChild.Status__c == 'Purchase'){
	    		Purchase++;
	    	}else if(applicationChild.Status__c == 'Onboarding'){
	    		Onboarding++;
	    	}else if(applicationChild.Status__c == 'Participation'){
	    		Participation++;
	    	}else if(applicationChild.Status__c == 'Cancelled'){
	    		Cancelled++;
	    	}
	    }
	    
	    for(Program_Offer__c programOffer: programOfferList){
	     	programOffer.Product_Desire__c = Product_Desire;
	     	programOffer.Participation_Desire__c = Participation_Desire;
	     	programOffer.Decision__c = Decision;
	     	programOffer.Confirmation__c = Confirmation;
	     	programOffer.Purchase__c = Purchase;
	     	programOffer.Onboarding__c = Onboarding;
	     	programOffer.Participation__c = Participation;
	     	programOffer.Cancelled__c = Cancelled;
	    }
	    
	    update programOfferList;
    }*/
    
    public static List<Program_Offer__c> recalculateStageFields_refactor (List<Application__c> newList, Map<Id, Application__c> oldMap,Boolean isInsert, Boolean isUpdate, Boolean isDelete){
    	Map<Id,Program_Offer__c> mapProgramOfferToUpdate = new Map<Id,Program_Offer__c>();
    	List<Application__c> lstAppsToProcess = new List<Application__c>();
    	Set<Id> setProgramOffersIds = new Set<Id>();
    	
    	//Application to Process
    	if(isInsert || isUpdate){
	    	for(Application__c app : newList){
	    		if(app.Program_Offer__c != null && ((isInsert && app.Status__c != null) || (isUpdate && app.Status__c != oldMap.get(app.Id).Status__c))){
	    			lstAppsToProcess.add(app);
	    			setProgramOffersIds.add(app.Program_Offer__c);	    		
	    		}
	    	}
    	}else if(isDelete){
    		for(Application__c app : oldMap.Values()){
	    		if(app.Program_Offer__c != null && app.Status__c != null){
	    			lstAppsToProcess.add(app);
	    			setProgramOffersIds.add(app.Program_Offer__c);
	    		}
	    	}
    	}
        //if(!Test.isRunningTest()){
    		if(lstAppsToProcess.size() > 0){
            system.debug('setProgramOffersIds='+setProgramOffersIds);
			Map<Id,Program_Offer__c> mapProgramOfferById = new Map<Id,Program_Offer__c>([SELECT Id, Product_Desire__c, Participation_Desire__c, Decision__c, Confirmation__c, Purchase__c, Onboarding__c, Participation__c, Cancelled__c FROM Program_Offer__c WHERE Id = :setProgramOffersIds]);			
            for(Application__c app : lstAppsToProcess){
                Program_Offer__c po = new Program_Offer__c();
				if(isInsert || isUpdate){
					po = mapProgramOfferById.get(app.Program_Offer__c);
					if(app.Status__c == 'Product Desire'){
                        if(po.Product_Desire__c != null){
                        	po.Product_Desire__c++;
                        }else{
                            po.Product_Desire__c = 1;
                        }
			    	}else if(app.Status__c == 'Participation Desire'){			    		
                        if(po.Participation_Desire__c != null){
                        	po.Participation_Desire__c++;
                        }else{
                            po.Participation_Desire__c = 1;
                        }
			    	}else if(app.Status__c == 'Decision'){
                        if(po.Decision__c != null){
                        	po.Decision__c++;
                        }else{
                            po.Decision__c = 1;
                        }
			    	}else if(app.Status__c == 'Confirmation'){
                        if(po.Confirmation__c != null){
                        	po.Confirmation__c++;
                        }else{
                            po.Confirmation__c = 1;
                        }
			    	}else if(app.Status__c == 'Purchase'){
                        if(po.Purchase__c != null){
                        	po.Purchase__c++;
                        }else{
                            po.Purchase__c = 1;
                        }
			    	}else if(app.Status__c == 'Onboarding'){
                        if(po.Onboarding__c != null){
                        	po.Onboarding__c++;
                        }else{
                            po.Onboarding__c = 1;
                        }
			    	}else if(app.Status__c == 'Participation'){
			    		if(po.Participation__c != null){
                        	po.Participation__c++;
                        }else{
                            po.Participation__c = 1;
                        }
			    	}else if(app.Status__c == 'Cancelled'){
                        if(po.Cancelled__c != null){
                        	po.Cancelled__c++;
                        }else{
                            po.Cancelled__c = 1;
                        }
			    	}
				}
				if(isDelete || isUpdate){
					Application__c oldApp = oldMap.get(app.Id);
					po = mapProgramOfferById.get(app.Program_Offer__c);
					if(oldApp.Status__c == 'Product Desire'){
                        if(po.Product_Desire__c != null){
                        	po.Product_Desire__c--;
                        }else{
                            po.Product_Desire__c = 0;
                        }
			    	}else if(oldApp.Status__c == 'Participation Desire'){
			    		if(po.Participation_Desire__c != null){
                        	po.Participation_Desire__c--;
                        }else{
                            po.Participation_Desire__c = 0;
                        }
			    	}else if(oldApp.Status__c == 'Decision'){
                        if(po.Decision__c != null){
                        	po.Decision__c--;
                        }else{
                            po.Decision__c = 0;
                        }
			    	}else if(oldApp.Status__c == 'Confirmation'){
                        if(po.Confirmation__c != null){
                        	po.Confirmation__c--;
                        }else{
                            po.Confirmation__c = 0;
                        }
			    	}else if(oldApp.Status__c == 'Purchase'){
                        if(po.Purchase__c != null){
                        	po.Purchase__c--;
                        }else{
                            po.Purchase__c = 0;
                        }
			    	}else if(oldApp.Status__c == 'Onboarding'){
                        if(po.Onboarding__c != null){
                        	po.Onboarding__c--;
                        }else{
                            po.Onboarding__c = 0;
                        }
			    	}else if(oldApp.Status__c == 'Participation'){
                        if(po.Participation__c != null){
                        	po.Participation__c--;
                        }else{
                            po.Participation__c = 0;
                        }                        
			    	}else if(oldApp.Status__c == 'Cancelled'){
                        if(po.Cancelled__c != null){
                        	po.Cancelled__c--;
                        }else{
                            po.Cancelled__c = 0;
                        }                			    	}
				}
				mapProgramOfferToUpdate.put(po.Id,po);
			}
			
			if(mapProgramOfferToUpdate.Size() > 0){
				update mapProgramOfferToUpdate.values();
			}			
			
    	}
        /*}else{
            
        }*/
    	return mapProgramOfferToUpdate.values();
    }
	
}