declare module "@salesforce/apex/Afs_PreSelectedController.getProgramOffered" {
  export default function getProgramOffered(param: {prId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_PreSelectedController.getProgramOfferedAccepted" {
  export default function getProgramOfferedAccepted(param: {prId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_PreSelectedController.isAppProcessed" {
  export default function isAppProcessed(param: {contactId: any, applicationId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_PreSelectedController.getProgramWrapper" {
  export default function getProgramWrapper(param: {contactId: any, applicationId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_PreSelectedController.payAndSave" {
  export default function payAndSave(param: {contactId: any, applicationId: any, programOfferId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_PreSelectedController.doChangeRequest" {
  export default function doChangeRequest(param: {applicationObj: any}): Promise<any>;
}
