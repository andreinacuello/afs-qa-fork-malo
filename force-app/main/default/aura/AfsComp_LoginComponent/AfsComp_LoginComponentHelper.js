({
    onInit : function(component, event) {
		component.set("v.Spinner","true");
		var action = component.get("c.getConfiguration");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.configWrapper",response.getReturnValue());
                component.set("v.Spinner","false");
                var configObj = response.getReturnValue();
                
                //prepare authURLs
                var comHomeURL = component.get("v.configWrapper.Community_URL__c");
                var startURL = '/s/?';
                
                //code for maintain programid
                var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
                var prIdValue; 
                if(sPageURL.indexOf('prId') > 0){
                	var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
                    var sParameterName;
                    for (var i = 0; i < sURLVariables.length; i++) {
                        sParameterName = sURLVariables[i].split('='); //to split the key from the value.
            
                        if (sParameterName[0] === 'prId') { //lets say you are looking for param name - firstName
                            prIdValue = (sParameterName[1] === undefined) ? '' : sParameterName[1];
                            startURL += 'prId=' + prIdValue + '&';
                        }
                    }    
                }
                
                var authFBURL = comHomeURL + '/services/auth/sso/Facebook?community=' + comHomeURL + '&startURL=' + startURL + 'from=facebook';
                var authGoogleURL = comHomeURL + '/services/auth/sso/Google?community=' + comHomeURL + '&startURL=' + startURL + 'from=google';
                var signUpUrl = comHomeURL + "/s/applynowregister";
                var forgetPasswordUrl = comHomeURL + "/s/afsforgetpassword";
                //console.log(authFBURL);
                //console.log(authGoogleURL);
                
                component.set("v.authFBURL", authFBURL);
                component.set("v.authGoogleURL",authGoogleURL);
                component.set("v.signUpUrl",signUpUrl);
                component.set("v.forgetPasswordUrl",forgetPasswordUrl);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                 component.set("v.Spinner","false");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	},
    
    getDetails :  function (component,event){
        var action = component.get("c.getLoginWrapper");
       
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.loginWrapper",response.getReturnValue());
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                 component.set("v.Spinner","false");
            }
        });
        $A.enqueueAction(action);
    },
    
    doLogin :  function (component,event){
        var action = component.get("c.userLogin");
        var loginWrapper = component.get("v.loginWrapper");
       	
        //code to maintain prId
        var sPageURL = decodeURIComponent(window.location.search.substring(1)); //You get the whole decoded URL of the page.
        var prIdValue; 
        if(sPageURL.indexOf('prId') > 0){
            var sURLVariables = sPageURL.split('&'); //Split by & so that you get the key value pairs separately in a list
            var sParameterName;
    
            for (var i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('='); //to split the key from the value.
                
                if (sParameterName[0] === 'prId') { //lets say you are looking for param name - firstName
                    prIdValue = (sParameterName[1] === undefined) ? '' : sParameterName[1];
                }
            }
        }
        action.setParams({
            "userName" : loginWrapper.Email,
            "password" : loginWrapper.Password,
            "prId" : prIdValue
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner","false");
            }
        });
        $A.enqueueAction(action);
    }
})