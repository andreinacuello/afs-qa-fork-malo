({
    doInit : function(component, event, helper) {
        component.set("v.Spinner", true);
        var rec = component.get("v.record");
        var configWrapper = component.get("v.configWrapper");
        var fileId;
        if(rec.item.SUBMIT_YOUR_HEALTH_UPDATE_FORM__c){
            fileId = configWrapper.DocumentManager_Txt_HealthUpdateForm__c;
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_HealthUpdateHeader__c ,component);
            component.set("v.DocumentHeader", component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_HealthUpdateDesc__c ,component);
            component.set("v.DocumentDesc",component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_HealthUpdateHelpTxt__c ,component);
            component.set("v.DocumentHelpText", component.get("v.LabelTempVal"));
            
        }else if(rec.item.SUBMIT_YOUR_HEALTH_CERTIFICATE_BY__c ){
            fileId = configWrapper.DocumentManager_Txt_HealthCertificate__c;
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_HealthCertificatHead__c ,component);
            component.set("v.DocumentHeader", component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_HealthCertificatDesc__c,component);
            component.set("v.DocumentDesc",component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_HealthCertifHelpTxt__c ,component);
            component.set("v.DocumentHelpText", component.get("v.LabelTempVal"));
            
        }else if(rec.item.SUBMIT_YOUR_ACADEMIC_HISTORY__c){
            fileId = configWrapper.DocumentManager_Txt_AcademicHistory__c;
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_AcademicHistoryHead__c ,component);
            component.set("v.DocumentHeader", component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_AcademicHistoryDesc__c ,component);
            component.set("v.DocumentDesc",component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_AcademicHistHelpTxt__c ,component);
            component.set("v.DocumentHelpText", component.get("v.LabelTempVal"));
            
        }else if(rec.item.SIGN_PARTICIPATION_AGREEMENT_BY__c){
            fileId = configWrapper.DocumentManager_Txt_ParticipatAcademi__c;
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_ParticipatAcadHead__c ,component);
            component.set("v.DocumentHeader", component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_ParticipatAcadDesc__c ,component);
            component.set("v.DocumentDesc",component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_ParticipatAcadHelTxt__c ,component);
            component.set("v.DocumentHelpText", component.get("v.LabelTempVal"));
            
        }else if(rec.item.SCHOLARSHIP_DOCUMENTATION__c ){
            fileId = configWrapper.DocumentManager_Txt_ScholarDoc__c;
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_ScholarDocHead__c ,component);
            component.set("v.DocumentHeader", component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_ScholarDocDesc__c ,component);
            component.set("v.DocumentDesc",component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_ScholarDocHelpTxt__c ,component);
            component.set("v.DocumentHelpText", component.get("v.LabelTempVal"));
            
        }else if(rec.item.FILL_IN_YOUR_DOMESTIC_RETURN_INFORMATION__c){
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_DomesticReturnHead__c,component);
            component.set("v.DocumentHeader", component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_DomesticReturnDesc__c ,component);
            component.set("v.DocumentDesc",component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_DomesticReturnHelp__c ,component);
            component.set("v.DocumentHelpText", component.get("v.LabelTempVal"));
            
        }else if(rec.item.FILL_IN_YOUR_DOMESTIC_TRAVEL_INFORMATION__c){
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_DomesticTravelHead__c,component);
            component.set("v.DocumentHeader", component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_DomesticTravelDesc__c,component);
            component.set("v.DocumentDesc",component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_DomesticTravelHelp__c,component);
            component.set("v.DocumentHelpText", component.get("v.LabelTempVal")); 
        
        }else if(rec.item.COMPLETE_YOUR_HOME_INTERVIEW__c){
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_HomeInterviewHead__c,component);
            component.set("v.DocumentHeader", component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_HomeInterviewDesc__c,component);
            component.set("v.DocumentDesc",component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_HomeInterviewHelpTxt__c,component);
            component.set("v.DocumentHelpText", component.get("v.LabelTempVal")); 
        
        }else if(rec.item.START_YOUR_LANGUAGE_TRAINING__c){
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_LanguageTrainingHead__c,component);
            component.set("v.DocumentHeader", component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_LanguageTrainingDesc__c,component);
            component.set("v.DocumentDesc",component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_LanguageTrainingHelp__c,component);
            component.set("v.DocumentHelpText", component.get("v.LabelTempVal")); 
        
        }else if(rec.item.ATTEND_YOUR_PRE_DEPARTURE_ORIENTATION__c){
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_PreDepartureHead__c,component);
            component.set("v.DocumentHeader", component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_PreDepartureDesc__c,component);
            component.set("v.DocumentDesc",component.get("v.LabelTempVal"));
            helper.getDynamicCustomLabelVal(configWrapper.DocumentManager_Txt_PreDepartureHelp__c,component);
            component.set("v.DocumentHelpText", component.get("v.LabelTempVal")); 
        
        }else{
            component.set("v.DocumentHeader", rec.item.Name);
        }
        // $A.get('e.force:refreshView').fire();
        helper.getDownloadFormUrl(component, event, fileId);
        //helper.getFiles(component, event);
        window.scrollTo(0, 0);
    },
    
    handleUploadFinished : function(component, event, helper) {
    	//get file list
    	 component.set("v.Spinner", true);
         var uploadedFiles = event.getParam("files"); 
         var files = component.get("v.files");
         var fileIdsList = [];
         for(var i  = 0 ; i< uploadedFiles.length ; i ++){
            var filObj = new Object();
            filObj.ContentDocumentId = uploadedFiles[i].documentId;
            fileIdsList.push(uploadedFiles[i].documentId);
            filObj.Title = uploadedFiles[i].name;
            files.push(filObj);
         }
        if(files.length == 0){
            component.set("v.isFileAdded",false);                
        }else{
            component.set("v.isFileAdded",true);  //file present
        }
        component.set("v.files",files);
        
        //helper.getFiles(component, event);
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": $A.get("$Label.c.AfsLbl_DM_FileUploadTitle"),
            "message": $A.get("$Label.c.AfsLbl_DM_FileUploadMsg")
        });
        toastEvent.fire();
        helper.doUpdateDocumentLink(component, event, fileIdsList);
        component.set("v.Spinner", false);
    },
    
    updateItemStatus : function(component, event, helper) {
         component.set("v.Spinner", true);
         var record  = component.get("v.record");
        if(record.item.Type__c != 'Filling Field'){
           if(!component.get("v.isFileAdded")){
               var toastEvent = $A.get("e.force:showToast");
               toastEvent.setParams({
                   "title": $A.get("$Label.c.AfsLbl_DM_FileUploadErrorTitle"),
                   "message": $A.get("$Label.c.AfsLbl_DM_FileUploadErrorMsg")
               });
               toastEvent.fire();
               component.set("v.Spinner", false);
               return;
           } 
        }else{
            if(record.item.Description__c == undefined || record.item.Description__c == ""){
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Not Allowed!",
                    "message": $A.get("$Label.c.AfsLbl_Error_PleaseAnswer")
                });
                toastEvent.fire();
                component.set("v.Spinner", false);
                return;
            }
        }
         
        
        //only when there is a file
    	var action = component.get("c.updateParent");
        action.setParams({ recId : component.get("v.record.item") });
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.parentToggle",false); 
                 component.set("v.Spinner", false);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                 component.set("v.Spinner", false);
                
            }
        });
        $A.enqueueAction(action);                       
    }, 
    
    deleteFile : function(component, event, helper) {
         component.set("v.Spinner", true);
    	helper.deleteFileAndUpdateList(component, event);
	},
    
    backToList : function(component, event, helper) {
        component.set("v.parentToggle",false);
    }
        
})