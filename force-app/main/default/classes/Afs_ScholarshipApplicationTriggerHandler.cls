public without sharing class Afs_ScholarshipApplicationTriggerHandler {
    
    public static List<Scholarship_Application__c> triggerNew = new List<Scholarship_Application__c>();
    public static List<Scholarship_Application__c> triggerOld = new List<Scholarship_Application__c>();
    public static Map<Id,Scholarship_Application__c> triggerNewMap = new Map<Id,Scholarship_Application__c>();
    public static  Map<Id,Scholarship_Application__c> triggerOldMap = new Map<Id,Scholarship_Application__c>();
    public static set<id> scholarshipIds = new set<id> ();
    public static set<id> applicationIds = new set<id> ();
    public static set<id> programIds = new set<id> ();
	public static boolean byPassTrigger = false;  
    public static boolean isTriggerRunning = false; 
    
    // Method called after insert event
    public static void afterInsert(){
        isTriggerRunning = true;
        syncScholarshipApplicationsOppty(getScholarshipApplications(false),false);
    }
    
    // Method called after update event
    public static void afterUpdate(){
        isTriggerRunning = true;
        syncScholarshipApplicationsOppty(getScholarshipApplications(true),true);
    }
    
    // Method called after delete event
    public static void afterDelete(){
        isTriggerRunning = true;
        deleteOpportunity();
    }
    
    // Get list of all scholarship applications needed to be synced
    private static list<Scholarship_Application__c> getScholarshipApplications(boolean isUpdate){
        set<id> scholarshipApplicationSet = new set<id> ();
        for(Scholarship_Application__c scholarApp : triggerNew){
         	// to execute in case of update
            if(isUpdate){
                // check added to query only those records where required values are getting changed
                Boolean flag = false;
                if(scholarApp.Amount_Asked__c != triggerOldMap.get(scholarApp.id).Amount_Asked__c){
                    flag = true;
                }else if(scholarApp.Amount_Awarded__c != triggerOldMap.get(scholarApp.id).Amount_Awarded__c){
                    flag = true;
                }else if(scholarApp.Applicant__c != triggerOldMap.get(scholarApp.id).Applicant__c){
                    flag = true;
                }else if(scholarApp.Application__c != triggerOldMap.get(scholarApp.id).Application__c){
                    flag = true;
                }else if(scholarApp.Application_Date__c != triggerOldMap.get(scholarApp.id).Application_Date__c){
                    flag = true;
                }else if(scholarApp.Eli__c != triggerOldMap.get(scholarApp.id).Eli__c){
                    flag = true;
                }else if(scholarApp.GPA__c != triggerOldMap.get(scholarApp.id).GPA__c){
                    flag = true;
                }else if(scholarApp.Program_Offer__c != triggerOldMap.get(scholarApp.id).Program_Offer__c){
                    flag = true;
                }else if(scholarApp.Scholarship__c != triggerOldMap.get(scholarApp.id).Scholarship__c){
                    flag = true;
                }else if(scholarApp.Stage__c != triggerOldMap.get(scholarApp.id).Stage__c){
                    flag = true;
                }
                
                if(flag){
                    scholarshipApplicationSet.add(scholarApp.id);
                    scholarshipIds.add(triggerOldMap.get(scholarApp.id).Scholarship__c);
                    applicationIds.add(triggerOldMap.get(scholarApp.id).Application__c);
                    programIds.add(triggerOldMap.get(scholarApp.id).Program_Offer__c);
                }
            }else{
                scholarshipApplicationSet.add(scholarApp.id);
                scholarshipIds.add(scholarApp.Scholarship__c);
            }
        }
        
        return [SELECT id,Amount_Asked__c,Amount_Awarded__c,Applicant__c,Applicant__r.FirstName,
                Application__c,Application_Date__c,Eli__c,GPA__c,Program_Offer__c,Scholarship__c,
                Stage__c FROM Scholarship_Application__c WHERE id IN: scholarshipApplicationSet];
    }
    
    // Sync Opportunity and ScholarshipApplications
    private static void syncScholarshipApplicationsOppty(list<Scholarship_Application__c> scholarshipApplicationToSync, Boolean isUpdate){
        map<String,Opportunity> mapScholarshipIdVsOppty = new map<String,Opportunity>();
        list<Opportunity> upsertOpportunity = new list<Opportunity>();
        Id RecordTypeID  = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Scholarship').getRecordTypeId();

        if(isUpdate){
            for(Opportunity oppty : [SELECT id,Scholarship_P__c,Application_P__c,Program_Offer_P__c FROM Opportunity
                                     WHERE Scholarship_P__c IN: scholarshipIds AND Application_P__c IN: applicationIds
                                     AND Program_Offer_P__c IN: programIds]){
                mapScholarshipIdVsOppty.put(oppty.Scholarship_P__c+'-'+oppty.Application_P__c+'-'+oppty.Program_Offer_P__c,oppty);
            }
        }
        
        for(Scholarship_Application__c scholarApp : scholarshipApplicationToSync){
            Opportunity opp = new Opportunity();
            opp.Name = scholarApp.Applicant__r.FirstName + ':' + System.label.AfsLbl_Trigger_Scholarship;
            opp.RecordTypeId = RecordTypeID;
            opp.Amount = scholarApp.Amount_Asked__c;
            opp.Amount_Awarded__c = scholarApp.Amount_Awarded__c;
            opp.Account_Name_P__c = scholarApp.Applicant__c;
            opp.Application_P__c = scholarApp.Application__c;
            opp.CloseDate = scholarApp.Application_Date__c;
            opp.Elegible_criteria__c = scholarApp.Eli__c;
            opp.GPA__c = scholarApp.GPA__c;
            opp.Program_Offer_P__c = scholarApp.Program_Offer__c;
            opp.Scholarship_P__c = scholarApp.Scholarship__c;
            opp.StageName = scholarApp.Stage__c;
            if(!mapScholarshipIdVsOppty.isEmpty() && mapScholarshipIdVsOppty.containskey(triggerOldMap.get(scholarApp.id).Scholarship__c+'-'+triggerOldMap.get(scholarApp.id).Application__c+'-'+triggerOldMap.get(scholarApp.id).Program_Offer__c)){
                opp.id = mapScholarshipIdVsOppty.get(triggerOldMap.get(scholarApp.id).Scholarship__c+'-'+triggerOldMap.get(scholarApp.id).Application__c+'-'+triggerOldMap.get(scholarApp.id).Program_Offer__c).id;
            }
            upsertOpportunity.add(opp);
        }
        //system.assert(false,upsertOpportunity);
        if(!upsertOpportunity.isEmpty()){
            Afs_Opportunity_Trigger_Helper.allowOppUpdate = true;
            upsert upsertOpportunity;
        }
    }
    
    // DELETE Opportunnity for all deleted scholarship application records
    private static void deleteOpportunity(){
        for(Scholarship_Application__c scholarApp : triggerOld){
            scholarshipIds.add(scholarApp.Scholarship__c);
        }
        
        DELETE [SELECT id FROM Opportunity WHERE Scholarship_P__c IN: scholarshipIds];
    }
    

}