({
	onResetPassword : function(component, event, helper) {
        component.set("v.Spinner",true);
    	helper.doResetPassword(component,event);
    },
    
    onLogin : function(component, event, helper) {
        component.set("v.Spinner",true);
    	helper.doLogin(component,event);
    }
})