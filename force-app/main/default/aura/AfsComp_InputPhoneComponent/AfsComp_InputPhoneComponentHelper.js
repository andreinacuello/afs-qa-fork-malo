({
    fetchPickListVal: function(component, savedNumber) {
        var action = component.get("c.getselectOptions");
        var opts = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                component.set("v.countries", allValues);
                opts = allValues.map(val => {
                    const [label, value] = val.split(' - ');
                    return ({
                        label,
                        value
                    })
                })
                component.find("InputSelectSingle").set("v.options", opts.sort((a, b) => {
                    var nameA = a.label.toUpperCase(); // ignore upper and lowercase
                    var nameB = b.label.toUpperCase(); // ignore upper and lowercase
                    if (nameA < nameB) {
                        return -1;
                    }
                    if (nameA > nameB) {
                        return 1;
                    }

                    // names must be equal
                    return 0;
                }));
                component.set("v.initFinished", true);
            }
        });
        $A.enqueueAction(action);
    },
    validateNumber: function(countryCode, phoneNumber, component) {
        const phoneUtil = libphonenumber.PhoneNumberUtil.getInstance();
        const inputCmp = component.find("inputCmp");
        //This is closed in a try-catch, because to parse and validate the number its length can't be shorter than 2.

        try {
            const number = phoneUtil.parseAndKeepRawInput(phoneNumber, countryCode);
            if (phoneUtil.isValidNumber(number)) {
                //Set the variable if it is valid.
                component.set("v.phone", phoneUtil.format(number, libphonenumber.PhoneNumberFormat.E164))
                inputCmp.set("v.errors", [{
                    message: ""
                }]);
            } else {
                //Set to null if it's not valid.
                component.set("v.phone", "invalid")
                inputCmp.set("v.errors", [{
                    message: "❌"
                }]);
            }
            component.set("v.phoneNumber", phoneNumber);
        } catch (e) {
            component.set("v.phone", "invalid")
            inputCmp.set("v.errors", [{
                message: "❌"
            }]);
        }
    }
})