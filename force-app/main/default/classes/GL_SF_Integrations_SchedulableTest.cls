@IsTest
private class GL_SF_Integrations_SchedulableTest {
    /*@IsTest public static void ScheduleTest(){
        Test.startTest();
        GL_SF_Integrations_Schedulable gl = new GL_SF_Integrations_Schedulable();
        String sch = '0 0 23 * * ?';
        system.schedule('Test GL_SF_Integrations_Schedulable', sch, gl);
        Test.StopTest();
    }*/
    private static testmethod void executeTest(){
        GlobalAPI__c GAPI = new GlobalAPI__c(Year__c = '2019', AdminKey__c = 'Test', EndPoint__c = 'Test', SecretKey__c = 'Test');
        insert GAPI;
        Test.setMock(HttpCalloutMock.class, new RemoteSiteManagerMock());
        Test.startTest();
        GL_SF_Integrations_Schedulable sch = new GL_SF_Integrations_Schedulable(); 
        sch.execute(null);
        Test.stopTest();
    }
}