({
	doInit : function(component, event, helper) {
		component.set("v.Spinner",true);
        helper.fetchPickListVal(component,"Asthma_Symptoms__c,Asthma_Severity__c,Asthma_Pet_Free_Room__c,Asthma_Live_With_Pets__c,Allergies_Severity__c,Allergies_Seasonality__c");
        helper.fetchPickListRadioVal(component,"Allergies__c,Allergies_medications__c,Allergies_EpiPen__c,Allergies_Affects__c,Allergies_special_considerations__c,Allergies_Pets__c,Allergies_Pet_free_room__c,Allergic_hairless_or_hypoallergenic_Pets__c,Asthma__c,Asthma_Medications__c,Asthma_Affect_Daily_Life__c,Asthma_Special_Considerations__c,Celiac_Disease__c,Celiac_Reaction__c,Celiac_Hospitalization__c,Celiac_Hospitalization_History__c,Celiac_Consume_Food_From_Same_Pot__c,Celiac_Consume_Food_From_Same_Plate_Ut__c,Celiac_Limit_Physical_Activities__c,Celiac_Visit_Doctor__c,Celiac_Medications__c,Diabetes__c,Diabetes_Need_to_visit_doctor__c,Dental_care_on_program__c");
        helper.fetchHealthInforRecord(component,event); 
        window.scrollTo(0,0);
	},
    
    setAllergies : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Allergies__c =event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setAllergiesMedication : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Allergies_medications__c =event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setAllergiesEpiPen : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Allergies_EpiPen__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setAllergiesAffects : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Allergies_Affects__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    }, 
    
    setAllergiesSpecialConsiderations : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Allergies_special_considerations__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setAllergiesSpecialConsiderations : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Allergies_special_considerations__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setAllergiesPets : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Allergies_Pets__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setAllergiesPetFreeRoom : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Allergies_Pet_free_room__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setAllergicHairlessorHypoallergenicPets : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Allergic_hairless_or_hypoallergenic_Pets__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setAsthma : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Asthma__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setAsthmaMedications : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Asthma_Medications__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setAsthmaAffectDailyLife : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Asthma_Affect_Daily_Life__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setAsthmaSpecialConsiderations : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Asthma_Special_Considerations__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setCeliacDisease : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Celiac_Disease__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setCeliacMedications : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Celiac_Medications__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setCeliacVisitDoctor : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Celiac_Visit_Doctor__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setCeliacLimitPhysicalActivities : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Celiac_Limit_Physical_Activities__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setCeliacConsumeFoodFromSamePlateUt : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Celiac_Consume_Food_From_Same_Plate_Ut__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setCeliacConsumeFoodFromSamePot : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Celiac_Consume_Food_From_Same_Pot__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setCeliacHospitalizationHistory : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Celiac_Hospitalization_History__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setCeliacHospitalization : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Celiac_Hospitalization__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setCeliacReaction : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Celiac_Reaction__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setDiabetes : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Diabetes__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setDiabetesNeedToVisitDoctor : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Diabetes_Need_to_visit_doctor__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setDiabetesNeedToVisitDoctor : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Diabetes_Need_to_visit_doctor__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    setDentalCareOnProgram : function (component, event, helper){
        var healthInfoObj = component.get("v.healthInfoObj");
        healthInfoObj.Dental_care_on_program__c = event.currentTarget.id;
        component.set("v.healthInfoObj",healthInfoObj); 
    },
    
    backToList : function(component, event, helper) {
        component.set("v.parentToggle",false);
    },
    
    doSaveWithoutValidation : function(component, event, helper) {
        component.set("v.Spinner",true);
        helper.onSave(component, event, true);
    },
    
    doSave : function(component, event, helper) {
        component.set("v.Spinner",true);
        if(helper.onValidateForm(component, event)){
            helper.onSave(component, event, false);
        }
    }
    
        
})