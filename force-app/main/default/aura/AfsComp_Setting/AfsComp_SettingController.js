({
	doInit : function(component, event, helper) {
        component.set("v.Spinner","true"); 
        var configObj = component.get("v.configWrapper");
        
		helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressHeader__c ,component);
        component.set("v.AddressHeader",component.get("v.LabelTempVal"));         
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressLabel__c  ,component);
        component.set("v.AddressLabel",component.get("v.LabelTempVal")); 
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressHelpTxt__c ,component);
        component.set("v.AddressLabelHelpTxt",component.get("v.LabelTempVal")); 
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressLine1PlaceHold__c ,component);
        component.set("v.AddressLine1PlaceHold",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressLine2PlaceHold__c,component);
        component.set("v.AddressLine2PlaceHold",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressCityPlaceHold__c,component);
        component.set("v.AddressCityPlaceHold",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressStatePlaceHold__c,component);
        component.set("v.AddressStatePlaceHold",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressZipPlaceHold__c,component);
        component.set("v.AddressZipPlaceHold",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressLine1PlaceHold__c ,component);
        component.set("v.AddressLine1PlaceHold",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressLine2PlaceHold__c,component);
        component.set("v.AddressLine2PlaceHold",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressCityPlaceHold__c,component);
        component.set("v.AddressCityPlaceHold",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressStatePlaceHold__c,component);
        component.set("v.AddressStatePlaceHold",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_AddressZipPlaceHold__c,component);
        component.set("v.AddressZipPlaceHold",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_MobileNumber__c,component);
        component.set("v.MobileNumber",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PhoneNumber__c,component);
        component.set("v.PhoneNumber",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_LegalCountry__c,component);
        component.set("v.LegalCountry",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_LegalCountryHelpTxt__c,component);
        component.set("v.LegalCountryHelpTxt",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_PersonalDetailHeader__c,component);
        component.set("v.PersonalDetailHeader",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_DOB__c,component);
        component.set("v.DOB",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_DOBHelpTxt__c,component);
        component.set("v.DOBHelpTxt",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_Gender__c,component);
        component.set("v.Gender",component.get("v.LabelTempVal"));        
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_GenderHelpTxt__c,component);
        component.set("v.GenderHelpTxt",component.get("v.LabelTempVal"));       
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_Nationality__c,component);
        component.set("v.Nationality",component.get("v.LabelTempVal"));   
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_NationalityHelpTxt__c ,component);
        component.set("v.NationalityHelpTxt",component.get("v.LabelTempVal"));    
        
        helper.getDynamicCustomLabelVal(configObj.LoginComp_Txt_Name__c,component);
        component.set("v.Name",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.LoginComp_Txt_FirstName__c,component);
        component.set("v.FirstName",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.LoginComp_Txt_LastName__c,component);
        component.set("v.LastName",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.LoginComp_Txt_Email__c,component);
        component.set("v.Email",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.LoginComp_Txt_EmailPlaceHolder__c,component);
        component.set("v.EmailPlaceHolder",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.LoginComp_Txt_HearAboutUs__c,component);
        component.set("v.HearAboutUs",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.LoginComp_Txt_HearAboutUsHelpText__c,component);
        component.set("v.HearAboutUsHelpTxt",component.get("v.LabelTempVal"));
        helper.getDynamicCustomLabelVal(configObj.PersonalDetail_Txt_SaveAndContinue__c,component);
        component.set("v.SaveAndContinue",component.get("v.LabelTempVal"));
        helper.fetchPickListVal(component, 'Country_of_Legal_Residence__c,NationalityCitizenship__c ,Gender__c,How_did_you_hear_about_AFS__c');
        
         var ContactInfo = component.get("v.ContactInfo");
        var addressLines = [];
        if(ContactInfo.MailingStreet != undefined){
            addressLines = ContactInfo.MailingStreet.split(";");
        }
        if(addressLines.length > 1){
            component.set("v.addressLine1",addressLines[0]);
            component.set("v.addressLine2",addressLines[1]);
        }else{
            component.set("v.addressLine1",ContactInfo.MailingStreet);
        }
        
	},
    
    showPassWordDialog : function (component,event,helper) {
        component.set("v.changePasswordRequest","true");
    },
    
    doPasswordChange : function (component,event,helper) {
        component.set("v.Spinner","true"); 
        helper.onPasswordChange(component,event);
    },
    
    doSaveRecord : function (component,event,helper) {
        var ContactInfo = component.get("v.ContactInfo");
        var configWrapper = component.get("v.configWrapper");
        var messageTemplateStr;
        
        if(ContactInfo.FirstName === undefined || ContactInfo.FirstName == ""){
            messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_FirstNameRequire");
        }
        
        if(ContactInfo.LastName === undefined || ContactInfo.LastName == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_LastNameRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_LastNameRequire");
            }
        }
        
        if(ContactInfo.Email === undefined || ContactInfo.Email == ""){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_EmailRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_EmailRequire");
            }
        }else{
            var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if(!re.test(ContactInfo.Email)){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_EmailNoValid");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_EmailNoValid");
                }
            }
        }
        
        var none = $A.get("$Label.c.AfsLbl_General_None");
        if(ContactInfo.How_did_you_hear_about_AFS__c === undefined || ContactInfo.How_did_you_hear_about_AFS__c == none){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_HearAboutUsRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_HearAboutUsRequire");
            }
        }
        
        if(ContactInfo.Terms_Service_Agreement__c == false){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_AgreeTermsRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_AgreeTermsRequire");
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_StreetAddress__c == 'Required'){
        	if((component.get("v.addressLine1") === undefined || component.get("v.addressLine1") == "")){
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_AddressRequire");
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_Apartment__c == 'Required'){
        	if((component.get("v.addressLine2") === undefined || component.get("v.addressLine2") == "")){
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_AddressRequire");
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_AddressCity__c == 'Required'){
            if(ContactInfo.MailingCity === undefined || ContactInfo.MailingCity == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_CityRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_CityRequire");
                }
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_AddressState__c == 'Required'){
            if(ContactInfo.MailingState === undefined || ContactInfo.MailingState == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_StateRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_StateRequire");
                }
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_PinCode__c == 'Required'){
            if(ContactInfo.MailingPostalCode === undefined || ContactInfo.MailingPostalCode == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_PostalCodeRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_PostalCodeRequire");
                }
            }
        }
        
        
        if(configWrapper.PersonalDetail_Fld_MobileNumber__c == 'Required'){
            if(ContactInfo.MobilePhone === undefined || ContactInfo.MobilePhone == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_MobileNumberRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_MobileNumberRequire");
                }
            }
        }
        
        if(ContactInfo.MobilePhone === 'invalid') {
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_MobileNumberInvalid");;
            }
        }
        
        if(configWrapper.PersonalDetail_Fld_PhoneNumber__c == 'Required'){
            if(ContactInfo.Phone === undefined || ContactInfo.Phone == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_PhoneRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_PhoneRequire");
                }
            }
        }
        
        if(ContactInfo.Phone === 'invalid'){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_PhoneNumberInvalid");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_PhoneNumberInvalid");
                }
            }
        
        if(configWrapper.PersonalDetail_Fld_LegalCountry__c == 'Required'){
            if(ContactInfo.Country_of_Legal_Residence__c === undefined ||
               ContactInfo.Country_of_Legal_Residence__c == "" ||
               ContactInfo.Country_of_Legal_Residence__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectCountry")){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_CountryLegalRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_CountryLegalRequire");
                }
            }
        }
        
        if(ContactInfo.Country_of_Legal_Residence__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectCountry")){
            ContactInfo.Country_of_Legal_Residence__c = "";
        }
        
        if(configWrapper.PersonalDetail_Fld_DOB__c == 'Required'){
            if(ContactInfo.Birthdate === undefined || ContactInfo.Birthdate == ""){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_DOBRequire");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_DOBRequire");
                }
            }
        }
        
        if(ContactInfo.Birthdate != undefined){
            var date = new Date();
            var month = date.getMonth()+1;
            var day = date.getDay();
            var year = date.getFullYear();
            
            var currentDate = new Date(year,month,day);
            if(new Date(ContactInfo.Birthdate) > currentDate){
                if(messageTemplateStr != undefined){
                    messageTemplateStr += "\n";
                    messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_DOBNoFuture");
                }else{
                    messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_DOBNoFuture");
                }
            }
        }
        
        if(ContactInfo.Gender__c === undefined || 
           ContactInfo.Gender__c == "" ||
           ContactInfo.Gender__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectGender")){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_GenderRequire");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_GenderRequire");
            }
        }
        
        if(ContactInfo.NationalityCitizenship__c === undefined ||
           ContactInfo.NationalityCitizenship__c == "" ||
           ContactInfo.NationalityCitizenship__c == $A.get("$Label.c.AfsLbl_YourApplication_SelectNationality")){
            if(messageTemplateStr != undefined){
                messageTemplateStr += "\n";
                messageTemplateStr += $A.get("$Label.c.AfsLbl_Error_Nationality");
            }else{
                messageTemplateStr = $A.get("$Label.c.AfsLbl_Error_Nationality");
            }
        }
        
        if(messageTemplateStr != undefined){
            var toastEvent = $A.get("e.force:showToast");
            var messageTemplate = messageTemplateStr;
            toastEvent.setParams({ 
                type : "Error",
                duration : 5000,
                message: messageTemplate
            });
            toastEvent.fire();
            return;
        }
        component.set("v.Spinner","true"); 
        helper.onSaveRecord(component,event);
    },
    
    checkAgreeTerms : function(component, event, helper) {
		var ContactInfo = component.get("v.ContactInfo");
        if(ContactInfo.Terms_Service_Agreement__c){
            ContactInfo.Terms_Service_Agreement__c = false;
        }else{
            ContactInfo.Terms_Service_Agreement__c = true;
        }
        component.set("v.ContactInfo",ContactInfo);
	},
    
    checkKeepMeInformed : function(component, event, helper) {
        var ContactInfo = component.get("v.ContactInfo");
        if(ContactInfo.Keep_me_informed_Opt_in__c){
            ContactInfo.Keep_me_informed_Opt_in__c = false;
        }else{
            ContactInfo.Keep_me_informed_Opt_in__c = true;
        }
        component.set("v.ContactInfo",ContactInfo);
	}
})