declare module "@salesforce/apex/ScholarshipToProgramOfferController.getProgram_Offers" {
  export default function getProgram_Offers(param: {recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/ScholarshipToProgramOfferController.createRecords" {
  export default function createRecords(param: {strJSONIds: any, recordId: any}): Promise<any>;
}
