({
	onInit : function(component, event) {
		var action = component.get("c.getConfiguration");
       
        action.setCallback(this, function(response) {
            
            if (response.getState() == "SUCCESS") {
                var configWrapper = response.getReturnValue();
                var loginAfs = configWrapper.Community_URL__c + "/s/afslogin";
                window.location.replace(loginAfs);

            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
	}
})