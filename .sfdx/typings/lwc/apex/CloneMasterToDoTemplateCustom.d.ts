declare module "@salesforce/apex/CloneMasterToDoTemplateCustom.newMasterToDoTemplate" {
  export default function newMasterToDoTemplate(param: {recordName: any, recordId: any}): Promise<any>;
}
declare module "@salesforce/apex/CloneMasterToDoTemplateCustom.getInfoRecords" {
  export default function getInfoRecords(param: {objectName: any, condition: any}): Promise<any>;
}
