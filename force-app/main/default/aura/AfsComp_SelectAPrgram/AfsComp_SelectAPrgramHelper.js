({
	onInit : function(component, event) {
        component.set("v.Spinner","true");
		var prId = "";
        if(window.location.search.indexOf("prId") > -1){
        	var sParameterName = window.location.search.substring(window.location.search.indexOf("prId")); 
        	if(sParameterName.indexOf("&") > - 1){
        		prId = sParameterName.substring(sParameterName.indexOf("=")+1,sParameterName.indexOf("&"));
        	}else{
        		prId = sParameterName.substring(sParameterName.indexOf("=")+1,sParameterName.length);
        	}        	
        }
        
        if(prId != "" && prId.length >= 15 ){
            component.set("v.isIdPresent","true");
        }
        var MapofPiclistValues = component.get("v.MapofPiclistValues");
		var action = component.get("c.getProgramWrapper");
        action.setParams({
             "mapStringyfied": MapofPiclistValues,
             "preSelectedProgram": prId,
             "isSearched" : false
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                if(response.getReturnValue().length > 0){
                    component.set("v.wrapperList",response.getReturnValue());
                    if(response.getReturnValue()[0].isSelectedRecord === true){
                        component.set("v.programObj",response.getReturnValue()[0]);
                    }
                    
                    for(var i =0 ;i < response.getReturnValue().length; i++){
                        if(response.getReturnValue()[i].isSavedRecord === true){
                            component.set("v.isSearchPressed","true");
                        }
                    }
                    component.set("v.atLeastOneSelectedProgram", true);
                }
                component.set("v.Spinner","false");
                component.set("v.isConfigLoaded","true");
                
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
            }
        });
        $A.enqueueAction(action);
    },
    
    doProgramSearch : function(component, event) {
		var prId = "";
        if(window.location.search.indexOf("prId") > -1){
        	var sParameterName = window.location.search.substring(window.location.search.indexOf("prId")); 
        	if(sParameterName.indexOf("&") > - 1){
        		prId = sParameterName.substring(sParameterName.indexOf("=")+1,sParameterName.indexOf("&"));
        	}else{
        		prId = sParameterName.substring(sParameterName.indexOf("=")+1,sParameterName.length);
        	}        	
        }
        var MapofPiclistValues = component.get("v.MapofPiclistValues");
        var counter = 0;
        for (var p in MapofPiclistValues) {
            if(MapofPiclistValues[p].length > 0){
                counter++;
            }
        }
        var action = component.get("c.getProgramWrapper");
        var isSerached = component.get("v.isSearchPressed");
        component.set("v.noSearchFilter","false");
        action.setParams({
            "mapStringyfied": MapofPiclistValues,
            "preSelectedProgram": prId,
            "isSearched" : true
        });
        
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                component.set("v.wrapperList",response.getReturnValue());
                
                if(response.getReturnValue().length > 0){
                    if(response.getReturnValue()[0].isSelectedRecord === true){
                        component.set("v.programObj",response.getReturnValue()[0]);
                    }
                }
                component.set("v.Spinner","false");
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = $A.get("$Label.c.Error_in_processing_request_try_after_few_minutes");
                toastEvent.setParams({
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                
                component.set("v.Spinner","false");
            }
        });
        $A.enqueueAction(action);
        
        
		
    },
    
    getDestination : function(component, event) {
		component.set("v.Spinner","true");
		var action = component.get("c.getDestinations");
        action.setParams({
             "objObject": {sobjectType : 'Program_Offer__c'},
             "fld": "Destinations__c"
        });
        
        action.setCallback(this, function(response) {
   
            if (response.getState() == "SUCCESS") {                
                component.set("v.destinationWrapper", response.getReturnValue());                             
            }  
            component.set("v.Spinner","false");
         });
        $A.enqueueAction(action);
    },
    
    getContactName : function(component, event) {
		
		var action = component.get("c.getCurrentUserName");
        component.set("v.Spinner","true");
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var serverVal = response.getReturnValue();
                component.set("v.contactName", serverVal[0]);   
                    
                    var destinationWrapper = component.get("v.destinationWrapper");
                    for(var j = 0;j < destinationWrapper.length;j++){
                        if(destinationWrapper[j].Name == serverVal[1]){
                            destinationWrapper[j].isSelected = true;
                            component.set("v.addDestination1",destinationWrapper[j]);
                        }
                        
                        if(destinationWrapper[j].Name == serverVal[2]){
                            destinationWrapper[j].isSelected = true;
                            component.set("v.addDestination2",destinationWrapper[j]);
                        }
                        
                        if(destinationWrapper[j].Name == serverVal[3]){
                            destinationWrapper[j].isSelected = true;
                            component.set("v.addDestination3",destinationWrapper[j]);
                        }
                    }
                    component.set("v.destinationWrapper",destinationWrapper);
                    
                
                component.set("v.Spinner","false");
            }  
            
         });
        $A.enqueueAction(action);
    },
    
    doSaveAndContinueProgram : function(component, event,listToSave,listToDelete) {
		
		var action = component.get("c.saveAndContinueProgram");
		var applicationWrapper  = component.get("v.applicationWrapper");
        
        var addDestination1 = component.get("v.addDestination1");
        var addDestination2 = component.get("v.addDestination2");
        var addDestination3 = component.get("v.addDestination3");
        
        var combineVal = "" + (addDestination1 == null ? " " : addDestination1.Name) + ";" + (addDestination2 == null ? " " : addDestination2.Name) + ";" +(addDestination3 == null ? " " : addDestination3.Name);
        action.setParams({
            "offerIdsToDeleteInterest" : listToDelete,
            "offerIdsToSave" : listToSave,
             "additionalVal" : combineVal
        });
        //Manish
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") {
                var applicationWrapper  = component.get("v.applicationWrapper");
                if(JSON.stringify(applicationWrapper.applicationObj) == JSON.stringify({})){
                    applicationWrapper.applicationObj = response.getReturnValue();
                	component.set("v.applicationWrapper",applicationWrapper);
                }
				
                window.setTimeout(
                    $A.getCallback(function() {
                        var navigateWrapper = component.get("v.navigateWrapper");
                        navigateWrapper.isSelectProgram  = false;
                        navigateWrapper.isProgramDetail = true;
                        navigateWrapper.isProgramDetailSideBar = true;
                        component.set("v.navigateWrapper",navigateWrapper);
                    }), 2000
                );
				/*var navigateWrapper = component.get("v.navigateWrapper");
                navigateWrapper.isSelectProgram  = false;
                navigateWrapper.isProgramDetail = true;
                component.set("v.navigateWrapper",navigateWrapper);*/
            } else {
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = $A.get("$Label.c.Error_in_processing_request_try_after_few_minutes");
                toastEvent.setParams({
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                
                component.set("v.Spinner","false");
            } 
            
         });
        $A.enqueueAction(action);
    },
    
    getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    }
    
    
})