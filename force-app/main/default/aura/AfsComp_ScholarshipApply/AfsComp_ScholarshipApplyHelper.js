({
	onInit : function(component, event) {
        var ContactInfo = component.get("v.ContactInfo");
        var applicationWrapper = component.get("v.applicationWrapper");
		var action = component.get("c.getScholarshipWrapper");
        action.setParams({
            "acountId": ContactInfo.AccountId,
            "contactId": ContactInfo.Id,
            "appId": applicationWrapper.applicationObj.Id
        });
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                component.set("v.ScholarWrapperList",response.getReturnValue());
                component.set("v.Spinner",false);
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                component.set("v.Spinner",false);
            }  
            
        });
        $A.enqueueAction(action);
	},
    
    getDynamicCustomLabelVal :  function (labelStr,component){
        var labelReference = $A.getReference("$Label.c." + labelStr);
        component.set("v.LabelTempVal", labelReference);
    },
    
    doSaveData : function(component, event){
        var cmpEvent = component.getEvent("afscompnavigation");
        cmpEvent.setParam("navigateTo","3");
		var ScholarWrapperList = component.get("v.ScholarWrapperList");
        var ContactInfo = component.get("v.ContactInfo");
        var applicationWrapper = component.get("v.applicationWrapper");
        var objectList = [];
        for(var i=0;i<ScholarWrapperList.length;i++){
            if(ScholarWrapperList[i].isSelected == true){
                objectList.push(ScholarWrapperList[i].scholarShipObj);
            }
            
        }
		var action = component.get("c.saveScholarshipWrapper");
        
        action.setParams({
            "ScholarshipWrapperList": objectList,
            "contactId" : ContactInfo.Id,
            "applicationId" : applicationWrapper.applicationObj.Id
        });
        
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var configWrapper = component.get("v.configWrapper");
                var isGCCDisbaled;
                if(configWrapper.GCC_Available_Stages__c != undefined && configWrapper.GCC_Available_Stages__c != ''){
                    if(configWrapper.GCC_Available_Stages__c.indexOf(applicationWrapper.applicationObj.Status__c) != -1){
                        isGCCDisbaled = false;
                    }else{
                        isGCCDisbaled = true;
                    }
                }else{
                    isGCCDisbaled = true;
                }               
                if(!isGCCDisbaled){
                    cmpEvent.fire();
                }else{
                    window.scrollTo(0,0);
                } 	
                
                component.set("v.Spinner","false"); 
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                
                toastEvent.fire();
                component.set("v.Spinner",false);
            }  
            
        });
        $A.enqueueAction(action);
    }
    
    
})