declare module "@salesforce/apex/Afs_ParentGuardianCtrl.getParentGuardianList" {
  export default function getParentGuardianList(param: {contactID: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_ParentGuardianCtrl.deleteParentGuardian" {
  export default function deleteParentGuardian(param: {reationId: any}): Promise<any>;
}
