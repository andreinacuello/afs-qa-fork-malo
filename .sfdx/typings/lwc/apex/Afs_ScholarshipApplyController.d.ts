declare module "@salesforce/apex/Afs_ScholarshipApplyController.getScholarshipWrapper" {
  export default function getScholarshipWrapper(param: {acountId: any, contactId: any, appId: any}): Promise<any>;
}
declare module "@salesforce/apex/Afs_ScholarshipApplyController.saveScholarshipWrapper" {
  export default function saveScholarshipWrapper(param: {ScholarshipWrapperList: any, contactId: any, applicationId: any}): Promise<any>;
}
