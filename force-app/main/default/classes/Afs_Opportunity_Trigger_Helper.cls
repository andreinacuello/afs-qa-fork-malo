public without sharing class Afs_Opportunity_Trigger_Helper {
    public static List<opportunity> opportunityNewList = new List<opportunity>();
    public static List<opportunity> opportunityOldList = new List<opportunity>();
    public static Map<Id,opportunity> opportunityNewMap = new Map<Id,opportunity>();
    public static  Map<Id,opportunity> opportunityOldMap = new Map<Id,opportunity>();
    public static boolean allowOppUpdate = false;
    public static void allowOpportunityUpdate(){
        if(!allowOppUpdate ){
            for(opportunity opp : opportunityNewList)
            	opp.addError(System.Label.AfsLbl_Opportunity_Update_Error);
        	}
    }
    public static void allowOpportunityDelete(){
         if(!allowOppUpdate){
            for(opportunity opp : opportunityOldList)
            	opp.addError(System.Label.AfsLbl_Opportunity_Delete_Error);
       } 
    }
        
}