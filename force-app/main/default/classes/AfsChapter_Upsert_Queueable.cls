/* 
	Name: GL_SF_Integrations_Daily
	CreatedBy: Facundo Nicora
	Description: Created for contains the logic for get Afs Chapters by integrations with GL.
*/
public class AfsChapter_Upsert_Queueable implements Queueable,Database.AllowsCallouts{    
    public Integer pageIndex;    
    
    public AfsChapter_Upsert_Queueable(Integer pi){
        this.pageIndex = pi;
    }
    
    public void execute(QueueableContext context) {
        try{
            //Get Page number pageIndex
            RemoteSiteManager rsm = new RemoteSiteManager();
            rsm.createAfs_Chapters(String.ValueOf(pageIndex), '300');
            system.debug('pageIndex: ' + pageIndex);
            system.debug('Integer.ValueOf(rsm.remoteSiteModel.response[0].pagesTotal: ' + Integer.ValueOf(rsm.remoteSiteModel.response[0].pagesTotal));
            //If there are more pages get the next
            if(pageIndex < Integer.ValueOf(rsm.remoteSiteModel.response[0].pagesTotal)){
                pageIndex++;
                AfsChapter_Upsert_Queueable job = new AfsChapter_Upsert_Queueable(pageIndex);
                ID jobID = System.enqueueJob(job);
            }
        }catch(Exception exc){
            system.debug(exc.getMessage());
        }
    }
}