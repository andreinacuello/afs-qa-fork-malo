@isTest
public class ContactTriggerHandler_Test { 
    @testSetup
    public static void setup_method(){
        Util_Test.create_AFS_Triggers_Switch('Afs_Trigger',true,true);
    }
    
    public static testMethod void calculateScore_Test(){
        List<Scoring_Settings__c> lstSS = new List<Scoring_Settings__c>();
        Scoring_Settings__c ss = Util_Test.createScoringSettingsRecord('Test SS1', 'Contact', 'Do_you_smoke__c', 'Value', 'Yes',null,null, -20);
        lstSS.add(ss);
        ss = Util_Test.createScoringSettingsRecord('Test SS2', 'Application__c', 'Name', 'Blank', null, null,null, -5);
        lstSS.add(ss);
        Insert lstSS;
        
        Account newAccount1 = Util_Test.create_AFS_Partner2('Test account 1'); //It's already insert
        
        Contact con = Util_Test.create_Conact(newAccount1,'Applicant','Test newContact');     

        Application__c app = Util_Test.createApplication(con,null);
       	Insert app;
        
        Test.startTest();
		con.Do_you_smoke__c = 'Yes';
        DataBase.Update(con);
        Test.stopTest();
        
        app = [SELECT Score__c FROM Application__c Limit 1];
        System.assertEquals(80, app.Score__c);

    }
}