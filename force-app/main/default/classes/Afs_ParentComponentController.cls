/** 
 * 	Description : Controller class for getting 
 * 			      config wrapper for parent wrapper
 */
global without sharing class Afs_ParentComponentController {

    @AuraEnabled
    global static AFS_Community_Configuration__c getConfiguration (){
        AFS_Community_Configuration__c configObj = new AFS_Community_Configuration__c();
        String communityId =  Network.getNetworkId(); //'0DB7F000000wrPG';
        String queryStr = 'SELECT id, Name, ';
        for(String fieldAPIName : Afs_UtilityApex.getFields('AFS_Community_Configuration__c',true)){
            queryStr += fieldAPIName + ' ,';
        }
        queryStr = queryStr.removeEnd(',');
        if(!Test.isRunningTest()){
            queryStr += ' FROM AFS_Community_Configuration__c WHERE Unique_Id__c =: communityId';
        }else{
            queryStr += ' FROM AFS_Community_Configuration__c';
            communityId = 'ABC';
        }
        
        if(String.isNotBlank(communityId)){
            
            list<AFS_Community_Configuration__c>  listCommunityConfig =  Database.query(queryStr);
            if(listCommunityConfig != null && !listCommunityConfig.isEmpty()){
                configObj = listCommunityConfig[0];
            }    
        }
        
        return configObj;
    }
    
	@AuraEnabled
    global static loginWrapper getLoginWrapper(){
        loginWrapper loginWrapperObj = new loginWrapper();
        return loginWrapperObj;
    }
    
    @AuraEnabled
    global static void updateUserLanguage(String selectedLanguage){
        list<String> languageList = new list<String>();
        User userObj = new User(id=UserInfo.getUserId());
        if(String.isNotBlank(selectedLanguage)){
	    	//Apply seleceted language
	    	Schema.DescribeFieldResult fieldResult = AFS_Community_Configuration__c.Afs_CommunityLanguages__c.getDescribe();
			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			for( Schema.PicklistEntry pickListVal : ple){
				if(selectedLanguage.equalsIgnoreCase(pickListVal.getLabel())){
					userObj.LanguageLocaleKey = pickListVal.getValue();
                    break;
				}
			} 
			update userObj;
			   
        }
        
    }
    
    @AuraEnabled
    global static String doTranslate(String selectedLanguage, String customLabelStr, Boolean isOnLoad){
    	List<String> lstCustomLabels = new List<String>();
    	if(String.isNotBlank(customLabelStr)){
    		lstCustomLabels = (List<String>)JSON.deserialize(customLabelStr, List<String>.class);
    	}
    	List<customLabelWrapper> lstCustomLabelWrapper = new List<customLabelWrapper>();
    	if(String.isNotBlank(selectedLanguage)){
    		String selectedLanguageCode = (isOnLoad ? selectedLanguage : '');
            if(!isOnLoad){
               Schema.DescribeFieldResult fieldResult = AFS_Community_Configuration__c.Afs_CommunityLanguages__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for(Schema.PicklistEntry pickListVal : ple){
                    if(selectedLanguage.equalsIgnoreCase(pickListVal.getLabel())){
                        selectedLanguageCode = pickListVal.getValue();
                        break;
                    }
                } 
            }
	    	
			if(lstCustomLabels != null && lstCustomLabels.size() > 0){
				for(String lbl : lstCustomLabels){
					String lblVal = '';
					if(String.isNotBlank(selectedLanguageCode)){
						Afs_LabelTranslator ltCls = new Afs_LabelTranslator();
						lblVal = ltCls.translate(lbl, selectedLanguageCode);
					}
					
					//If there is no translation added in custom label
					//then we will show value in English language
					if(String.isBlank(lblVal)){
						Afs_LabelTranslator ltCls = new Afs_LabelTranslator();
						lblVal = ltCls.translate(lbl, 'en');
					}
					lstCustomLabelWrapper.add(new customLabelWrapper(lbl, lblVal));
				}
			}
        }
        return JSON.serialize(lstCustomLabelWrapper);
    }
    
    @AuraEnabled
    global static List<String> getLanguageList(String selectedLanguageCode){
        list<String> languageList = new list<String>();
        if(String.isNotBlank(selectedLanguageCode)){
            list<String> languageListSelected = selectedLanguageCode.contains(';') ? selectedLanguageCode.split(';') :
            									(selectedLanguageCode.contains(':') ? selectedLanguageCode.split(':') :
                                                 new list<String> ());
            if(!languageListSelected.isEmpty()){
                set<String> languageSetSelected = new set<String> ();
                languageSetSelected.addAll(languageListSelected);
                Schema.DescribeFieldResult fieldResult = AFS_Community_Configuration__c.Afs_CommunityLanguages__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for( Schema.PicklistEntry pickListVal : ple){
                    if(languageSetSelected.contains(pickListVal.getValue())){
                        languageList.add(pickListVal.getLabel());
                    }
                }
                
                languageList.sort();                                            
            }  
        }
        
        return languageList;
    }
    
    @AuraEnabled
    global static void userLogin(String userName, string password,String prId){
        try{
            if (String.isNotBlank(userName) && String.isNotBlank(password)) { 
                if (password != null && password.length() > 1) {
                    ApexPages.PageReference lgn = Site.login(userName, password, '');
                    //IMP: to maintain program name                
                    if(string.isNotBlank(prId)){
                        string communityHomeURL = getConfiguration().Community_URL__c + '/s/?prId=' + prId;
                        Afs_UtilityApex.logger(communityHomeURL);
                        lgn.getParameters().put('retURL',communityHomeURL);
                    }
                    
                    lgn.getParameters().put('prId',prId);
                    aura.redirect(lgn);
                }
            }
        }catch(Exception ex){
            throw new AuraHandledException( ex.getMessage());
        }
    }
    
    @AuraEnabled
    global static loginWrapper getNewLoginWrapper(){
        Contact con = Afs_ParentComponentController.getContactRecord();
        String lanCode = UserInfo.getLanguage();   
        loginWrapper loginWrapperObj = new loginWrapper();
		loginWrapperObj.FirstName = con.FirstName;
        loginWrapperObj.LastName = con.LastName;
        loginWrapperObj.MiddleName = con.MiddleName;
        loginWrapperObj.Email  = con.Email;
        loginWrapperObj.DOB = con.BirthDate;
        Schema.DescribeFieldResult fieldResult = AFS_Community_Configuration__c.Afs_CommunityLanguages__c.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			if(lanCode.equalsIgnoreCase(pickListVal.getValue())){
				loginWrapperObj.languageSelected = pickListVal.getLabel();
			}
		}  
        return loginWrapperObj;
    }
    
    @AuraEnabled
    global static Contact updateUserAndContact(Contact conObj,String languageSelected){
        try{
            // GET USER DETAILS 
            USER userObj = [SELECT id,ContactId,firstname,Lastname,Email,BirthDate__c,LanguageLocaleKey FROM USER WHERE id =: UserInfo.getUserId()];
            userObj.Firstname = conObj.FirstName;
            userObj.Lastname = conObj.LastName;
            userObj.Email = conObj.Email;
            userObj.BirthDate__c = conObj.Birthdate;
            //Apply seleceted language
            if(String.isNotBlank(languageSelected)){
		    	Schema.DescribeFieldResult fieldResult = AFS_Community_Configuration__c.Afs_CommunityLanguages__c.getDescribe();
				List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
				for( Schema.PicklistEntry pickListVal : ple){
					if(languageSelected.equalsIgnoreCase(pickListVal.getLabel())){
						userObj.LanguageLocaleKey = pickListVal.getValue();
                        break;
					}
				}     
	        }
            update userObj;
            
            //UPDATE CONTACT DETAILS
            update conObj;

        }catch(Exception e){
            throw new AuraHandledException( e.getMessage());
        }
        return conObj;
    }
    
    @AuraEnabled
    global static String generateSession(String wrapObj, String prId, String languageSelected, Boolean islanguageSelectedByUser){
        String returnUrl = '';
        try{
			loginWrapper wraperObj = (loginWrapper)JSON.deserialize(wrapObj , loginWrapper.class);
            String communityId = Network.getNetworkId();
            List<NetworkSelfRegistration> accs = [SELECT AccountID 
                                                  FROM NetworkSelfRegistration 
                                                  WHERE networkid = :communityId];
            List<Network> recCommunity = [SELECT SelfRegProfileId, Name,UrlPathPrefix 
                                          FROM Network WHERE Id = :communityId];
            
            //check if user exists or not
            User userObj; 
            List<User> usrs = [SELECT Id FROM User 
                            WHERE Email =: wraperObj.Email 
                            AND ProfileId = :recCommunity[0].SelfRegProfileId 
                            LIMIT 1];
            if(string.isNotBlank(communityId) && usrs.size() > 0){
                //userObj = usrs[0];  
                   
                throw new customException(System.label.AfsLbl_Error_UserAlreadyExist);
            }else{
                //create new user inmemory
                userObj = AfsUserManagementController.getInMemoryUserAccount(wraperObj.Email,wraperObj.FirstName,wraperObj.LastName,communityId, wraperObj);            	                
            }
            
            //set wrapper data
            userObj.BirthDate__c = wraperObj.DOB;
            
        	islanguageSelectedByUser = (islanguageSelectedByUser == null) ? false : islanguageSelectedByUser;
            //Apply selected language
            if(islanguageSelectedByUser){
               Schema.DescribeFieldResult fieldResult = AFS_Community_Configuration__c.Afs_CommunityLanguages__c.getDescribe();
                List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
                for(Schema.PicklistEntry pickListVal : ple){
                    if(languageSelected.equalsIgnoreCase(pickListVal.getLabel())){
                        userObj.LanguageLocaleKey = pickListVal.getValue();
                        break;
                    }
                }
            }else{
            	userObj.LanguageLocaleKey = languageSelected;
            }
	        
            Contact conObj = [SELECT id,AccountId FROM Contact WHERE id =: userObj.contactId];
            //create and login user
            PageReference pg = AfsUserManagementController.createUserAndLogin(userObj, wraperObj.Password, conObj.AccountId);
            Afs_UtilityApex.logger(pg.getParameters());
            
            //IMP: to maintain program name                
            if(string.isNotBlank(prId)){
                string communityHomeURL = getConfiguration().Community_URL__c + '/s/?prId=' + prId;
                Afs_UtilityApex.logger(communityHomeURL);
                pg.getParameters().put('retURL',communityHomeURL);
            }
            
            aura.redirect(pg);
        }catch(Exception e){
            String errMsg = e.getMessage();
            if(e.getMessage().equalsIgnoreCase(System.label.AfsLbl_Error_PasswordSystem)){
                errMsg += ' \n '+System.label.AfsLbl_Error_PasswordAddOn;
            }
            throw new AuraHandledException( errMsg );
        }
        return null;
    }
    
    
    @AuraEnabled
    global static navigateWrapper getNavigateWrapper(){
        navigateWrapper navigateWrapperObj = new navigateWrapper();
        return navigateWrapperObj;
    }
    
    @AuraEnabled
    global static String resetPassword(String userNameStr){
        String strMessage = System.label.AfsLbl_ForgetPass_PassworResetLinkSent;
        try{
            if(String.isNotBlank(userNameStr)){
                list<User> userList = [SELECT id FROM User WHERE Username =: userNameStr]; 
                if(!userList.isEmpty()){
                    System.resetPassword(userList[0].id,true);
                }else{
                    throw new customException(System.label.AfsLbl_ForgetPass_NoUserExist);
                }
            }else{
                throw new customException(System.label.AfsLbl_ForgotPass_UsernameRequire);
            }
        }catch(Exception e){
            throw new AuraHandledException(e.getMessage());
        }
        return strMessage;
    }
    
    @AuraEnabled
    global static Contact getContactRecord() {
        Contact contactObj = new Contact();
        String currentContactId = [SELECT id,ContactId FROM USER WHERE id =: UserInfo.getUserId()].ContactId;
        String queryStr = 'SELECT id,';
        for(String fieldAPIName : Afs_UtilityApex.getFields('Contact',false)){
            if(!fieldAPIName.equalsIgnoreCase('id')){
                queryStr += fieldAPIName + ' ,';
            }
        }
        queryStr = queryStr.removeEnd(',');
        queryStr += ' FROM Contact WHERE id =: currentContactId';
        if(String.isNotBlank(currentContactId)){
            
            list<Contact>  listContact =  Database.query(queryStr);
            if(listContact != null && !listContact.isEmpty()){
                contactObj = listContact[0];
            }    
        }
        return contactObj; 
        
    }
    
    @AuraEnabled
    global static ApplicationWrapper getApplicationRecord(){
        String currentContactId = [SELECT id,ContactId FROM USER WHERE id =: UserInfo.getUserId()].ContactId;
        ApplicationWrapper applicationWrapper;
        String queryStr = 'SELECT id,Name,OwnerId,Program_Offer__r.Currency__c, ';
        for(String fieldAPIName : Afs_UtilityApex.getFields('Application__c',true)){
            queryStr += fieldAPIName + ' ,';
        }
        queryStr = queryStr.removeEnd(',');
        queryStr += ' FROM Application__c WHERE Applicant__c =: currentContactId';
        list<Application__c>  listApplication =  Database.query(queryStr);
        system.debug(listApplication);
        if(listApplication != null && !listApplication.isEmpty()){
            applicationWrapper = new ApplicationWrapper(listApplication[0],'');
        }else{
            applicationWrapper = new ApplicationWrapper(new Application__c(),'');
        }    
        return applicationWrapper;
    }
    
    @AuraEnabled
    global static String getDP(){
        String currentContactId = [SELECT id,ContactId FROM USER WHERE id =: UserInfo.getUserId()].ContactId;
        Set<id> contentDocumentIdSet = new Set<Id> ();
        for(ContentDocumentLink cdlink : [SELECT id,ContentDocumentId,LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId =: currentContactId]){
            contentDocumentIdSet.add(cdlink.ContentDocumentId);
        }
        
        list<ContentVersion> cvList = [SELECT id,VersionData FROM ContentVersion WHERE ContentDocumentId IN: contentDocumentIdSet AND Afs_ProfilePhoto__c = true];
        String url = (cvList.isEmpty() ?  ''  : 'data:image/jpeg;base64,' + EncodingUtil.base64Encode(cvList[0].VersionData));
        return url;
    }
    
    
    global class ApplicationWrapper {
        
        @AuraEnabled
        global Application__c applicationObj {get;set;} 
        @AuraEnabled
        global list<picklistWrapper> WhatAreYouLookingFor {get;set;}
        @AuraEnabled
        global String currencyStr {get;set;}
        @AuraEnabled
        global list<picklistWrapper> HowWouldFriendsFamilyDescribeYou {get;set;}
        @AuraEnabled
        global String photoUrlOwner {get;set;}
        
        global ApplicationWrapper(Application__c obj,String photoUrl) {
            WhatAreYouLookingFor = new list<picklistWrapper> ();
            HowWouldFriendsFamilyDescribeYou = new list<picklistWrapper> ();
            photoUrlOwner = photoUrl;
            if(obj != null && obj.Program_Offer__c != null){
                currencyStr = obj.Program_Offer__r.Currency__c;
            }
            
            map<String,String> picklistLblVal = new map<String,String> ();
            picklistLblVal = Afs_UtilityApex.getPicklistValMap('Application__c','What_are_you_looking_for__c');
            for(String fieldValue : picklistLblVal.keySet()){
                picklistWrapper picklistWrapperObj;
                if(String.isNotBlank(obj.What_are_you_looking_for__c) && obj.What_are_you_looking_for__c.indexOf(picklistLblVal.get(fieldValue)) > -1){
                    picklistWrapperObj = new picklistWrapper(fieldValue,true,picklistLblVal.get(fieldValue));
                }else{
                    picklistWrapperObj = new picklistWrapper(fieldValue,false,picklistLblVal.get(fieldValue));
                } 
                WhatAreYouLookingFor.add(picklistWrapperObj);
            }
            
            picklistLblVal = Afs_UtilityApex.getPicklistValMap('Application__c','How_would_friends_family_describe_you__c');
            for(String fieldValue : picklistLblVal.keyset()){
                picklistWrapper picklistWrapperObj;
                if(String.isNotBlank(obj.How_would_friends_family_describe_you__c) && obj.How_would_friends_family_describe_you__c.indexOf(picklistLblVal.get(fieldValue)) > -1){
                    picklistWrapperObj = new picklistWrapper(fieldValue,true,picklistLblVal.get(fieldValue));
                }else{
                    picklistWrapperObj = new picklistWrapper(fieldValue,false,picklistLblVal.get(fieldValue));
                } 
                HowWouldFriendsFamilyDescribeYou.add(picklistWrapperObj);
            }
            applicationObj = obj;
        }
    }
    
    global class picklistWrapper {
        @AuraEnabled
        global Boolean isSelected {get;set;}
        @AuraEnabled
        global String fieldVal {get;set;}
        @AuraEnabled
        global String fieldLabel {get;set;}
        
        global picklistWrapper(String lbl, boolean flag,String val){
            fieldVal = val;
            isSelected = flag;
            fieldLabel = lbl;
        }
    }
    
    global class navigateWrapper{
        
        @AuraEnabled
        global Boolean isSelectProgram {get;set;}
        @AuraEnabled
        global Boolean isSelectProgramSideBar {get;set;}
        @AuraEnabled
        global Boolean isProgramDetail {get;set;}
        @AuraEnabled
        global Boolean isProgramDetailSideBar {get;set;}
        @AuraEnabled
        global Boolean isAboutYou {get;set;}
        @AuraEnabled
        global Boolean isAboutYouSideBar {get;set;}
        @AuraEnabled
        global Boolean isPortalSubmissionConfirmation {get;set;}
        @AuraEnabled
        global Boolean isPortalSubmissionConfirmationSideBar {get;set;}
        @AuraEnabled
        global Boolean isPreSelected {get;set;}
        @AuraEnabled
        global Boolean isPreSelectedSideBar {get;set;}
        @AuraEnabled
        global Boolean isWaitlisted {get;set;}
        @AuraEnabled
        global Boolean isAccepted {get;set;}
        @AuraEnabled
        global Boolean isAcceptedSideBar {get;set;}
        
        global navigateWrapper(){
            isSelectProgram = false;
            isSelectProgramSideBar = false;
            isProgramDetail = false;
            isProgramDetailSideBar = false;
            isAboutYou = false;
            isAboutYouSideBar = false;
            isPortalSubmissionConfirmation =false;
            isPortalSubmissionConfirmationSideBar = false;
            isPreSelected = false;
            isPreSelectedSideBar = false;
            isAccepted = false;
            isAcceptedSideBar = false;
        } 
    }
    
    global class loginWrapper {
        @AuraEnabled
        global String FirstName {get;set;}
        @AuraEnabled
        global String LastName {get;set;}
        @AuraEnabled
        global String MiddleName {get;set;}
        @AuraEnabled
        global String Email {get;set;}
        @AuraEnabled
        global String Password {get;set;}
        @AuraEnabled
        global Date DOB {get;set;}
        @AuraEnabled
        global String ZipCode {get;set;}
        @AuraEnabled
        global String MobileNumber {get;set;}
        @AuraEnabled
        global String PromoCode {get;set;}
        @AuraEnabled
        global String HearAboutUs {get;set;}
        @AuraEnabled
        global Boolean AgreeTerms {get;set;}
        @AuraEnabled
        global Boolean KeepMeInformed {get;set;}
        @AuraEnabled
        global list<Afs_UtilityApex.PicklistWrapperUtil> aboutUsPicklist {get;set;}
        @AuraEnabled
        global String languageSelected {get;set;}
        
        
        global loginWrapper() {
            FirstName = '';
            LastName = '';
            MiddleName = '';
            Email = '';
            Password = '';
            MobileNumber = '';
            ZipCode = '';
            PromoCode = '';
            HearAboutUs = '';
            languageSelected = '';
            AgreeTerms = false;
            KeepMeInformed = false;
            aboutUsPicklist = new list<Afs_UtilityApex.PicklistWrapperUtil>();
            aboutUsPicklist.addAll(Afs_UtilityApex.getPicklistWrapper('Contact','How_did_you_hear_about_AFS__c'));
            
        }
    } 
    
    global class customLabelWrapper {
        
        @AuraEnabled
        global String customLabelName {get;set;}
        @AuraEnabled
        global String customLabelValue {get;set;}
        
        global customLabelWrapper(String customLabelName, String customLabelValue) {
        	this.customLabelName = customLabelName;
        	this.customLabelValue = customLabelValue;
        }
    }
    
    global class customException extends Exception {}
}