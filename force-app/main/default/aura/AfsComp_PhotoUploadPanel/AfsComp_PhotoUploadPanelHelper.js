({
	onInit :  function(cmp, event) {
        var action = cmp.get("c.getUploadedPhoto");
        
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                if(response.getReturnValue().length > 0){
                    for(var i = 0; i<8;i++){
                        if(i == 0){
                            if(response.getReturnValue()[i] != undefined){
                               cmp.set("v.documentUrl1",response.getReturnValue()[i]);
                               cmp.set("v.showDP1","true"); 
                               cmp.set("v.CDLId1", response.getReturnValue()[i+1]);
                            }
                        }else if(i == 2){
                            if(response.getReturnValue()[i] != undefined){
                                cmp.set("v.documentUrl2",response.getReturnValue()[i]);
                    			cmp.set("v.showDP2","true");
                                cmp.set("v.CDLId2", response.getReturnValue()[i+1]);
                            }
                        }else if(i == 4){
                            if(response.getReturnValue()[i] != undefined){
                                cmp.set("v.documentUrl3",response.getReturnValue()[i]);
                    			cmp.set("v.showDP3","true");
                                cmp.set("v.CDLId3", response.getReturnValue()[i+1]);
                            }
                        }else if(i == 6){
                            if(response.getReturnValue()[i] != undefined){
                                cmp.set("v.documentUrl4",response.getReturnValue()[i]);
                    			cmp.set("v.showDP4","true");
                                cmp.set("v.CDLId4", response.getReturnValue()[i+1]);
                            }
                        }
                    }
                    
                }
                
                if(cmp.get("v.showDP4") == "true" || cmp.get("v.showDP4") == true){
                    cmp.set("v.allFilled", "true");
                }else{
                    cmp.set("v.allFilled", "false");
                }
                cmp.set("v.Spinner", "false");
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                cmp.set("v.Spinner", "false");
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    getBase64String : function(cmp, event, documentId) {
		
        var action = cmp.get("c.getEncodedStringList");
        action.setParams({
            "fileId": documentId,
            "isProfilePhoto": false
        });
        
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                var documentTo = cmp.get("v.fileUploadTo");
                if(documentTo == "documentUrl1"){
                    cmp.set("v.documentUrl1",response.getReturnValue()[0]);
                	cmp.set("v.showDP1", "true");
                    cmp.set("v.CDLId1", response.getReturnValue()[1]);
                }else if(documentTo == "documentUrl2"){
                    cmp.set("v.documentUrl2",response.getReturnValue()[0]);
                    cmp.set("v.showDP2", "true");
                    cmp.set("v.CDLId2", response.getReturnValue()[1]);
                }else if(documentTo == "documentUrl3"){
                    cmp.set("v.documentUrl3",response.getReturnValue()[0]);
                    cmp.set("v.showDP3", "true");
                    cmp.set("v.CDLId3", response.getReturnValue()[1]);
                }else{
                    cmp.set("v.documentUrl4",response.getReturnValue()[0]);
                    cmp.set("v.showDP4", "true");
                    cmp.set("v.CDLId4", response.getReturnValue()[1]);
                }
                if(response.getReturnValue().length > 2){
                    for(var i = 0; i<response.getReturnValue().length;i++){
                        
                        if(i == 0){
                            if(response.getReturnValue()[i] != undefined && (cmp.get("v.documentUrl1") == undefined || cmp.get("v.documentUrl1") == "")){
                               cmp.set("v.documentUrl1",response.getReturnValue()[i]);
                               cmp.set("v.showDP1","true"); 
                               cmp.set("v.CDLId1", response.getReturnValue()[i+1]);
                            }
                        }else if(i == 2){
                            if(response.getReturnValue()[i] != undefined && (cmp.get("v.documentUrl2") == undefined || cmp.get("v.documentUrl2") == "")){
                                cmp.set("v.documentUrl2",response.getReturnValue()[i]);
                    			cmp.set("v.showDP2","true");
                                cmp.set("v.CDLId2", response.getReturnValue()[i+1]);
                            }
                        }else if(i == 4){
                            if(response.getReturnValue()[i] != undefined && (cmp.get("v.documentUrl3") == undefined || cmp.get("v.documentUrl3") == "")){
                                cmp.set("v.documentUrl3",response.getReturnValue()[i]);
                    			cmp.set("v.showDP3","true");
                                cmp.set("v.CDLId3", response.getReturnValue()[i+1]);
                            }
                        }else if(i == 6){
                            if(response.getReturnValue()[i] != undefined && (cmp.get("v.documentUrl4") == undefined || cmp.get("v.documentUrl4") == "")){
                                cmp.set("v.documentUrl4",response.getReturnValue()[i]);
                    			cmp.set("v.showDP4","true");
                                cmp.set("v.CDLId4", response.getReturnValue()[i+1]);
                            }
                        }
                    }
                    
                }
                
                if((cmp.get("v.showDP1") == "true" || cmp.get("v.showDP1") == true) && (cmp.get("v.showDP2") == "true" || cmp.get("v.showDP2") == true) && (cmp.get("v.showDP3") == "true" || cmp.get("v.showDP3") == true) && (cmp.get("v.showDP4") == "true" || cmp.get("v.showDP4") == true)){
                    cmp.set("v.allFilled", "true");
                }else{
                    cmp.set("v.allFilled", "false");
                }
                cmp.set("v.isOpen","false");
                cmp.set("v.Spinner", "false");
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                cmp.set("v.Spinner", "false");
            }  
            
        });
        $A.enqueueAction(action);
    },
    
    removeProfilePhoto : function(cmp, event, CDLId) {
        
        var action = cmp.get("c.deleteCDL");
        action.setParams({
            "documentId": CDLId
        });
        
        action.setCallback(this, function(response) {          
            if (response.getState() == "SUCCESS") { 
                cmp.set("v.Spinner", "false");
            }else{
                var toastEvent = $A.get("e.force:showToast");
                var messageTemplate = response.getError()[0].message;
                toastEvent.setParams({ 
                    type : "error",
                    duration : 5000,
                    message: messageTemplate
                });
                toastEvent.fire();
                cmp.set("v.Spinner", "false");
            }  
            
        });
        $A.enqueueAction(action);
        
    }
})