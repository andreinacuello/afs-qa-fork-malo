public class ToDoItemTriggerHelper {
    /*Contains logic for after Insert event*/
    public static void afterInsert(Map<Id,To_Do_Item__c> mapNew){
        calculateOpportunityCheckboxs(mapNew,null,false,false);
        actualizarOppCheckbox(mapNew,null,false);
    }
    /*Contains logic for after Update event*/
    public static void afterUpdate(Map<Id,To_Do_Item__c> mapNew,Map<Id,To_Do_Item__c> mapOld){
        calculateOpportunityCheckboxs(mapNew,mapOld,true,false);
        actualizarOppCheckbox(mapNew,mapOld,false);
    }
    /*Contains logic for after Delete event*/
    public static void afterDelete(Map<Id,To_Do_Item__c> mapOld){
        calculateOpportunityCheckboxs(null,mapOld,false,true);
    }
    
    /*If all the To Do Items of the same stage are in the same status (In-Review or Completed) then mark the corresponding opportunity checkbox*/
    public static void calculateOpportunityCheckboxs(Map<Id,To_Do_Item__c> mapNew,Map<Id,To_Do_Item__c> mapOld,Boolean isUpdate, Boolean isDelete){
        List<To_Do_Item__c> lstToDoItemToFilter = !isDelete ? mapNew.values() : mapOld.values();
        List<To_Do_Item__c> lstToDoItemToProcess = new List<To_Do_Item__c>();
       	Set<Id> setApplicationIds = new Set<Id>();
        Map<Id,Opportunity> mapOpps = new  Map<Id,Opportunity>();
        
        /*Filter To Do Items*/
        for(To_Do_Item__c tdi : lstToDoItemToFilter){
            if(!isUpdate || tdi.Status__c != mapOld.get(tdi.Id).Status__c || tdi.Stage_of_portal__c	 != mapOld.get(tdi.Id).Stage_of_portal__c	){
                setApplicationIds.add(tdi.Application__c);
            }
        }
		
        /*Process*/
        for(Application__c app : [SELECT Id, Opportunity__r.Pre_application_in_review__c, Opportunity__r.Pre_selected_in_review__c, Opportunity__r.Accepted_in_review__c, Opportunity__r.Get_Ready_in_review__c, Opportunity__r.Extra_documentation_needed_in_review__c,
                                  Opportunity__r.Pre_application_Completed__c, Opportunity__r.Pre_selected_Completed__c, Opportunity__r.Accepted_Completed__c, Opportunity__r.Get_Ready_Completed__c, Opportunity__r.Extra_documentation_needed_Completed__c,
                                  (SELECT Status__c, Stage_of_portal__c FROM To_Do_List__r) FROM Application__c WHERE Id in :setApplicationIds]){
        	Map<String,Integer> mapStageCounterTotal = new Map<String,Integer>();
            Map<String,Integer> mapStageStatusCounter = new Map<String,Integer>();
            for(To_Do_Item__c tdi : app.To_Do_List__r){
            	String Key = '0';
                
                if(tdi.Stage_of_portal__c == '1. Stage: Pre-application'){
                    key = '1';
                }else if(tdi.Stage_of_portal__c == '2. Stage: Pre-selected'){
                    key = '2';
                }else if(tdi.Stage_of_portal__c == '3. Stage: Accepted'){
                    key = '3';
                }else if(tdi.Stage_of_portal__c == '4. Stage: Get Ready'){
                    key = '4';
                }else if(tdi.Stage_of_portal__c == '- Extra documentation needed'){
                    key = '5';
                }else{
                    continue;
                }

                
                /*Count Total To Do Item for stage*/
                if(!mapStageCounterTotal.containsKey(key)){
                    mapStageCounterTotal.put(key,0);
                }
                Integer aux = mapStageCounterTotal.get(key);
                mapStageCounterTotal.put(key,++aux);              
                
                if(tdi.Status__c == 'Completed'){
                   key += 'CO'; 
                }else if(tdi.Status__c == 'In Review'){
                   key += 'IR'; 
                }else{
                    continue;
                }
                
                /*Count Total To Do Item in the same status for stage*/
                if(!mapStageStatusCounter.containsKey(key)){
                    mapStageStatusCounter.put(key,0);
                }
                aux = mapStageStatusCounter.get(key);
                mapStageStatusCounter.put(key,++aux);
               
            }
            
            for(String key : mapStageStatusCounter.keySet()){
                String stage = key.substring(0,1);
                /*if all TDI on the same stage have the same status then mark checkbox*/
                if(mapStageCounterTotal.get(stage) == mapStageStatusCounter.get(key)){
                   	Opportunity opp;
                    if(!mapOpps.containsKey(app.Opportunity__c)){
                       opp = new Opportunity(Id=app.Opportunity__c);
                       mapOpps.put(opp.Id,opp);
                    }else{
                       opp = mapOpps.get(app.Opportunity__c); 
                    }                    
                    
                    if(!app.Opportunity__r.Pre_application_in_review__c && key == '1IR'){
                        opp.Pre_application_in_review__c = true;
                    }else if(!app.Opportunity__r.Pre_selected_in_review__c && key == '2IR'){
                        opp.Pre_selected_in_review__c = true;
                    }else if(!app.Opportunity__r.Accepted_in_review__c && key == '3IR'){
                        opp.Accepted_in_review__c = true;
                    }else if(!app.Opportunity__r.Get_Ready_in_review__c && key == '4IR'){
                        opp.Get_Ready_in_review__c = true;
                    }else if(!app.Opportunity__r.Extra_documentation_needed_in_review__c && key == '5IR'){
                        opp.Extra_documentation_needed_in_review__c = true;
                    }else if(!app.Opportunity__r.Pre_application_Completed__c && key == '1CO'){
                        opp.Pre_application_Completed__c = true;  
                    }else if(!app.Opportunity__r.Pre_selected_Completed__c && key == '2CO'){
                        opp.Pre_selected_Completed__c = true;
                    }else if(!app.Opportunity__r.Accepted_Completed__c && key == '3CO'){
                        opp.Accepted_Completed__c = true;
                    }else if(!app.Opportunity__r.Get_Ready_Completed__c && key == '4CO'){
                        opp.Get_Ready_Completed__c = true;
                    }else if(!app.Opportunity__r.Extra_documentation_needed_Completed__c && key == '5CO'){
                        opp.Extra_documentation_needed_Completed__c = true;
                    }  
                }    
            }
        }		
        
        /*Update Opp*/
        if(!mapOpps.values().isEmpty()){
            Afs_Opportunity_Trigger_Helper.allowOppUpdate = true;
        	List<Database.SaveResult> lstSR = Database.Update(mapOpps.values(),false);
            for(Integer i = 0;i < lstSR.Size(); i++){                
            	Database.SaveResult sr = lstSR[i];
                if(!sr.isSuccess()){
                    system.debug(logginglevel.error,mapOpps.values()[i] + ' --> ' + String.join(sr.getErrors(),' / '));
                }
            }
        }        
    }
    
    //Mark the checkbox on Opportunity when ToDoItem is marked In Review or Completed
    public static void actualizarOppCheckbox(Map<Id,To_Do_Item__c> mapNew,Map<Id,To_Do_Item__c> mapOld,Boolean isUpdate){
        Set<String> setStatus = new Set<String>{'Completed','In Review'};
        Set<Id> opps = new Set<Id>();
        Set<Id> toDos = new Set<Id>();
        List<Opportunity> updateOpps = new List<Opportunity>();
        
        for(To_Do_Item__c toDo :mapNew.Values()){
            if(setStatus.contains(toDo.Status__c) && toDo.COMPLETE_YOUR_PROFILE_BY__c && (!isUpdate || toDo.Status__c != mapOld.get(toDo.Id).Status__c)){
                    toDos.add(toDo.Id);
            }
        }        
        for(To_Do_Item__c toDo :[SELECT Application__r.Opportunity__c FROM To_Do_Item__c WHERE Id IN :toDos]){
            opps.add(toDo.Application__r.Opportunity__c);
        }
            
        for(Id idOpp : opps){
            updateOpps.add(new Opportunity(Id = idOpp, Profile_Completed__c = true));
        }            
        
        if(updateOpps.size() > 0){
            Afs_Opportunity_Trigger_Helper.allowOppUpdate = true;
            List<Database.SaveResult> lstSR = database.update(updateOpps, false);
            for(Integer i = 0;i < lstSR.Size(); i++){                
                Database.SaveResult sr = lstSR[i];
                if(!sr.isSuccess()){
                    system.debug(logginglevel.error,updateOpps[i] + ' --> ' + String.join(sr.getErrors(),' / '));
                }
            } 
        }
       
    }
}