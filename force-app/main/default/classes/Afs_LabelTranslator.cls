public class Afs_LabelTranslator {
	
   public String translate(String labelName, String language){
       Pagereference r = Page.Afs_LabelTranslator;
       r.getParameters().put('label_lang', language);
       r.getParameters().put('label', labelName);
       String labelValue = '';
       if(!Test.isRunningTest()){
       	labelValue = r.getContent().toString();
       }
       return labelValue;
   }
   
}